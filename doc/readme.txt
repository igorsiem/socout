Doc Folder
==========
This folder is for documentation. By default, markdown files (with subscript
`.md`) will be converted to PDF files using pandoc, and placed in the
`build/doc` folder (along with HTML doxygen output).