# Rake file for building documentation

# Make sure the doc target directory exists
directory $doc_build_dir

# High-level PDF docs
#
# Start by globbing all the .md files in the doc subdirectory
Dir.glob("#{$doc_source_dir}/*.md").each do |source_filename|
    # Extract the basename of the file.
    file_basename = File.basename(source_filename, ".md")
    
    # Construct a file task for converting the .md file into the
    # corresponding PDF.
    file "#{$doc_build_dir}/#{file_basename}.pdf" => [
            $doc_build_dir,
            "#{$doc_source_dir}/#{file_basename}.md"] do
        sh "pandoc #{$doc_source_dir}/#{file_basename}.md -o " +
            "#{$doc_build_dir}/#{file_basename}.pdf"
    end
    
    # Add the file task to a general :pdf_files task
    task :pdf_files => "#{$doc_build_dir}/#{file_basename}.pdf"
end # End *.md file glob

# Low-level code documentation, generated using doxygen
directory $code_doc_build_dir
task :run_doxygen => $code_doc_build_dir do
    sh "doxygen"
end

# Set up a nice 
namespace :doc do
    desc "generate high-level PDF documentation"
    task :pdf => :pdf_files
    
    desc "generate low-level HTML code documentation"
    task :code => :run_doxygen
    
    task :all => [:pdf, :code]
end

desc "generate all documetnation"
task :doc => "doc:all"
