SocOut Readme.txt
=================
This is the source code for SocOut, a software tool for aggregating social
media content from diverse sources.

To learn more about SocOut, you should consult the following resources:

*   [The SocOut Website](http://theigorcode.com/socout/)
*   [The SocOut Repo](https://bitbucket.org/igorsiem/socout)

SocOut and all associated material is copyright (c) 2015 Igor Siemienowicz,
except where explicitly noted otherwise. Correspondence may be directed to
socout@theigorcode.com.

This is free software, licensed under the GPL v3.0. Some software components
on which SocOut relies are licensed under different terms. These are
documented in the `licenses` section.