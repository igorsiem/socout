# Rake file for building from source

# Make sure the build directory exists
directory $build_dir

# Cmake stuff - this generates the platform-specific project / make files
task :run_cmake => $build_dir do
    # We need to run cmake from the build directory, so change to that
    # location now.
    old_pwd = Dir.pwd
    Dir.chdir $build_dir
    
    # Platform specific stuff.
    cmake_command = "cmake ../#{$source_dir}"
    if $platform == :windows
        # Check that Visual Studio 2010 or 2013 has been installed, and that
        # the vcvars batch file has been called. There is no way to call it
        # from the Ruby script, and have the environment variables carry
        # over. There might be a clean or not-so-clean way around this, but
        # I'm not sure what it is at this point.
        if ENV['VS120COMNTOOLS'] == nil
            raise "could not find environment variable 'VS120COMNTOOLS' - " +
                "is Visual Studio 2012 installed?"
        end

        if ENV['VCINSTALLDIR'] == nil
            raise "could not find environment variable 'VCINSTALLDIR' - " +
                "have you run 'vcvars.bat' or 'vcvars32.bat'?"
        end
        
        # Select our generator - 32-bit or 64-bit Visual Studio 12.
        cmake_generator = "Visual Studio 12"
        cmake_generator = cmake_generator + " Win64" if $wordsize == 64

        # Put together our cmake command.
        cmake_command = "cmake -G \"#{cmake_generator}\" ..\\" +
            "#{$source_dir}"
        
    end
    
    sh cmake_command
    
    Dir.chdir old_pwd
end

# This does the work of compiling and and linking the target binaries
task :run_make => :run_cmake do
    old_pwd = Dir.pwd
    Dir.chdir $build_dir

    # The make command is platform-specific
    make_command = "make"
    
    if $platform == :windows
        make_command = "msbuild #{$project_name}.sln " +
            "/p:Configuration=Release"
    end
        
    sh make_command

    Dir.chdir old_pwd
end

namespace :debug do
    # This does a debug version of compiling and linking binaries
    task :run_make => :run_cmake do
        old_pwd = Dir.pwd
        Dir.chdir $build_dir

        # The make command is platform-specific
        make_command = "make"
    
        if $platform == :windows
            make_command = "msbuild #{$project_name}.sln " +
                "/p:Configuration=Debug"
        end
        
        sh make_command

    end # run_make task
end # debug namespace 

# If we're in windows, we need a number of Qt DLLs. We copy them to the
# release area so that we can run from there, and they also need to be
# deployed in the installation package. The following creates rake rules for
# each of these files, so that we're only copying them if we have to.
dll_dependencies = []
if $platform == :windows
    dlls = [
        "icudt52.dll",
        "icuin52.dll",
        "icuuc52.dll",
        "Qt5Gui.dll",
        "Qt5Core.dll",
        "Qt5Widgets.dll",
        "Qt5Xml.dll",
        "Qt5Svg.dll",
        "Qt5Network.dll"
]
        
    directory "#{$build_dir}/bin/Release"
                
    dlls.each do |dll_name|
        file "#{$build_dir}/bin/Release/#{dll_name}" =>
                ["#{$qt_environment}/bin/#{dll_name}",
                "#{$build_dir}/bin/Release"] do
            puts "Copying #{dll_name}..."
                
            FileUtils.cp(
                "#{$qt_environment}/bin/#{dll_name}",
                "#{$build_dir}/bin/Release")
                
            puts "    ... done"
        end
        
        dll_dependencies << "#{$build_dir}/bin/Release/#{dll_name}"
    end # each dll
end # if windows

# The debug version of the DLL dependencies
dll_debug_dependencies = []
if $platform == :windows
    dlls = [
        "icudt52.dll",
        "icuin52.dll",
        "icuuc52.dll",
        "Qt5Guid.dll",
        "Qt5Cored.dll",
        "Qt5Widgetsd.dll",
        "Qt5Xmld.dll",
        "Qt5Svgd.dll",
        "Qt5Networkd.dll"
]
        
    directory "#{$build_dir}/bin/Debug"
                
    dlls.each do |dll_name|
        file "#{$build_dir}/bin/Debug/#{dll_name}" =>
                ["#{$qt_environment}/bin/#{dll_name}",
                "#{$build_dir}/bin/Debug"] do
            puts "Copying #{dll_name}..."
                
            FileUtils.cp(
                "#{$qt_environment}/bin/#{dll_name}",
                "#{$build_dir}/bin/Debug")
                
            puts "    ... done"
        end
        
        dll_debug_dependencies << "#{$build_dir}/bin/Debug/#{dll_name}"
    end # each dll
end # if windows

# Set up top-level targets that will actually be used (run "rake -T" to get
# a list of all of these
namespace :binaries do
    desc "set up the build environment and project files"
    task :environment => :run_cmake
    
    desc "build all binary targets"
    task :make => :run_make
    
    task :all => [:environment, :make]
    task :all => dll_dependencies    
end

# Set up top-level targets for generating debug targets.
namespace :debug do
    namespace :binaries do
        desc "set up the build environment and project files"
        task :environment => :run_cmake
    
        desc "build all debug binary targets"
        task :make => "debug:run_make"
    
        task :all => [:environment, :make]
        task :all => dll_debug_dependencies
    end # binaries namespace
    
    desc "build all debug binary targets"
    task :binaries => "binaries:all"
end # debug namespace

desc "build all binary targets"
task :binaries => "binaries:all"
