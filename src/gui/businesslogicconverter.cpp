/**
 * \file businesslogicconverter.cpp Implements the BusinessLogicConverter
 * class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDateTime>

#include <utilities/Log.h>

#include "businesslogicconverter.h"
#include "errorhandling.h"

QHash<QString, QVariant>& BusinessLogicConverter::to_qhash(
        const Item& item,
        QHash<QString, QVariant>& hash)
{
    hash["datetime"] = QVariant(QDateTime::fromTime_t(item.get_datetime()));
    hash["headline"] = QVariant(item.get_headline().c_str());
    hash["digest"] = QVariant(item.get_digest().c_str());
    hash["text"] = QVariant(item.get_text().c_str());
    hash["profile-image-url"] = QVariant(
                item.get_profile_image_url().c_str());
    hash["author"] = QVariant(item.get_author().c_str());

    return hash;
}   // end to_qhash

Item& BusinessLogicConverter::to_item(
        const QHash<QString, QVariant>& hash,
        Item& item)
{
    QHash<QString, QVariant>::const_iterator
            datetime_itr = hash.find("datetime"),
            headline_itr = hash.find("headline"),
            digest_itr = hash.find("digest"),
            text_itr = hash.find("text"),
            profile_image_url_itr = hash.find("profile-image-url"),
            author_itr = hash.find("author");

    if (datetime_itr == hash.end())
        RAISE_ERROR("Qt hash object has no \"datetime\" element when "
                    "converting to an Item object.");
    if (headline_itr == hash.end())
        RAISE_ERROR("Qt hash object has no \"headline\" element when "
                    "converting to an Item object.")
    if (digest_itr == hash.end())
        RAISE_ERROR("Qt hash object has no \"digest\" element when "
                    "converting to an Item object.");
    if (text_itr == hash.end())
        RAISE_ERROR("Qt hash object has no \"text\" element when "
                    "converting to an Item object.");
    if (profile_image_url_itr == hash.end())
        RAISE_ERROR("Qt hash object has no \"profile-image-url element "
                    "when converting to an Item object.");
    if (author_itr == hash.end())
        RAISE_ERROR("Qt hash object has no \"author\" element when "
                    "converting to an Item object.");

    item = Item(datetime_itr->toDateTime().toTime_t(),
                headline_itr->toString().toStdString(),
                digest_itr->toString().toStdString(),
                text_itr->toString().toStdString(),
                profile_image_url_itr->toString().toStdString(),
                author_itr->toString().toStdString());

    return item;
}   // end to_item

QList<QVariant> BusinessLogicConverter::to_qlist(
        const Items& items,
        QList<QVariant>& list)
{
    for (auto item : items)
    {
        QHash<QString, QVariant> v;
        list.append(QVariant(to_qhash(item.second, v)));
    }

    return list;
}   // end to_qlist method

Items& BusinessLogicConverter::to_items(
        const QList<QVariant>& list,
        Items& items)
{
    for (auto v : list)
    {
        Item item(0, "", "", "");
        to_item(v.toHash(), item);

        items.insert(std::make_pair(item.get_datetime(), item));
    }

    return items;
}   // end to_items method
