/**
 * \file timelinespecificationdialog.h Declares the
 * TimelineSpecificationDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <business-logic/Account.h>
#include <business-logic/Timeline.h>

#ifndef TIMELINESPECIFICATIONDIALOG_H
#define TIMELINESPECIFICATIONDIALOG_H

#include <QDialog>

namespace Ui {
class TimelineSpecificationDialog;
}

/**
 * \brief A dialog for specifying a timeline
 *
 * \todo At the moment, Timelines only have a name, and one or more account
 * IDs. In the future, we'll be adding stuff like time periods and
 * keywords / hashtags.
 */
class TimelineSpecificationDialog : public QDialog
{
    Q_OBJECT

public:

    /**
     * \brief Constructor - sets up the UI
     *
     * \param timeline The Timeline being edited
     *
     * \param account_ids The list of accounts available in the data file
     *
     * \param timelines The list of existing timeline objects; this is used
     * to ensure that the timeline names remain unique
     *
     * \parent The parent widget for the dialog
     */
    explicit TimelineSpecificationDialog(
            Timeline& timeline,
            const AccountIdentifiers& account_ids,
            const Timelines& timelines,
            QWidget *parent = 0);

    /**
     * \brief Destructor - shuts down the UI
     */
    ~TimelineSpecificationDialog();

    /**
     * \brief Retrieve the value of the 'refresh' check-box
     *
     * This is used to indicate that the user wishes to refresh the timeline
     * after its specification has been updated.
     */
    bool refresh_checked(void) const;

protected slots:

    /**
     * \brief Perform validation before closing
     *
     * This method validates the fields, ensuring that at least one account
     * has been selected, and that the name is not blank.
     *
     * If everything is OK, the base-class `accept` method is called.
     */
    virtual void accept(void) override;

private:

    /**
     * \brief The UI framework created by Qt
     */
    Ui::TimelineSpecificationDialog *ui;

    /**
     * \brief The timeline being edited
     */
    Timeline& m_timeline;

    /**
     * \brief The existing timelines; this is used to
     * ensure that timeline names are kept unique
     */
    const Timelines& m_timelines;
};  // end TimelineSpecificationDialog class

#endif // TIMELINESPECIFICATIONDIALOG_H
