/**
 * \file timelinespecificationdialog.h Implements the
 * TimelineSpecificationDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QListWidget>
#include <QListWidgetItem>

#include "timelinespecificationdialog.h"
#include "ui_timelinespecificationdialog.h"
#include "errorhandling.h"

TimelineSpecificationDialog::TimelineSpecificationDialog(
        Timeline &timeline,
        const AccountIdentifiers &account_ids,
        const Timelines &timelines,
        QWidget *parent) :
    QDialog(parent),
    m_timeline(timeline),
    m_timelines(timelines),
    ui(new Ui::TimelineSpecificationDialog)
{
    ui->setupUi(this);

    // Set up the item elements - first, the name.
    ui->name_edit->setText(timeline.get_name().c_str());

    // Now the accounts.
    for (auto account_id : account_ids)
    {
        // Set up a list item for this account.
        std::stringstream account_id_strm;
        account_id_strm << account_id.second << " (" << account_id.first <<
                           ")";

        QListWidgetItem* account_item =
                new QListWidgetItem(account_id_strm.str().c_str());
        account_item->setFlags(
                    account_item->flags() | Qt::ItemIsUserCheckable);

        // If the account is in the timeline, check its checkbox.
        if (timeline.get_account_ids().find(account_id) !=
                timeline.get_account_ids().end())
            account_item->setCheckState(Qt::Checked);
        else account_item->setCheckState(Qt::Unchecked);

        // Put together a QStringList QVariant that we can use to easily
        // identify the account later.
        QStringList account_id_strl = {
            account_id.first.c_str(),
            account_id.second.c_str() };

        account_item->setData(Qt::UserRole, QVariant(account_id_strl));

        ui->accounts_list->insertItem(0, account_item);
    }   // end account ID loop
}   // end constructor

TimelineSpecificationDialog::~TimelineSpecificationDialog()
{
    delete ui;
}   // end destructor

bool TimelineSpecificationDialog::refresh_checked(void) const
{
    return ui->refresh_cb->isChecked();
}   // end refresh_checked

void TimelineSpecificationDialog::accept(void)
{
    MASTER_TRY

    if (ui->name_edit->text().isEmpty())
        RAISE_ERROR("The Timeline name may not be blank.");

    // If we have changed the name, make sure that the name is not already
    // taken.
    if (ui->name_edit->text().toStdString() != m_timeline.get_name())
    {
        if (m_timelines.find(ui->name_edit->text().toStdString()) !=
                m_timelines.end())
            RAISE_ERROR("There is already a timeline with the name \"" <<
                        ui->name_edit->text().toStdString() << "\".");
    }   // end if the name has been updated

    AccountIdentifiers account_ids;
    for (int i = 0; i < ui->accounts_list->count(); i++)
    {
        // Get out the list item, and see whether it is checked.
        QListWidgetItem* item = ui->accounts_list->item(i);
        if (item->checkState() == Qt::Checked)
        {
            QVariant item_data = item->data(Qt::UserRole);
            QStringList account_id_strl = item_data.toStringList();

            account_ids.insert(
                        std::make_pair(
                            account_id_strl[0].toStdString(),
                            account_id_strl[1].toStdString()));
        }   // end if item is checked
    }   // end list widget items loop

    if (account_ids.empty())
        RAISE_ERROR("A Timeline must have at least one Account selected.");

    m_timeline.set_name(ui->name_edit->text().toStdString());
    m_timeline.get_account_ids() = account_ids;

    QDialog::accept();

    MASTER_CATCH("Item");
}   // end accept
