/**
 * \file Implements the ProgressDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMessageBox>
#include <QThreadPool>

#include "progressdialog.h"
#include "ui_progressdialog.h"

bool ProgressTask::report_progress(int p)
{
    emit progress(p);
    return is_stop_signalled();
}   // end report_progress

int ProgressDialog::execute(
        QWidget* parent,
        ProgressTask* task,
        const std::string& title)
{
    // Instantiate the progress dialog
    ProgressDialog dlg(parent, title);

    // Connect the signals and slots between the dialog and the task.
    connect(
        task, &ProgressTask::progress,
        &dlg, &ProgressDialog::progress);

    connect(
        task, &ProgressTask::finished,
        &dlg, &ProgressDialog::finished);

    connect(
        task, &ProgressTask::finished_with_error,
        &dlg, &ProgressDialog::finished_with_error);

    connect(
        &dlg, &ProgressDialog::should_stop,
        task, &ProgressTask::should_stop);

    // Pass the task to the global thread pool.
    QThreadPool::globalInstance()->start(task);

    // Execute the dialog
    return dlg.exec();
}   // end execute

ProgressDialog::ProgressDialog(
        QWidget *parent,
        const std::string &title) :
    QDialog(parent),
    ui(new Ui::ProgressDialog),
    m_title(title),
    m_cancel_button_clicked(false)
{
    ui->setupUi(this);
    setWindowTitle(title.c_str());
}   // end constructor

ProgressDialog::~ProgressDialog()
{
    delete ui;
}   // end destructor

void ProgressDialog::progress(int value)
{
    ui->progress_bar->setValue(value);
}   // end progress method

void ProgressDialog::finished(void)
{
    ui->progress_bar->setValue(100);

    if (m_cancel_button_clicked) reject();
    else accept();
}   // end finished method

void ProgressDialog::finished_with_error(const QString& message)
{
    QMessageBox::critical(this, m_title.c_str(), message);
    reject();
}   // end finished_with_error method

void ProgressDialog::on_cancel_button_clicked()
{
    emit should_stop();
    m_cancel_button_clicked = true;
}   // end on_cancel_button_clicked method
