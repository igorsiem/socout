/**
 * \file retrieveaccountitemstask.h Implements the RetrieveAccountItemsTask
 * class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RETRIEVEACCOUNTITEMSTASK_H
#define RETRIEVEACCOUNTITEMSTASK_H

#include <QRunnable>
#include <QObject>
#include <QVariant>

#include <business-logic/Account.h>

/**
 * \brief A background task for retrieving items from a given account
 *
 * Objects of this class are instatiated (using `new`) with an `Account`
 * object. Its signals are linked to appropriate slots in the `TimelineView`
 * and `MainWindow` objects, after which it is passed to the global thread
 * pool for execution and destruction.
 *
 * \todo In the current implementation, this task only retrieves the 'recent'
 * history of account items. Future iterations should include search and
 * filtering parameters.
 */
class RetrieveAccountItemsTask final : public QObject, public QRunnable
{
    Q_OBJECT

public:

    /**
     * \brief Constructor - sets up all the internal variables for the class
     *
     * \param account The `Account` object for which `Items` are retrieved;
     * the correct `Service` object is derived from this; note that this
     * object is *cloned*
     */
    explicit RetrieveAccountItemsTask(
            Account& account) :
        QObject(),
        QRunnable(),
        m_account(account.clone()) {}

    /**
     * \brief Destructor - deletes the internal copy of the Account object
     */
    virtual ~RetrieveAccountItemsTask(void) { delete m_account; }

    /**
     * \brief Retrieve the items for the account
     *
     * This method is called in the background thread. It emits various
     * signals for progress monitoring. No exceptions are thrown. Errors are
     * signalled with `finished_with_error`.
     */
    virtual void run(void) override;

signals:

    /**
     * \brief The task has commenced
     */
    void started(void);

    /**
     * \brief The task has completed without error
     *
     * \param stopped_early The task finished early, because of a `stop`
     * signal
     */
    void finished(bool stopped_early);

    /**
     * \brief The task finished with an error
     *
     * \param error_message The error message
     */
    void finished_with_error(const QString& error_message);

    /**
     * \brief Signal that items have been retrieved for the account, and
     * placed in the `items_container
     *
     * \todo Signalling item retrieval with a copy of the items collection
     * may not be very efficient
     *
     * \param data The items that have been retrieved, converted to a
     * QVariant
     */
    void items_retrieved(QVariant data);

    // --- Internal Declarations ---

private:

    /**
     * \brief The Accout for which the Items are to be retrieve
     *
     * This object is cloned from that passed to the constructor, and
     * deleted by the destructor.
     */
    Account* m_account;
};  // end RetrieveAccountItemsTask class

#endif // RETRIEVEACCOUNTITEMSTASK_H
