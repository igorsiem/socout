/**
 * \brief Implements the ConfigurationDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>

#include <QStandardPaths>
#include <QMessageBox>
#include <QFileDialog>
#include <QString>

#include <business-logic/Configuration.h>

#include "configurationdialog.h"
#include "ui_configurationdialog.h"
#include "errorhandling.h"

ConfigurationDialog::ConfigurationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigurationDialog)
{
    ui->setupUi(this);

    // Set up the UI from the config object.
    //
    // Verify accounts on load...
    const Configuration& config =
            Configuration::get_configuration();
    ui->verify_accounts_on_load_chb->setChecked(
                config.get_verify_accounts_on_load());

    // Console log level
    QStringList log_levels =
        { "NONE", "ERROR", "WARNING", "INFO", "DEBUG" };
    ui->console_log_level_cmb->addItems(log_levels);
    ui->console_log_level_cmb->setCurrentText(
                Log::to_string(
                    config.get_console_log_level()).c_str());

    // Log file
    ui->log_filename_edit->setText(config.get_file_log_filename().c_str());

    // File logging level
    ui->log_file_logging_level_cmb->addItems(log_levels);
    ui->log_file_logging_level_cmb->setCurrentText(
                Log::to_string(
                    config.get_file_log_level()).c_str());
}   // end constructor

ConfigurationDialog::~ConfigurationDialog()
{
    delete ui;
}   // end destructor

void ConfigurationDialog::accept(void)
{
    MASTER_TRY

    // If the file logging level is not "NONE", then we we must have a
    // non-empty log filename.
    if ((ui->log_file_logging_level_cmb->currentText() != "NONE") &&
            ui->log_filename_edit->text().isEmpty())
        RAISE_ERROR("File logging level is not \"NONE\", but log filename "
                    "is empty. If you want to log messages to a file, then "
                    "you need to select a filename.");

    Configuration& config = Configuration::get_configuration();
    config.set_verify_accounts_on_load(
                ui->verify_accounts_on_load_chb->isChecked());
    config.set_console_log_level(
                Log::to_level(
                    ui->console_log_level_cmb->currentText().toStdString()));
    config.set_file_log_filename(
                ui->log_filename_edit->text().toStdString());
    config.set_file_log_level(
                Log::to_level(
                    ui->log_file_logging_level_cmb->
                    currentText().toStdString()));

    // Write the updated configuration to the file.
    config.write_config_file();

    LOG_INFO("Updated configuration written to \"" <<
             config.get_config_filename() << "\".");

    QDialog::accept();

    MASTER_CATCH("Update Configuration");
}   // end accept method

void ConfigurationDialog::on_log_filename_select_button_clicked(void)
{
    MASTER_TRY

    // Get a filename from the user.
    QString filename = QFileDialog::getSaveFileName(
                this,
                "Log File",
                "", "Text files (*.txt);;All files (*.*)");

    if (filename.isEmpty() == false)
        ui->log_filename_edit->setText(filename);

    MASTER_CATCH("Select Log File");
}   // end on_log_filename_select_button_clicked method
