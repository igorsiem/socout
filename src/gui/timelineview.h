/**
 * \file timelineview.h Declares the TimelineView class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TIMELINEVIEW_H
#define TIMELINEVIEW_H

#include <QWidget>
#include <QObject>
#include <QLabel>
#include <QVariant>

#include <business-logic/Item.h>
#include <business-logic/DataFile.h>
#include <utilities/ThreadSafeDeque.h>

class MainWindow;

namespace Ui {
class TimelineView;
}

/**
 * \brief A view for displaying a timeline
 */
class TimelineView : public QWidget
{
    Q_OBJECT

public:

    /**
     * \brief Constructor - sets up the UI
     *
     * \param timeline_name The name of the timeline that this widget is
     * displaying
     *
     * \param data_file A reference to the DataFile object; note that this is
     * shared with the rest of the system, but implements its own thread-
     * safety measures, so it is safe to modify
     *
     * \param A pointer to the main window object; this is used for various
     * processing signals and indications
     *
     * \param parent The parent widget
     */
    TimelineView(
            const std::string& timeline_name,
            DataFile& data_file,
            MainWindow* mainwindow,
            QWidget *parent = 0);

    /**
     * \brief Destructor - shuts down the view
     */
    ~TimelineView();

    /**
     * \brief Retrieve the name of the timeline for this view
     */
    const std::string& get_timeline_name(void) const
        { return m_timeline_name; }

signals:

    /**
     * \brief Signals that the user wishes to delete the timeline
     *
     * \param timeline_name The name of the timeline
     */
    void request_delete_timeline(QString timeline_name);

public slots:

    /**
     * \brief Refresh the timeline
     *
     * This method actually starts the process, which is actually performed
     * in a background task.
     */
    void on_refresh_button_clicked(void);

private slots:

    /**
     * \brief Signals that the user has clicked the Delete button
     *
     * This method simply emits another signal intended to be caught by the
     * main window, which will delete the timeline and its tab.
     */
    void on_delete_button_clicked(void);

    /**
     * \brief Process items that have been retrieved
     *
     * When a background task retrieves items for an account, it emits an
     * `items_retrieved` signal, which is connected to this slot. This method
     * takes the retrieved items, and adds them to the list view.
     *
     * Note that the list view is *not* cleared prior to having its items
     * added, but it does sort the list by the item text (set to a sortable
     * text version of the item date/time). This method might be called
     * several times for a given 'refresh' operation.
     *
     * \todo Signalling item retrieval with a copy of the items as a QVaraint
     * collection may not be very efficient, but it solves a lot of problems
     * in the short-term.
     *
     * \param items The items that have been retrieved, as a QVariant
     */
    void process_retrieved_items(QVariant data);

    /**
     * \brief Allow the user to edit the parameters of the timeline
     */
    void on_edit_button_clicked();

private:

    /**
     * \brief The UI framework, created by Qt
     */
    Ui::TimelineView *ui;

    /**
     * \brief The name of the timeline being displayed
     */
    std::string m_timeline_name;

    /**
     * \brief A reference to the main data file object
     */
    DataFile& m_data_file;

    /**
     * \brief A pointer to the main window object
     *
     * This is used for various processing and status signals.
     */
    MainWindow* m_mainwindow;

    /**
     * \brief A label for displaying a 'processing' animation
     */
    QLabel* m_processing_animation_label;

    /**
     * \brief A count for the number of refresh operations that are currently
     * in progress
     */
    int m_refresh_ops_in_progress;
};  // end TimelineView class

#endif // TIMELINEVIEW_H
