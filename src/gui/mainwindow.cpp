/**
 * \file mainwindow.cpp MainWindow methods related to Person objects and the
 * People collection
 *
 * Note that this is one of a number of .cpp files implemeting methods that
 * are declared in the mainwindow.h file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QFileDialog>
#include <QVBoxLayout>

#include <business-logic/Error.h>
#include <utilities/Log.h>
#include <business-logic/Configuration.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "errorhandling.h"
#include "progressdialog.h"
#include "itemwidget.h"
#include "configurationdialog.h"
#include "timelineview.h"

/**
 * \brief A task for verifying account credentials in a background thread
 *
 * This class implements functionality for reading a DataFile object from a
 * named file. It implements the ProgressTask interface so that the work can
 * be performed in the backround, with progress monitoring. It is not used
 * anywhere but in this source file, so is totally contained here.
 */
class OpenDataFileTask final : public ProgressTask
{

public:

    /**
     * \brief Constuctor, initialising the internal attributes
     *
     * \param filename The name of the file being opened
     *
     * \param data_file The data file being read
     */
    OpenDataFileTask(
            const std::string& filename,
            DataFile& data_file) :
        ProgressTask(),
        m_filename(filename),
        m_data_file(data_file) {}

    /**
     * \brief Trivial destructor
     */
    virtual ~OpenDataFileTask(void) {}

    /**
     * \brief Open the data file
     *
     * This method is run in the background thread, and may take some time.
     * No exceptions are thrown - they are all caught and signalled by
     * emitting the `finished_with_error` signal.
     */
    virtual void run(void) override
    {
        try
        {
            // Read into a temporary data file, in case there is an error, or
            // cancellation.
            m_data_file.read_from(m_filename, *this);
            emit finished();
        }
        catch (const std::exception& error)
        {
            emit finished_with_error(error.what());
        }
        catch (...)
        {
            emit finished_with_error("An unrecognised error occurred while "
                                     "opening the data file.");
        }
    }   // end run method

    /**
     * \brief Log a message indicating that the task was cancelled
     *
     * This method signals that the monitored task was not completed due to
     * cancellation by the user. There is no reason to do anything with this,
     * but an INF message is logged.
     */
    virtual void signal_task_halted_early(void)
    {
        LOG_INFO("Opening of data file \"" << m_filename << "\" was "
                 "cancelled.");
    }   // end signal_task_halted_early method

    // --- Internal Declarations ---

private:

    /**
     * \brief The name of the file being opened
     */
    std::string m_filename;

    /**
     * \brief The DataFile object being read
     */
    DataFile& m_data_file;
};  // end OpenDataFileTask class

MainWindow::MainWindow(void) :
    QMainWindow(NULL),
    ui(new Ui::MainWindow),
    m_data_file(),
    m_people_table_is_being_set_up(false),
    m_accounts_table_is_being_set_up(false),
    m_undo_stack(),
    m_redo_stack(),
    m_timeline_views(),
    m_background_tasks_count(0),
    m_completed_background_tasks_count(0)
{
    // Set up the Qt UI framework.
    ui->setupUi(this);

    Configuration& config = Configuration::get_configuration();

    // Check whether our main window should be maximised or minimised
    if (config.get_main_window_state() == Configuration::ws_max)
    {
        LOG_DEBUG(__FUNCTION__ << " - maximising main window");
        setWindowState(Qt::WindowMaximized);
    }
    else
    {
        LOG_DEBUG(__FUNCTION__ << " - main window is not being maximised");
    }

    // Check whether a filename was specified.
    if (config.get_data_file_filename().empty() == false)
    {
        LOG_INFO("Opening data file \"" << config.get_data_file_filename() <<
                 "\" specified on the command-line.");

        // Read the file into a temporary object.
        DataFile temp;
        if (ProgressDialog::execute(
                    this,
                    new OpenDataFileTask(
                        config.get_data_file_filename(),
                        temp),
                    "Open Data File") == QDialog::Accepted)
        {
            m_data_file = temp;
            m_data_file.clear_is_dirty();
        }
    }   // end if the command-line filename was not empty
    else LOG_DEBUG("No data file specified on the command-line.");

    // Set up the GUI content.
    setup_gui();

    // Set up the undo / redo stacks
    m_undo_stack.clear();
    m_redo_stack.clear();
    set_undo_redo_action_enablement();
}   // end constructor

MainWindow::~MainWindow(void)
{
    delete ui;
}   // end destructor

void MainWindow::background_task_commenced(void)
{
    m_background_tasks_count++;
    update_background_task_processing_display();
}   // end background_task_commenced

void MainWindow::background_task_finished(bool)
{
    m_completed_background_tasks_count++;
    update_background_task_processing_display();
}   // end background_task_finished

void MainWindow::background_task_finished_with_error(
        const QString& error_message)
{
    m_completed_background_tasks_count++;
    update_background_task_processing_display();

    LOG_ERROR("Problem with background processing: " <<
              error_message.toStdString());

    QMessageBox::critical(
                this,
                "Problem with Background Processing",
                error_message);
}   // end background_task_finished_with_error

void MainWindow::update_background_task_processing_display(void)
{
    LOG_DEBUG(__FUNCTION__ << " - total background tasks: " <<
              m_background_tasks_count);
    LOG_DEBUG(__FUNCTION__ << " - total completed tasks: " <<
              m_completed_background_tasks_count);

    // Display the progress bar if we have some tasks to complete.
    if ((m_background_tasks_count > 1) &&
            (m_completed_background_tasks_count <
             m_background_tasks_count))
    {
        m_background_tasks_pb->setMaximum(m_background_tasks_count);
        m_background_tasks_pb->setValue(m_completed_background_tasks_count);
        m_background_tasks_pb->show();
    }
    else m_background_tasks_pb->hide();

    // Reset the task counts if we have completed everything.
    if (m_background_tasks_count == m_completed_background_tasks_count)
    {
        m_background_tasks_count = 0;
        m_completed_background_tasks_count = 0;
    }
}   // end update_background_task_processing method

void MainWindow::on_action_exit_triggered()
{
    close();
}   // end on_action_exit_triggered method

void MainWindow::closeEvent(QCloseEvent *event)
{
    // Check whether the file needs saving.
    if (m_data_file.is_dirty())
    {
        if (QMessageBox::question(this, "Close SocOut", "The data file has "
                "been changed. Would you like to save it?") ==
                QMessageBox::Yes)
        {
            // Do a standard save.
            on_action_save_triggered();
        }   // end if the user selected Yes

        event->accept();
    }   // end if the file is dirty
    else event->accept();

    // Set the window state in the config file (note that we don't save the
    // window state as minimised, because we don't ever want to start up
    // minimised.
    Configuration& config = Configuration::get_configuration();
    if (isMaximized()) config.set_main_window_state(Configuration::ws_max);
    else config.set_main_window_state(Configuration::ws_normal);

    config.write_config_file();
}   // end closeEvent

// -- Data File Functionality --

void MainWindow::on_action_new_triggered(void)
{
    MASTER_TRY

    // If the data file is 'dirty', get confirmation from the user that they
    // don't want to save.
    if (!m_data_file.is_dirty() || QMessageBox::question(this, "New Data "
            "File", "The changes in this Data File will be lost. Do you "
            "want to continue?") == QMessageBox::Yes)
    {
        // Recreate an empty data file, and set up its display.
        m_data_file.clear_to_new();
        setup_gui();

        // Clear the undo and redo stacks.
        m_undo_stack.clear();
        m_redo_stack.clear();
        set_undo_redo_action_enablement();
    }   // end if we want to go ahead and create a new data file.

    MASTER_CATCH("New Data File");
}   // end on_action_new_triggered method

void MainWindow::on_action_open_triggered()
{
    MASTER_TRY

    // If the data file is 'dirty', get confirmation from the user that they
    // don't want to save.
    if (!m_data_file.is_dirty() || QMessageBox::question(this, "Open Data "
            "File", "The changes in this Data File will be lost. Do you "
            "want to continue?") == QMessageBox::Yes)
    {

        // Get a filename to open.
        std::string filename = QFileDialog::getOpenFileName(
                    this,
                    "Open File",
                    "",
                    "SocOut files (*.socout);;All files (*.*)").
                toStdString();
        if (filename.empty() == false)
        {
            // Read the file into a temporary object.
            DataFile temp;
            if (ProgressDialog::execute(
                        this,
                        new OpenDataFileTask(filename, temp),
                        "Open Data File") == QDialog::Accepted)
            {
                m_data_file = temp;
                m_data_file.clear_is_dirty();
            }

            // Set up the UI.
            setup_gui();

            // Clear the undo and redo stacks.
            m_undo_stack.clear();
            m_redo_stack.clear();
            set_undo_redo_action_enablement();
        }
    }   // end if we can go ahead with opening the file.

    MASTER_CATCH("Open Data File");
}   // end on_action_open_triggered method

void MainWindow::on_action_save_triggered()
{
    MASTER_TRY
    // If we don't have a filename, go to save as. Otherwise, save the file.
    if (m_data_file.get_filename().empty()) on_action_save_as_triggered();
    else
    {
        m_data_file.save_to(m_data_file.get_filename());
        m_data_file.clear_is_dirty();
    }
    MASTER_CATCH("Save Data File");
}   // end on_action_save_triggered method

void MainWindow::on_action_save_as_triggered()
{
    MASTER_TRY

    // Get a filename from the user.
    std::string filename = QFileDialog::getSaveFileName(
                this,
                "Save File",
                "", "SocOut files (*.socout);;All files (*.*)").
            toStdString();

    if (filename.empty() == false)
    {
        // Save the data file and clear its
        m_data_file.save_to(filename);
        m_data_file.clear_is_dirty();
    }

    MASTER_CATCH("Save Data File As");
}   // end on_action_save_as_triggered method

void MainWindow::on_action_undo_triggered()
{
    MASTER_TRY
    undo_last_action();
    MASTER_CATCH("Undo");
}   // end on_action_undo_triggered

void MainWindow::on_action_redo_triggered()
{
    MASTER_TRY
    redo_last_undo();
    MASTER_CATCH("Redo");
}   // end on_action_redo_triggered

void MainWindow::on_action_options_triggered(void)
{
    ConfigurationDialog dlg;
    dlg.exec();
}   // end on_action_options_triggered method

void MainWindow::save_state_for_undo(void)
{
    // Save the state to the back of the deque, but trim to ten items from
    // the front.
    m_undo_stack.push_back(m_data_file);
    while (m_undo_stack.size() > 10) m_undo_stack.pop_front();

    m_redo_stack.clear();
    set_undo_redo_action_enablement();
}   // end save_state_for_undo

void MainWindow::setup_gui(void)
{
    setup_accounts_table();
    set_undo_redo_action_enablement();
    setup_timeline_tabs();

    // Set up a background task monitoring bar in the status bar.
    m_background_tasks_pb = new QProgressBar;
    m_background_tasks_pb->setMaximum(120);
    m_background_tasks_pb->setValue(100);
    ui->status_bar->insertWidget(0, m_background_tasks_pb);
    update_background_task_processing_display();
}   // end setup_gui

void MainWindow::undo_last_action(void)
{
    if (m_undo_stack.size() > 0)
    {
        // Save the current state to the redo stack, but trim it to ten items
        // from the front.
        m_redo_stack.push_back(m_data_file);
        while (m_redo_stack.size() > 10) m_redo_stack.pop_front();

        m_data_file = m_undo_stack.back();
        m_undo_stack.pop_back();

        setup_gui();
    }
}   // end undo_last_action

void MainWindow::redo_last_undo(void)
{
    if (m_redo_stack.size() > 0)
    {
        m_undo_stack.push_back(m_data_file);
        m_data_file = m_redo_stack.back();
        m_redo_stack.pop_back();

        setup_gui();
    }
}   // end redo_last_undo

void MainWindow::set_undo_redo_action_enablement(void)
{
    if (m_undo_stack.size() > 0) ui->action_undo->setEnabled(true);
    else ui->action_undo->setEnabled(false);

    if (m_redo_stack.size() > 0) ui->action_redo->setEnabled(true);
    else ui->action_redo->setEnabled(false);
}   // end set_undo_redo_action_enablement
