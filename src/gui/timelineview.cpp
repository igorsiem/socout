/**
 * \file timelineview.h Implements the TimelineView class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <iomanip>

#include <QMovie>
#include <QThreadPool>
#include <QRunnable>
#include <QListWidget>
#include <QListWidgetItem>
#include <QDateTime>

#include <business-logic/Account.h>
#include <business-logic/Item.h>
#include <business-logic/twitter/Twitter.h>
#include <utilities/ThreadSafeDeque.h>
#include <utilities/Log.h>

#include "timelineview.h"
#include "ui_timelineview.h"
#include "errorhandling.h"
#include "mainwindow.h"
#include "retrieveaccountitemstask.h"
#include "businesslogicconverter.h"
#include "itemwidget.h"
#include "timelinespecificationdialog.h"

TimelineView::TimelineView(const std::string &timeline_name,
        DataFile &data_file,
        MainWindow *mainwindow,
        QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimelineView),
    m_timeline_name(timeline_name),
    m_data_file(data_file),
    m_mainwindow(mainwindow),
    m_processing_animation_label(new QLabel),
    m_refresh_ops_in_progress(0)
{
    ui->setupUi(this);
    ui->name_label->setText(timeline_name.c_str());

    // Set up a small (hidden) animation for when we're updating
    m_processing_animation_label->setMovie(
                new QMovie(":/resources/processing-animation.gif"));
    m_processing_animation_label->setAlignment(Qt::AlignHCenter);
    m_processing_animation_label->hide();
    ui->verticalLayout->insertWidget(1, m_processing_animation_label);
}   // end constructor

TimelineView::~TimelineView()
{
    delete ui;
}   // end destructor

void TimelineView::on_refresh_button_clicked(void)
{
    MASTER_TRY

    // Hide our main list view.
    ui->list->hide();
    m_processing_animation_label->show();
    m_processing_animation_label->movie()->start();

    // Disable the refresh button (it will be enabled when all refresh
    // ops are complete).
    ui->refresh_button->setEnabled(false);

    // Clear our main list view of the old data.
    ui->list->clear();

    // Get out the timeline object.
    Timeline timeline = m_data_file.get_timelines()[m_timeline_name];

    // Get copies of our accounts out.
    AccountSharedPtrList accounts;
    m_data_file.get_account_copies(timeline.get_account_ids(), accounts);

    // For each account, start a background task to retrieve its timeline.
    for (auto account : accounts)
    {
        // Create a retrieval task object, and connect up its signals.
        RetrieveAccountItemsTask* task =
                new RetrieveAccountItemsTask(*account);

        // TODO: this could be handled at the generic MainWindow level,
        // with a method taking a BackgroundTask pointer, and connecting
        // up the higher-level signals/slots in a single method.
        connect(
                    task,
                    SIGNAL(started()),
                    m_mainwindow,
                    SLOT(background_task_commenced()));

        connect(
                    task,
                    SIGNAL(finished(bool)),
                    m_mainwindow,
                    SLOT(background_task_finished(bool)));

        connect(
                    task,
                    SIGNAL(finished_with_error(QString)),
                    m_mainwindow,
                    SLOT(background_task_finished_with_error(QString)));

        connect(
                    task,
                    SIGNAL(items_retrieved(QVariant)),
                    this,
                    SLOT(process_retrieved_items(QVariant)));

        LOG_DEBUG(__FUNCTION__ << " - starting task to retrieve "
                  "timeline for account \"" << account->get_id() << "\" "
                  "(service \"" << account->get_service_name() << "\").");

        // Hand the task over to the global thread pool.
        QThreadPool::globalInstance()->start(task);

        // Increment the refresh ops counter.
        m_refresh_ops_in_progress++;
    }   // end accounts loop

    MASTER_CATCH("Refresh Timeline");
}   // end on_refresh_button_clicked method

void TimelineView::on_edit_button_clicked()
{
    MASTER_TRY

    // Get the timeline object out.
    Timelines timelines = m_data_file.get_timelines();
    Timelines::iterator timeline_itr = timelines.find(m_timeline_name);
    if (timeline_itr == timelines.end())
        RAISE_ERROR("A Timeline with the name \"" << m_timeline_name <<
                    "\" could not be located.");

    Timeline timeline = timeline_itr->second;

    AccountIdentifiers account_ids;
    TimelineSpecificationDialog dlg(
                timeline,
                m_data_file.get_account_ids(account_ids),
                timelines,
                this);

    if (dlg.exec() == QDialog::Accepted)
    {
        m_mainwindow->save_state_for_undo();
        timelines.erase(timeline_itr);
        timelines.insert(
                    std::make_pair(
                        timeline.get_name(),
                        timeline));
        m_data_file.set_timelines(timelines);
        m_data_file.set_is_dirty(true);

        // Make sure the timeline name is updated everywhere it needs to be.
        ui->name_label->setText(timeline.get_name().c_str());
        m_timeline_name = timeline.get_name();
        m_mainwindow->update_timeline_tab_names();

        // Refresh the tab if we want to.
        if (dlg.refresh_checked()) on_refresh_button_clicked();
    }   // end if the timeline parameters were edited successfully

    MASTER_CATCH("Edit Timeline Specification");
}   // end on_edit_button_clicked method

void TimelineView::on_delete_button_clicked()
{
    // Simply pass this signal on to the main window to do all the hard work
    // of actually deleting the timeline.
    emit request_delete_timeline(m_timeline_name.c_str());
}   // end on_delete_button_clicked method

void TimelineView::process_retrieved_items(QVariant data)
{
    // Convert out retrieved variant data back to items.
    Items items;
    BusinessLogicConverter::to_items(data.toList(), items);

    LOG_DEBUG(__FUNCTION__ << " - received " << items.size() << " item(s).");

    // ui->list->setSortingEnabled(false);

    // Put the items into our list.
    for (auto item_itr : items)
    {
        // Turn the date/time into text for the (hidden) item text,
        // so that it will sort properly.
        //
        // TODO Clean this up - it's horrible.
        std::stringstream item_strm;
        QDateTime datetime;
        datetime.setTime_t(item_itr.second.get_datetime());
        datetime = datetime.toLocalTime();
        item_strm << std::setfill('0') << std::setw(4) <<
                     datetime.date().year() << "-" << std::setfill('0') <<
                     std::setw(2) << datetime.date().month() << "-" <<
                     std::setfill('0') << std::setw(2) <<
                     datetime.date().day() << " " << std::setfill('0') <<
                     std::setw(2) << datetime.time().hour() << ":" <<
                     std::setfill('0') << std::setw(2) <<
                     datetime.time().minute() << ":" << std::setfill('0') <<
                     std::setw(2) << datetime.time().second();

        // Create a new new list widget item, and hook it up to a content
        // ItemWidget. Note that the list widget takes ownership of these
        // objects.
        ItemWidget* item_widget = new ItemWidget(
                    item_itr.second,
                    m_mainwindow);
        QListWidgetItem* list_item = new QListWidgetItem;

        // Put the date/time text into the list item so that it will sort
        // properly.
        //
        // TODO This is a hack - clean it up.
        list_item->setText(item_strm.str().c_str());

        list_item->setSizeHint(
                    QSize(
                        item_widget->size().width(),
                        item_widget->get_preferred_height()));

        ui->list->insertItem(0, list_item);
        ui->list->setItemWidget(list_item, item_widget);
    }   // end items loop

    // Sort the items.
    // ui->list->setSortingEnabled(true);
    ui->list->sortItems(Qt::DescendingOrder);

    // Stop / hide our 'waiting' animation, and display our newly created
    // list.
    m_processing_animation_label->movie()->stop();
    m_processing_animation_label->hide();
    ui->list->show();

    // Decrement the refresh ops counter. If we've reached zero, we can
    // enable the refresh button again.
    m_refresh_ops_in_progress--;

    if (m_refresh_ops_in_progress == 0) ui->refresh_button->setEnabled(true);
}   // end on_items_retrieved method
