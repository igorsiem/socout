/**
 * \file mainwindow_accounts.cpp MainWindow methods related to Account
 * objects and the Accounts collection
 *
 * Note that this is one of a number of .cpp files implemeting methods that
 * are declared in the mainwindow.h file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QTableWidgetItem>

#include <business-logic/Error.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "errorhandling.h"
#include "twitternewaccountdialog.h"

void MainWindow::on_account_add_button_clicked(void)
{
    MASTER_TRY

    // Get out the list of existing account IDs.
    AccountIdentifiers existing_account_ids;
    m_data_file.get_account_ids(existing_account_ids);

    // Use the appropriate dialog
    // TODO Only Twitter support at the moment - need to add more services.
    Twitter::Account* twitter_account =
            TwitterNewAccountDialog::get_new_twitter_account(
                this,
                existing_account_ids);

    if (twitter_account)
    {
        // Save state for undo, add the new account object, and mark the data
        // file as 'dirty'.
        save_state_for_undo();
        m_data_file.add(twitter_account);
        m_data_file.set_is_dirty(true);

        // Signal that we're working on the table, and turn off sorting
        // temporarily.
        m_accounts_table_is_being_set_up = true;
        ui->accounts_table->setSortingEnabled(false);

        // Now we can put the Account item in.
        ui->accounts_table->insertRow(0);
        QTableWidgetItem* service_item =
                new QTableWidgetItem(
                    twitter_account->get_service_name().c_str()),
            *id_item =
                new QTableWidgetItem(twitter_account->get_id().c_str());

        // Disable editing for the items we are inserting.
        service_item->setFlags(service_item->flags() ^ Qt::ItemIsEditable);
        id_item->setFlags(id_item->flags() ^ Qt::ItemIsEditable);

        ui->accounts_table->setItem(0, 0, id_item);
        ui->accounts_table->setItem(0, 1, service_item);

        // Turn on table sorting, and resize the columns to fit.
        ui->accounts_table->setSortingEnabled(true);
        ui->accounts_table->resizeColumnsToContents();

        // Signal that we're done with the Accounts table
        m_accounts_table_is_being_set_up = false;
    }   // end if we got a new Twitter account from this

    MASTER_CATCH("Add new Account");
}   // end on_account_add_button_clicked

void MainWindow::on_account_delete_button_clicked(void)
{
    MASTER_TRY

    // Retrieve the selected row numbers, then assemble the list of accounts
    // to delete.
    std::set<int> rows_to_delete;
    QList<QTableWidgetItem*> selected_items =
        ui->accounts_table->selectedItems();

    // Note that the std::set container will eliminate duplicates, because we
    // are processing each selected ITEM, and there are
    for (auto item : selected_items)
        rows_to_delete.insert(item->row());

    // Now assemble a set of Account IDs to delete.
    AccountIdentifiers account_ids_to_delete;
    for (auto row : rows_to_delete)
    {
        // Form the account ID from the item text in the table.
        QTableWidgetItem* user_id_item = ui->accounts_table->item(row, 0),
                *service_item = ui->accounts_table->item(row, 1);

        account_ids_to_delete.insert(
                    std::make_pair(
                        service_item->text().toStdString(),
                        user_id_item->text().toStdString()));
    }

    // Now we have our list of account IDs to delete. Make sure we have
    // something to delete, and get confirmation from the user.
    std::stringstream question_msg;
    question_msg << "You are about to delete " <<
                    account_ids_to_delete.size() <<
                    " Account record(s). Do you wish to continue?";

    if (!account_ids_to_delete.empty() &&
            (QMessageBox::question(this, "Delete Account(s)",
            question_msg.str().c_str()) == QMessageBox::Yes))
    {
        save_state_for_undo();
        m_data_file.erase_accounts(account_ids_to_delete);

        // Redo the accounts table from scratch.
        setup_accounts_table();
    }   // end if we have something to delete, and the user confirmed.
    MASTER_CATCH("Delete Account record");
}   // end on_account_delete_button_clicked method

void MainWindow::setup_accounts_table(void)
{
    // Signal that the Accounts table is being set up.
    m_accounts_table_is_being_set_up = true;

    // Clear the table and turn off sorting temporarily.
    while (ui->accounts_table->rowCount())
        ui->accounts_table->removeRow(0);

    ui->accounts_table->setSortingEnabled(false);

    // Set up the columns.
    ui->accounts_table->setColumnCount(2);
    QStringList fields;
    fields << "Username / ID" << "Service";
    ui->accounts_table->setHorizontalHeaderLabels(fields);

    // Put the account IDs into the table
    AccountIdentifiers ids;
    m_data_file.get_account_ids(ids);
    for (auto account_id : ids)
    {
        // Insert a new row for the account in the table (remember that these
        // will be sorted later).
        ui->accounts_table->insertRow(0);

        // Create the items for the table cells.
        QTableWidgetItem* service_item =
                new QTableWidgetItem(account_id.first.c_str()),
            *id_item = new QTableWidgetItem(account_id.second.c_str());

        // Make sure the items are not editable.
        service_item->setFlags(service_item->flags() ^ Qt::ItemIsEditable);
        id_item->setFlags(id_item->flags() ^ Qt::ItemIsEditable);

        // Put the new item objects into the table (it takes ownership of the
        // objects so that we don't have to.
        ui->accounts_table->setItem(0, 0, id_item);
        ui->accounts_table->setItem(0, 1, service_item);
    }   // end account ID loop

    // Everything is in place - we can turn sorting back on, with all the
    // other trimmings.
    ui->accounts_table->sortItems(0);
    ui->accounts_table->horizontalHeader()->setSortIndicatorShown(true);
    ui->accounts_table->setSortingEnabled(true);

    // Resize the columns to fit.
    ui->accounts_table->resizeColumnsToContents();

    // Signal that we are done setting up the Accounts table.
    m_accounts_table_is_being_set_up = false;
}   // end setup_accounts_table
