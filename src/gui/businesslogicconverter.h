/**
 * \file businesslogicconverter.h Declares the BusinessLogicConverter
 * class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QVariant>
#include <QHash>
#include <QList>
#include <QString>

#include <business-logic/Item.h>

#ifndef BUSINESSLOGICCONVERTER_H
#define BUSINESSLOGICCONVERTER_H

/**
 * \brief A class with a number of static methods for converting between
 * Qt-native types and SocOut business logic
 *
 * These conversions add a certain amount of overhead to processing, but
 * allow us to preserve good separation between the business logic library
 * and Qt, and also allow these type to be used in signals and slots.
 */
class BusinessLogicConverter
{
public:

    /**
     * \brief Convert the given Item to a QHash<String, QVariant> object
     *
     * The hash has the following structure:
     *
     *      "datetime": date/time (QDateTime)
     *      "headline": <headline> (QString)
     *      "digest": <digest> (QString)
     *      "text": <text> (QString)
     *      "profile-image-url": <URL> (QString)
     *      "author": <author> (QString)
     *
     * \todo This method does not convert item metadata, which we may need in
     * the future
     *
     * \param item The item to convert
     *
     * \param hash The result hash into which the items are placed; note that
     * any existing items are NOT cleared prior to filling
     *
     * \return A reference to `hash`
     */
    static QHash<QString, QVariant>& to_qhash(
            const Item& item,
            QHash<QString, QVariant>& hash);

    /**
     * \brief Convert a given QHash to an Item
     *
     * The hash must have the structure described in the documentation for
     * the `to_qhash` method.
     *
     * \todo Item metadata is not transferred. We may need this in the
     * future.
     *
     * \param hash The hash structure to convert
     *
     * \param item The item to fill with data
     *
     * \return A reference to `item`
     *
     * \throw Error A problem occurred during conversion
     */
    static Item& to_item(const QHash<QString, QVariant>& hash,
            Item &item);

    /**
     * \brief Convert an Items container to a QList<QVariant> container
     *
     * \param items The items container
     *
     * \param list The list into which the items are to be placed; note that
     * this is not emptied prior to filling
     *
     * \return A reference to `list`
     */
    static QList<QVariant> to_qlist(
            const Items& items,
            QList<QVariant>& list);

    /**
     * \brief Convert a QList<QVariant> container to an Items container
     *
     * \param list The QList to convert
     *
     * \param items The Items container into which the items are to be
     * placed; this is not emptied prior to filling
     *
     * \return A reference to `items`
     *
     * \throw Error A problem occurred during the conversion
     */
    static Items& to_items(const QList<QVariant>& list, Items& items);
};  // end BusinessLogicConverter

#endif // BUSINESSLOGICCONVERTER_H
