/**
 * \file Declares the ProgressTask and ProgressDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QDialog>
#include <QRunnable>
#include <QMutex>

#include <utilities/MutexGuard.h>
#include <business-logic/ProgressMonitor.h>

/**
 * \brief A base-class for a lengthy task that needs to be performed
 * in the background
 *
 * This class implements the QRunnable interface. Classes derived from
 * this class are instantiated (using `new`) and the pointer is
 * supplied to the `exec` method (an overload of `QDialog::exec`). The
 * Task object is executed by the application thread pool, passing
 * progress notifications back to this dialog (via signals and slots).
 *
 * The thread pool deletes the task object automatically, after its `run`
 * method is complete. Prior to this, the task object needs to notify
 * this dialog that it is complete.
 *
 * When implementing this interface, remember that the `run` method needs
 * to be overridden to execute the task. Its signature cannot change, so you
 * cannot pass arguments to it. Instead, you need to implement the class so
 * that any arguments that the task requires to run are passed to the
 * constructor to be stored in the object. The `run` method can then access
 * these as it needs to.
 *
 * The only other method that needs to be overridden is
 * `signal_task_halted_early`. This allows you to take action if the task
 * did not complete because the user cancelled it. Depending on the task, it
 * may not be necessary to do anything in this circumstance, in which case
 * you can simply implement an empty method.
 */
class ProgressTask :
        public QObject,
        public QRunnable,
        public ProgressMonitor
{
    Q_OBJECT

public:

    /**
     * Trivial constructor
     *
     * Sets runnable auto-deletion to `true`.
     */
    ProgressTask(void) :
        QObject(),
        QRunnable(),
        ProgressMonitor(),
        m_stop_signalled_mutex(),
        m_stop_signalled(false)
    { setAutoDelete(true); }

    /**
     * \brief Trivial destructor
     */
    virtual ~ProgressTask(void) {}

    /**
     * \brief Raise the `stop_signalled` flag
     */
    void set_stop_signalled(void)
        { GUARD(m_stop_signalled_mutex); m_stop_signalled = true; }

    /**
     * \brief Check the value of thje `stop_signalled` flag
     *
     * \return True is stop has been signalled
     */
    bool is_stop_signalled(void) const
        { GUARD(m_stop_signalled_mutex); return m_stop_signalled; }

    // -- Inherited from QRunnable --

    /**
     * \brief The method to run
     *
     * Naturally, this method runs in the background thread, so normal
     * thread-safety precautions apply. Signal can be emitted at any
     * time.
     *
     * This method should not throw any exceptions - the
     * 'finished_with_error' signal can be emitted to signal an error.
     */
    virtual void run(void) override = 0;

    // -- Inherited from ProgressMonitor --

    /**
     * \brief Report on the progress of a task
     *
     * If the Task object is passed as a ProgressMonitor to a lengthy task,
     * this method will be called periodically throughout the task, and is
     * used to emit the `progress` signal (to update the ProgressDialog
     * object).
     *
     * \param p An integer between 0 and 100, indicating percentage progress
     * through the task.
     *
     * \return True if the process should halt; false otherwise
     */
    virtual bool report_progress(int p) override;

    /**
     * \brief Signal that the monitored task was not completed due to
     * cancellation
     *
     * The monitored task will call this method if it is stopping prior to
     * completion due to the `report_progress` method returning `true`. Note
     * that if that method returned `true` to halt the process, but the task
     * completed anyway, then this method will not be called. Likewise, if
     * the task halted early, but not because the method returned `true`,
     * then this method is not called either (but the task method would have
     * thrown an exception in this case).
     */
    virtual void signal_task_halted_early(void) = 0;

signals:

    /**
     * \brief Signal progress
     *
     * This signal is emitted to update the progress dialog.
     *
     * \param value The value to set in the progress bar; values are
     * clipped between 0 and 100
     */
    void progress(int value);

    /**
     * \brief Signal that the task is complete
     *
     * This should be emitted just prior to the `run` method finishing
     */
    void finished(void);

    /**
     * \brief Signals that the task has finished with an error
     *
     * This should be emitted by the run method when is terminates with
     * an error.
     *
     * \param message The error message
     */
    void finished_with_error(const QString& message);

public slots:

    /**
     * \brief Signals that the user wants the task to stop (raises the
     * `stop_signalled` flag)
     *
     * Note: when implementing this, it may be necessary to implement
     * thread-safety measures if a flag is being raised by this method,
     * and checked in the `run` loop.
     */
    void should_stop(void) { set_stop_signalled(); }

private:

    /**
     * \brief A mutex protecting the `stop` flag
     */
    mutable QMutex m_stop_signalled_mutex;

    /**
     * \brief The `stop` flag that signals that the task should finish early
     *
     * This is not accessed directly, but via the `is_stop_signalled` and
     * `set_stop_signalled` methods.
     */
    bool m_stop_signalled;
};  // end ProgressTask class

namespace Ui {
class ProgressDialog;
}

/**
 * \brief A dialog for running a lengthy task, and showing its progress
 *
 * This class is used via the `execute` static method. First of all, a class
 * that implements the ProgressTask interface needs to be created, and
 * instantiated using `new`. This is passed to the `execute` method, which
 * hooks up the progress monitoring signals and slots between the dialog and
 * the task, and then passes the task over to the application threadpool for
 * execution.
 *
 * The task object must be allocated using `new`, and is automatically
 * deleted by the thread pool when it is complete.
 */
class ProgressDialog : public QDialog
{
    Q_OBJECT

public:

    /**
     * \brief Set the task executing, and show the dialog to monitor it
     *
     * This method instantiates the dialog object, and then makes all the
     * signal and slot connections between the dialog and the task object.
     * It then hands the task object over to the thread pool for execution.
     *
     * The task object should be instantiated using `new`, and will be
     * deleted by the thread pool after it is complete.
     *
     * \param parent The QWidget that is the parent of the dialog
     *
     * \param task A pointer to the task that will be executed in the
     * backgound; this should be allocated using `new`, and will be deleted
     * automatically by the thread pool when it is complete
     *
     * \param title The human-readable title for the task that is displayed
     * in the dialog
     *
     * \return The result code (QDialog::Accepted if everything finished OK,
     * or QDialog::Rejected if something when wrong, or the user cancelled)
     */
    static int execute(
            QWidget* parent,
            ProgressTask* task,
            const std::string& title);

    // -- Constructor / Destructor --

protected:

    /**
     * \brief Constructor - sets up the UI
     *
     * Note that this method is not called externally - it is called via
     * the `execute` static method.
     *
     * \param task_name This is the name of the task that is displayed in
     * the title box
     *
     * \param parent Pointer to the parent of the dialog
     */
    explicit ProgressDialog(QWidget *parent, const std::string& title);

public:

    /**
     * \brief Destructor - shuts down the dialog
     */
    ~ProgressDialog(void);

protected slots:

    /**
     * \brief Progress has been made
     *
     * This signal is received when the task has made progress
     *
     * \param value The value to set in the progress bar; values are
     * clipped between 0 and 100
     */
    void progress(int value);

    /**
     * \brief Signal that the task is complete
     *
     * This method closes the dialog (with `accept`) when the task emits
     * a `finished` signal.
     *
     * This should be emitted just prior to the `run` method finishing
     */
    void finished(void);

    /**
     * \brief Signals that the task has finished with an error
     *
     * This signal is emitted by the run method when is terminates with
     * an error. This slot implements this by displaying the error message,
     * and then closing the dialog (using `reject`).
     *
     * \param message The error message
     */
    void finished_with_error(const QString &message);

signals:

    /**
     * \brief The task should stop
     *
     * This signal is emitted when the user clicks on the 'cancel' button
     * in the dialog. It is sent to the task to tell it that it should stop
     * its processing.
     */
    void should_stop(void);

private slots:

    /**
     * \brief Signals the task that it should finish early
     *
     * This method is called when the user clicks on the "Cancel" button.
     * Note that this dialog is not closed by this method - instead the
     * `cancel_button_clicked` flag is raised.
     */
    void on_cancel_button_clicked();

private:

    /**
     * \brief Pointer to the Qt-created UI framework
     */
    Ui::ProgressDialog *ui;

    /**
     * \brief The human-readable title of the task
     */
    std::string m_title;

    /**
     * \brief Whether or not the user clicked the cancel button
     */
    bool m_cancel_button_clicked;
};  // end ProgressDialog class

#endif // PROGRESSDIALOG_H
