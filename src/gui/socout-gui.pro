#-------------------------------------------------
#
# Project created by QtCreator 2015-04-09T13:56:34
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = socout-gui
TEMPLATE = app

# Make sure MOC doesn't try to parse the Boost libraries - it creates a
# whole lotta errors.
load(moc)
QMAKE_MOC += -DBOOST_MPL_IF_HPP_INCLUDED

INCLUDEPATH += $$PWD/../
INCLUDEPATH += $$PWD/../third-party/yaml-cpp/include
INCLUDEPATH += $$PWD/../third-party/boost
INCLUDEPATH += $$PWD/../third-party/curl/include
INCLUDEPATH += $$PWD/../third-party/
INCLUDEPATH += $$PWD/../third-party/jsoncpp

SOURCES += main.cpp\
        mainwindow.cpp \
    persondialog.cpp \
    twitternewaccountdialog.cpp \
    mainwindow_accounts.cpp \
    mainwindow_timeline.cpp \
    itemdialog.cpp \
    progressdialog.cpp \
    itemwidget.cpp \
    configurationdialog.cpp \
    timelinespecificationdialog.cpp \
    timelineview.cpp \
    retrieveaccountitemstask.cpp \
    businesslogicconverter.cpp

HEADERS  += mainwindow.h \
    persondialog.h \
    errorhandling.h \
    twitternewaccountdialog.h \
    itemdialog.h \
    progressdialog.h \
    itemwidget.h \
    configurationdialog.h \
    timelinespecificationdialog.h \
    timelineview.h \
    retrieveaccountitemstask.h \
    businesslogicconverter.h

FORMS    += mainwindow.ui \
    persondialog.ui \
    twitternewaccountdialog.ui \
    itemdialog.ui \
    progressdialog.ui \
    itemwidget.ui \
    configurationdialog.ui \
    timelinespecificationdialog.ui \
    timelineview.ui

win32:CONFIG(release, debug|release): \
    LIBS += -L$$PWD/../../build/lib/release/ \
    -lutilities \
    -lyaml-cpp \
    -lbusiness-logic \
    -ltwitcurl \
    -ljsoncpp \
    -l$$PWD/../third-party/curl/lib/x64/libcurl_a
else:win32:CONFIG(debug, debug|release): \
    LIBS += -L$$PWD/../../build/lib/debug/ \
    -lutilities \
    -lyaml-cpp \
    -lbusiness-logic \
    -ltwitcurl \
    -ljsoncpp \
    -l$$PWD/../third-party/curl/lib/x64/libcurl_a_debug
else:unix: \
    LIBS += -L$$PWD/../../build/lib/ \
    -lutilities \
    -lyaml-cpp \
    -lbusiness-logic \
    -ltwitcurl \
    -ljsoncpp \
    -l$$PWD/../third-party/curl/lib/x64/libcurl_a

DEPENDPATH += $$PWD/../

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../build/lib/release/libutilities.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../build/lib/debug/libutilities.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../build/lib/release/utilities.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../build/lib/debug/utilities.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../build/lib/libutilities.a

RESOURCES += \
    resources.qrc

RC_ICONS = socout.ico

OTHER_FILES +=
