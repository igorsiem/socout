/**
 * \file persondialog.h Declares the PersonDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERSONDIALOG_H
#define PERSONDIALOG_H

#include <set>
#include <QDialog>
#include <business-logic/Person.h>

namespace Ui {
class PersonDialog;
}

/**
 * \brief Encapsulates a dialog for editing the attributes of a Person object
 */
class PersonDialog : public QDialog
{
    Q_OBJECT

public:

    /**
     * \brief Constructor, initialising the dialog
     *
     * \param person A reference to the Person object being edited
     *
     * \param existing_emails The set of existing e-mail addresses in the
     * collection, so that a unique e-mail address may be validated
     *
     * \param A pointer to the parent widget
     */
    explicit PersonDialog(
            Person& person,
            const std::set<std::string>& existing_emails,
            QWidget *parent = 0);

    /**
     * \brief Destructor - shuts down the dialog
     */
    ~PersonDialog(void);

protected slots:

    /**
     * \brief Perform validation before closing
     *
     * This method is called when the dialog is being accepted (OK button
     * being pushed). It checks the dialog fields for valid input, and
     * displays a message if there is a problem (and does not call the
     * base-class `accept` method).
     *
     * If everything is OK, the base-class `accept` method is called.
     */
    virtual void accept(void) override;

private:

    /**
     * \brief A pointer to the internally-generated UI structure
     */
    Ui::PersonDialog *ui;

    /**
     * \brief The Person object being edited
     */
    Person& m_person;

    /**
     * \brief The collection of e-mail addresses already existing, so we can
     * ensure a unique one
     */
    const std::set<std::string>& m_existing_emails;
};  // end PersonDialog class

#endif // PERSONDIALOG_H
