/**
 * \file retrieveaccountitemstask.h Implements the RetrieveAccountItemsTask
 * class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <business-logic/twitter/Twitter.h>
#include <utilities/Log.h>

#include "retrieveaccountitemstask.h"
#include "errorhandling.h"
#include "businesslogicconverter.h"

void RetrieveAccountItemsTask::run(void)
{
    try
    {
        emit started();

        LOG_DEBUG(__FUNCTION__ << " - commencing retrieval of items "
                  "for account \"" << m_account->get_id() <<
                  "\" (" << m_account->get_service_name() << ")");

        Items items;

        // Which service is our account for?
        //
        // TODO At the moment, we only support Twitter
        if (m_account->get_service_name() == "Twitter")
            Twitter::get_twitter().get_recent(*m_account, items);
        else RAISE_ERROR("The account with ID \"" <<
                         m_account->get_id() << "\" is for an "
                         "unrecognised service called \"" <<
                         m_account->get_service_name() << "\".");

        // Convert to a QVariant / QList and emit to the other process.
        QList<QVariant> list;
        emit items_retrieved(
                    QVariant(
                        BusinessLogicConverter::to_qlist(items ,list)));

        // Also emit a signal that we are finished.
        emit finished(false);

        LOG_DEBUG(__FUNCTION__ << " - retrieved " << items.size() <<
                  " item(s)");
    }
    catch (const std::exception& error)
    {
        emit finished_with_error(error.what());
    }
    catch(...)
    {
        emit finished_with_error("An unrecognised error was signalled.");
    }
}   // end run method
