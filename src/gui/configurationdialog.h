/**
 * \brief Declares the ConfigurationDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIGURATIONDIALOG_H
#define CONFIGURATIONDIALOG_H

#include <QDialog>

namespace Ui {
class ConfigurationDialog;
}

/**
 * \brief A dialog for editing the application config
 */
class ConfigurationDialog : public QDialog
{
    Q_OBJECT

public:

    /**
     * \brief Constructor - sets up the UI
     *
     * \param A pointer to the parent widget for the dialog
     */
    explicit ConfigurationDialog(QWidget *parent = 0);

    /**
     * \brief Shuts down the UI
     */
    ~ConfigurationDialog();

protected slots:

    /**
     * \brief Validates the dialog info, puts it into the Configuration
     * object, and writes it to the file
     */
    virtual void accept(void) override;

private slots:

    /**
     * \brief Allows the user to select a filename for the log file
     */
    void on_log_filename_select_button_clicked(void);

private:

    /**
     * \brief The UI framework set up by Qt
     */
    Ui::ConfigurationDialog *ui;
};  // end ConfigurationDialog class

#endif // CONFIGURATIONDIALOG_H
