/**
 * \file Declares the ItemDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QStringList>
#include <QVariant>

#include "itemdialog.h"
#include "ui_itemdialog.h"
#include "errorhandling.h"

ItemDialog::ItemDialog(Item &item,
        const AccountIdentifiers &account_ids,
        AccountIdentifiers &selected_account_ids,
        QWidget *parent) :
    QDialog(parent),
    m_item(item),
    m_account_ids(account_ids),
    m_selected_account_ids(selected_account_ids),
    ui(new Ui::ItemDialog)
{
    ui->setupUi(this);

    // Set up the list of account ids.
    for (auto account_id : m_account_ids)
    {
        // Set up the listbox item for the account. Make it user-checkable.
        std::stringstream account_name;
        account_name << account_id.second << " (" << account_id.first << ")";

        QListWidgetItem* item =
                new QListWidgetItem(account_name.str().c_str());
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);

        // Is this account already selected?
        if (m_selected_account_ids.find(account_id) !=
                m_selected_account_ids.end())
        {
            item->setCheckState(Qt::Checked);
        }
        else item->setCheckState(Qt::Unchecked);

        // Put together a QStringList QVariant that we can use to easily
        // identify the account later.
        QStringList account_id_strl = {
            account_id.first.c_str(),
            account_id.second.c_str() };

        item->setData(Qt::UserRole, QVariant(account_id_strl));

        // Add the item to the accounts list. Note that the list widget
        // takes ownership of the list item object, so we don't need to
        // worry about deleting it.
        ui->accounts_list->insertItem(0, item);
    }   // end account loop

    // Sort, but do not enable 'live' sorting, because the check state
    // changes it.
    ui->accounts_list->sortItems();

    // Set up the text fields from the item as well. If it is a new item,
    // they will be empty.
    // ui->headline_edit->setText(m_item.get_headline().c_str());
    ui->digest_edit->setPlainText(m_item.get_digest().c_str());
    // ui->text_edit->setPlainText(m_item.get_text().c_str());

    // TODO The headline and text items will be enabled in future iterations.
}   // end constructor

ItemDialog::~ItemDialog(void)
{
    delete ui;
}   // end destructor

int ItemDialog::edit(
        Item& item,
        const AccountIdentifiers& account_ids,
        AccountIdentifiers& selected_account_ids,
        QWidget* parent)
{
    ItemDialog dlg(item, account_ids, selected_account_ids, parent);
    return dlg.exec();
}   // end edit method

void ItemDialog::accept(void)
{
    MASTER_TRY

    // At this point, we clear the selected accounts list. All the ones that
    // were selected before had a corresponding checked entry in the list
    // box, and now we want to make sure that only what is selected in the
    // list box is now in our selected set.
    m_selected_account_ids.clear();

    // Go through the accounts in the list box, looking for checked items.
    for (int i = 0; i < ui->accounts_list->count(); i++)
    {
        // Get out the list item, and see if it is checked.
        QListWidgetItem* account_item = ui->accounts_list->item(i);
        if (account_item->checkState() == Qt::Checked)
        {
            QVariant item_data = account_item->data(Qt::UserRole);
            QStringList account_id_strl = item_data.toStringList();

            m_selected_account_ids.insert(
                        std::make_pair(
                            account_id_strl[0].toStdString(),
                            account_id_strl[1].toStdString()));
        }   // end if the list item is checked.
    }   // end accounts list item loop

    // Are any account selected?
    if (m_selected_account_ids.empty())
        RAISE_ERROR("You have not selected any accounts to which this item "
                    "is to be posted.");

    // Check that we have at least one part of our item.
    if (/* ui->headline_edit->text().isEmpty() && */
            ui->digest_edit->toPlainText().isEmpty() /* &&
            ui->text_edit->toPlainText().isEmpty() */)
        RAISE_ERROR("The item has no message content.");

    // Everything looks OK.
    m_item = Item(
                time(NULL),
                /* ui->headline_edit->text().toStdString() */ "",
                ui->digest_edit->toPlainText().toStdString(),
                /* ui->text_edit->toPlainText().toStdString() */ "");

    QDialog::accept();

    MASTER_CATCH("Item");
}   // end accept
