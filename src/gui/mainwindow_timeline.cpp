/**
 * \file mainwindow_timeline.cpp MainWindow methods related to Timeline stuff
 *
 * Note that this is one of a number of .cpp files implemeting methods that
 * are declared in the mainwindow.h file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <memory>
#include <iomanip>
#include <ctime>

#include <QDateTime>

#include <business-logic/Error.h>
#include <business-logic/twitter/Twitter.h>
#include <business-logic/Timeline.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "errorhandling.h"
#include "itemdialog.h"
#include "progressdialog.h"
#include "itemwidget.h"
#include "timelinespecificationdialog.h"

void MainWindow::update_timeline_tab_names(void)
{
    // Loop through the list of timeline tabs, and update them.
    for (auto timeline_view_itr : m_timeline_views)
    {
        ui->tabs->setTabText(
                    timeline_view_itr.first,
                    timeline_view_itr.second->get_timeline_name().c_str());
    }
}   // end update_timeline_tab_names method

void MainWindow::on_action_new_timeline_triggered(void)
{
    MASTER_TRY

    // Create a new timeline, and edit it.
    Timelines timelines = m_data_file.get_timelines();
    Timeline timeline;
    AccountIdentifiers account_ids;
    TimelineSpecificationDialog dlg(
                timeline,
                m_data_file.get_account_ids(account_ids),
                timelines,
                this);

    if (dlg.exec() == QDialog::Accepted)
    {
        // We already know that the timeline object is valid and has a unique
        // name.
        save_state_for_undo();
        timelines.insert(std::make_pair(timeline.get_name(), timeline));
        m_data_file.set_timelines(timelines);
        m_data_file.set_is_dirty(true);

        // Open a new tab for the timeline.
        open_timeline(timeline.get_name(), dlg.refresh_checked());
    }   // end if the user completed a new and valid timeline

    MASTER_CATCH("New Timeline");
}   // end on_action_new_timeline_triggered method

void MainWindow::when_timeline_delete(QString timeline_name)
{
    MASTER_TRY

    // Get out the timelines collection.
    Timelines timelines = m_data_file.get_timelines();
    Timelines::iterator timeline_itr =
            timelines.find(timeline_name.toStdString());
    if (timeline_itr == timelines.end())
        RAISE_ERROR("A timeline with the name \"" <<
                    timeline_name.toStdString() << "\" could not be "
                    "located.");

    // Find the index of the tab with the timeline name.
    for (auto timeline_view_itr : m_timeline_views)
    {
        TimelineView* timeline_view = timeline_view_itr.second;
        if (timeline_view->get_timeline_name() ==
                timeline_name.toStdString())
        {
            ui->tabs->removeTab(timeline_view_itr.first);
        }   // end if this is the timeline view we're looking for
    }   // end timeline view loop

    // Now we remove it from the list of timelines and update the datafile.
    timelines.erase(timeline_itr);
    save_state_for_undo();
    m_data_file.set_timelines(timelines);
    m_data_file.set_is_dirty(true);

    MASTER_CATCH("Delete Timeline");
}   // end when_timeline_delete method

void MainWindow::on_action_post_item_triggered(void)
{
    MASTER_TRY

    // Get the account IDs list, so that the user can choose one.
    AccountIdentifiers account_ids, selected_account_ids;
    m_data_file.get_account_ids(account_ids);

    if (account_ids.empty())
        RAISE_ERROR("You have not added any Accounts yet. Go to the "
                    "Accounts page and add at least one Account, or "
                    "load your SocOut data file containing Account "
                    "records, before attempting to post an item.");

    // Create a new empty item object for the user to post.
    Item item(0, "", "", "");

    // Edit the new item.
    if (ItemDialog::edit(item, account_ids, selected_account_ids, this) ==
            QDialog::Accepted)
    {
        // Retrieve all our selected accounts.
        AccountSharedPtrList accounts;
        m_data_file.get_account_copies(selected_account_ids, accounts);

        // Loop through all of our accounts to post our item.
        for (auto account : accounts)
        {
            // What type of account is this?
            //
            // TODO At the moment, we only have Twitter.
            //
            // TODO This bit is a candidate for MT
            if (account->get_service_name() == "Twitter")
                Twitter::get_twitter().post(*account, item);
            // TODO: New service types go here
            else RAISE_ERROR("The account \"" << account->get_id() <<
                             "\" has unrecognised Service name \"" <<
                             account->get_service_name() << "\".");
        }   // end account loop
    }   // end if the user selected OK

    MASTER_CATCH("Post Item");
}   // end on_action_post_item_triggered

void MainWindow::open_timeline(
        const std::string timeline_name,
        bool refresh)
{
    // Make sure that we have named a valid timeline.
    Timelines timelines = m_data_file.get_timelines();
    if (timelines.find(timeline_name) == timelines.end())
        RAISE_ERROR("A timeline with the name \"" << timeline_name << "\" "
                    "does not exist.");

    // Open the tab, connect up its signals and slot,s and remember its
    // index.
    TimelineView* view = new TimelineView(timeline_name, m_data_file, this);
    connect(
                view,
                SIGNAL(request_delete_timeline(QString)),
                this,
                SLOT(when_timeline_delete(QString)));
    int index = ui->tabs->addTab(view, timeline_name.c_str());
    ui->tabs->setCurrentIndex(index);
    m_timeline_views.insert(std::make_pair(index, view));

    // If we're refreshing the timeline as well, make that happen.
    if (refresh) view->on_refresh_button_clicked();
}   // end open_timeline method

void MainWindow::setup_timeline_tabs(void)
{
    // Clear any existing timeline tabs.
    for (auto view_itr : m_timeline_views)
    {
        // Remove the tab, and delete its widget.
        ui->tabs->removeTab(view_itr.first);
        delete view_itr.second;
    }

    m_timeline_views.clear();

    // Recreate the tabs from the timelines collection.
    Timelines timelines = m_data_file.get_timelines();

    for (auto timeline : timelines)
        open_timeline(timeline.first);
}   // end setup_timeline_tabs
