/**
 * \file mainwindow.h Declares the MainWindow UI class
 *
 * Note that there is more than one .cpp file implementing the methods
 * of this class. All these files have filenames starting with `mainwindow`
 * and ending in `.cpp`.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <stack>
#include <map>

#include <QMainWindow>
#include <QCloseEvent>
#include <QTableWidgetItem>
#include <QListWidgetItem>
#include <QProgressBar>

#include <business-logic/DataFile.h>
#include <business-logic/Item.h>

#include "timelineview.h"

namespace Ui {
class MainWindow;
}

/**
 * \brief Encapsulates the main window of the SocOut application
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

    // --- External Interface ---

public:

    // -- Constructor / Destructor --
    //
    // These methods are implemented in mainwindow.cpp.
    /**
     * \brief Constructs the main window and initialises everything
     *
     * \param parent The parent of this window (generally NULL).
     */
    explicit MainWindow(void);

    /**
     * \brief Destructor - shuts the UI down
     */
    ~MainWindow(void);

    // --- General Management ---
    //
    // These methods are implemented in the `mainwindow.cpp` file.

public slots:

    /**
     * \brief A background task has started
     *
     * This updates the internal counter of active background tasks, as well
     * as the progress indicator in the status bar.
     */
    void background_task_commenced(void);

    /**
     * \brief A background task was finished
     *
     * This method updates the internal counter of active background tasks,
     * as well as the progress indicator in the status bar.
     *
     * \param stoppped_early True if the task was stopped because the user
     * cancelled it
     */
    void background_task_finished(bool stopped_early);

    /**
     * \brief A background task finished with an error
     *
     * This method updates the internal counter of active background tasks,
     * and displays an error message.
     *
     * \param error_message A human-readable description of the error message
     */
    void background_task_finished_with_error(const QString& error_message);

private:

    /**
     * \brief Updates the background processing progress bar
     *
     * This method checks the values of the m_background_tasks_count and
     * m_completed_background_tasks_count variables, and updates the
     * m_background_tasks_pb progress bar accordingly. The logic has the
     * following rules:
     *
     *  1.  If:
     *
     *      a.  the total tasks count is greater than 2 *and*
     *
     *      b.  the completed tasks are less than the total tasks
     *
     *      then the progress bar is is shown, with the progress displayed.
     *
     *  2.  If either condition in (1) is false, then the progress bar is
     *      hidden.
     *
     *  3.  If the completed tasks equal the total tasks, then both are reset
     *      to zero.
     */
    void update_background_task_processing_display(void);

private slots:

    /**
     * \brief Closes the main window, which closes the application.
     */
    void on_action_exit_triggered();

protected:

    /**
     * \brief Called when the window is closing - checks whether the data
     * file needs saving
     */
    virtual void closeEvent(QCloseEvent *event) override;

    // -- Declarations Related to the Data File --
    //
    // These methods are implemented in mainwindow.cpp

private slots:

    /**
     * \brief Create a new data file
     */
    void on_action_new_triggered(void);

    /**
     * \brief Open an existing data file
     *
     * This is lengthy operation. It is executed in a background thread,
     * with a progress dialog.
     */
    void on_action_open_triggered();

    /**
     * \brief Save the data file to disk
     */
    void on_action_save_triggered(void);

    /**
     * \brief Save the data file to disk, asking for
     */
    void on_action_save_as_triggered(void);

    /**
     * \brief Undo the most recent change to the data file
     */
    void on_action_undo_triggered(void);

    /**
     * \brief Redo the most recent undone change to the data file
     */
    void on_action_redo_triggered(void);

    /**
     * \brief Allows the user to edit the configuration options
     */
    void on_action_options_triggered(void);

    // -- Account-related Methods --
    //
    // These methods are implemented in the file `mainwindow_accounts.cpp`

private slots:

    // TODO: GUI stuff for account objects

    /**
     * \brief Allows the user to add a new account
     *
     * \todo Only Twitter accounts are supported at this stage - need to
     * support other account types
     */
    void on_account_add_button_clicked(void);

    /**
     * \brief Delete an Account object
     */
    void on_account_delete_button_clicked(void);

private:

    /**
     * \brief Set up the accounts table the GUI from scratch
     *
     * Note that the table widget is cleared first.
     */
    void setup_accounts_table(void);

    // -- Timeline-related Methods --
    //
    // These methods are found in mainwindow_timeline.cpp

public:

    /**
     * \brief Update the names of the timeline tabs, to correspond witht the
     * timeline names in the widgets
     */
    void update_timeline_tab_names(void);

private slots:

    /**
     * \brief Allow the user to post a new Item
     *
     * \todo This bit takes time - it needs to be done in a background
     * thread.
     */
    void on_action_post_item_triggered(void);

    /**
     * \brief Allows the user to create a new timeline specification
     */
    void on_action_new_timeline_triggered(void);

    /**
     * \brief Deletes a named timeline
     *
     * \param timeline_name The name of the timeline to delete
     */
    void when_timeline_delete(QString timeline_name);

private:

    /**
     * \brief Create a tab with a TimelineView widget
     *
     * \param timeline_name The name of the timeline; note that this should
     * be a timeline that is already in the timelines collection in the data
     * file
     *
     * \param refresh Load / refresh the timeline parameters
     */
    void open_timeline(
            const std::string timeline_name,
            bool refresh = false);

    /**
     * \brief Set up the timeline tabs
     *
     * This method is called when a data file is first created or opened. It
     * clears existing timeline tabs, and creates all the timelines specified
     * in the data file.
     */
    void setup_timeline_tabs(void);

    // -- General Utility Functions --
    //
    // These methods are implemented in the main_window.cpp file.

public:

    /**
     * \brief Save the state of the data file to the undo stack
     */
    void save_state_for_undo(void);

private:

    /**
     * \brief Set up all elements of the GUI from scratch
     *
     * This method clears all the GUI elements (like the people table and
     * so on, and reconstructs it from the data file. It calls various
     * specialised set-up methods (like `setup_people_table`. It is called
     * when we want to rebuild the (such as when a data file is loaded, or
     * when we process an undo or redo action).
     */
    void setup_gui(void);

    /**
     * \brief Undo the previous action (if there is one in the stack)
     *
     * Note that the the current state of the system is saved to the redo
     * stack before restoring from the undo stack.
     */
    void undo_last_action(void);

    /**
     * \brief Redo the previously undone action (if there is one in the
     * stack)
     */
    void redo_last_undo(void);

    /**
     * \brief Enable / disable the undo/redo actions in accordance with the
     * state of the undo/redo stacks
     */
    void set_undo_redo_action_enablement(void);

    // --- Attributes ---

private:

    /**
     * \brief The internal UI structure for the main window
     */
    Ui::MainWindow *ui;

    /**
     * \brief The main data file, containing all the users data
     */
    DataFile m_data_file;

    /**
     * \brief Signals that the People table is being set up, or loaded from a
     * disk file
     *
     * This flag is checked when a signal comes through that a table item has
     * changed. The change should be ignored by the update code when it
     * originates from a file-load operation.
     */
    bool m_people_table_is_being_set_up;

    /**
     * \brief Signals that the Accounts table is being set up or loaded from
     * a disk file
     *
     * This flag is checked when table update / change signals are being
     * processed, so we know to ignore them during the time that the table
     * is being set up.
     */
    bool m_accounts_table_is_being_set_up;

    /**
     * \brief The stack of undo states (implemented using a deque)
     *
     * Each of the objects in this stack is an entire copy of the data file
     * prior to an edit operation, that can then be 'undone' by simply
     * popping that copy of the file off the stack.
     *
     * This is not particularly efficient in terms of memory usage, but it is
     * simple and requires little maintenance. We may revisit this design
     * choice in the future.
     *
     * The back of the deque is treated as the 'top' of the stack. The
     * container is trimmed to ten items at all times.
     *
     * Note that this collection need not be made thread-safe as it will only
     * ever be accessed from the main GUI thread.
     */
    std::deque<DataFile> m_undo_stack;

    /**
     * \brief The stack of redo states (implemented using a deque)
     *
     * Each of the objects in this stack is an entire copy of the data file
     * prior to an undo operation, so that what was undone can be 'redone',
     * simply by popping the copy of the data file of the stack.
     *
     * This is not particularly efficient in terms of memory usage, but it is
     * simple and requires little maintenance. We may revisit this design
     * choice in the future.
     *
     * The back of the deque is treated as the 'top' of the stack. The
     * container is trimmed to ten items at all times.
     *
     * Note that this collection need not be made thread-safe as it will only
     * ever be accessed from the main GUI thread.
     */
    std::deque<DataFile> m_redo_stack;

    /**
     * \brief The timeline view widgets currently being displayed, indexed by
     * their tab indices
     */
    std::map<int, TimelineView*> m_timeline_views;

    /**
     * \brief Progress bar for monitoring background tasks in the status bar
     */
    QProgressBar* m_background_tasks_pb;

    /**
     * \brief Total number of background tasks
     *
     * This is incremented each time a background task is commenced. It is
     * reset to zero when the number of completed tasks reaches the total
     * number of tasks.
     */
    int m_background_tasks_count;

    /**
     * \brief Total number of completed background tasks
     *
     * This is incremented each time a background task is completed. When
     * the number of completed tasks equals the total number of tasks,
     * everything is reset to zero.
     */
    int m_completed_background_tasks_count;
};  // end MainWindow class

#endif // MAINWINDOW_H
