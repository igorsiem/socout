/**
 * \file itemwidget.cpp Implements the ItemWidget class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <iomanip>

#include <QDateTime>
#include <QPainter>
#include <QException>
#include <QUrl>
#include <QByteArray>
#include <QRegExp>

#include <utilities/Log.h>

#include "itemwidget.h"
#include "ui_itemwidget.h"
#include "mainwindow.h"

ItemWidget::ItemWidget(const Item& item,
        MainWindow *mainwindow,
        QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ItemWidget),
    m_item(item),
    m_preferred_height(0),
    m_profile_image(NULL),
    m_network_access_manager()
{
    ui->setupUi(this);

    ui->author_label->setText(item.get_author().c_str());

    // Format the date and time as a stream.
    std::stringstream item_strm;
    QDateTime datetime;
    datetime.setTime_t(item.get_datetime());
    datetime = datetime.toLocalTime();

    // TODO This is horrible, Igor - clean it up!
    item_strm << std::setfill('0') << std::setw(4) <<
                 datetime.date().year() << "-" << std::setfill('0') <<
                 std::setw(2) << datetime.date().month() << "-" <<
                 std::setfill('0') << std::setw(2) <<
                 datetime.date().day() << " " << std::setfill('0') <<
                 std::setw(2) << datetime.time().hour() << ":" <<
                 std::setfill('0') << std::setw(2) <<
                 datetime.time().minute() << ":" << std::setfill('0') <<
                 std::setw(2) << datetime.time().second();

    ui->date_time_label->setText(item_strm.str().c_str());

    if (item.get_headline().empty())
        ui->headline_label->hide();
    else ui->headline_label->setText(item.get_headline().c_str());

    // Put together the text content.
    QString digest_and_text = item.get_digest().c_str();
    if (!digest_and_text.isEmpty() && !item.get_text().empty())
        digest_and_text += "\n\n";
    digest_and_text += item.get_text().c_str();

    // Substitute hyperlinks using a regexp
    QRegExp url_finder("((ht|f)tp(s?)\\:\\/\\/[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z"
                       "])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\'\\/\\\\"
                       "\\+&amp;%\\$#_]*)?)");
    digest_and_text.replace(url_finder, "<a href=\"\\1\">\\1</a>");

    ui->digest_and_text_browser->setHtml(digest_and_text);

    // Calculate what the size of the widget should be.
    //
    // TODO This seem pretty dodgy and arbitrary. We'll leave it for now,
    // but it will definitely need a second (and maybe a third and fourth)
    // look.
    ui->digest_and_text_browser->document()->setTextWidth(
                ui->digest_and_text_browser->size().width());

    int digest_and_text_height =
            ui->digest_and_text_browser->document()->size().height() / 7;

    int headline_height = ui->headline_label->size().height();
    if (ui->headline_label->isHidden()) headline_height = 0;

    m_preferred_height = 100 + headline_height +
            digest_and_text_height;

    // Connect up our signals and slots to the main window.
    connect(
            this,
            SIGNAL(avatar_download_started()),
            mainwindow,
            SLOT(background_task_commenced()));

    connect(
            this,
            SIGNAL(avatar_finished_downloading(bool)),
            mainwindow,
            SLOT(background_task_finished(bool)));

    // If we have an image URL, set off the avatar image download.
    if (item.get_profile_image_url().empty() == false)
    {
        // First, connect up the NAM's completion signal to our processing
        // slot, so that we are notified when the download is complete.
        connect(
            &m_network_access_manager,
            SIGNAL(finished(QNetworkReply*)),
            this,
            SLOT(process_downloaded_avatar_image_data(QNetworkReply*)));

        // Now we can actually do the download.
        QNetworkRequest request(QUrl(item.get_profile_image_url().c_str()));
        m_network_access_manager.get(request);

        emit avatar_download_started();

        LOG_DEBUG("Downloading profile image " <<
                  item.get_profile_image_url());
    }   // end if we have a profile image URL
}   // end constructor

ItemWidget::~ItemWidget(void)
{
    // If we have a profile image object, delete it.
    if (m_profile_image)
    {
        delete m_profile_image;
        m_profile_image = NULL;
    }
    else
    {
        // The profile image is still downloading. Emit a signal saying
        // that it has finished downloading, so that the main window
        // monitoring of background tasks is kept up to date.
        emit avatar_finished_downloading(true);
    }

    delete ui;
}   // end destructor

void ItemWidget::paintEvent(QPaintEvent *event)
{
    if (m_profile_image)
    {
        QPainter painter(this);

        // Scale the image to the size that we've allowed for it in the
        // widget using the spacer.
        int hmargin = ui->horizontalLayout->contentsMargins().left(),
                vmargin = ui->horizontalLayout->contentsMargins().top();
        QRect target_rect(
            hmargin,
            vmargin,
            ui->profile_image_spacer->sizeHint().width() - hmargin,
            ui->profile_image_spacer->sizeHint().width() - vmargin);

        painter.drawImage(
                    target_rect,
                    *m_profile_image,
                    m_profile_image->rect());
    }

    QWidget::paintEvent(event);
}   // end paintEvent method

void ItemWidget::process_downloaded_avatar_image_data(QNetworkReply* reply)
{
    LOG_DEBUG("Profile image " << m_item.get_profile_image_url() <<
              " downloaded successfully.");

    // Instantiate the image object, and fill it with our downloaded bytes.
    m_profile_image = new QImage;
    m_profile_image->loadFromData(reply->readAll());

    // Update our display
    QWidget::update();

    emit avatar_finished_downloading(false);
}   // end process_downloaded_avatar_image_data
