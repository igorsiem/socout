/**
 * \file persondialog.h Implements the PersonDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QRegExp>

#include <business-logic/Error.h>

#include "persondialog.h"
#include "ui_persondialog.h"
#include "errorhandling.h"

PersonDialog::PersonDialog(
        Person& person,
        const std::set<std::string>& existing_emails,
        QWidget *parent
        ) :
    QDialog(parent),
    ui(new Ui::PersonDialog),
    m_person(person),
    m_existing_emails(existing_emails)
{
    ui->setupUi(this);
}   // end constructor

PersonDialog::~PersonDialog(void)
{
    delete ui;
}   // end destructor

void PersonDialog::accept(void)
{
    MASTER_TRY

    // Check person properties and update the Person object. Throw an
    // exception if there is a problem.

    // Validate the e-mail address against a RegExp for e-mail addresses.
    // This one is taken from http://www.regular-expressions.info/email.html
    QRegExp email_reg(
        "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$",
        Qt::CaseInsensitive);

    if (email_reg.exactMatch(ui->email_edit->text()) == false)
        RAISE_ERROR("E-mail address is invalid. Please use a valid e-mail "
                    "address structure.");

    // Make sure the e-mail address is unique.
    std::string email = ui->email_edit->text().toStdString();
    if (m_existing_emails.find(email) != m_existing_emails.end())
        RAISE_ERROR("The e-mail address \"" << email << " is already in "
                    "use.");

    // All validated now.
    m_person.set_first_name(ui->first_name_edit->text().toStdString());
    m_person.set_last_name(ui->last_name_edit->text().toStdString());
    m_person.set_email(email);

    // Call the parent accept method to execute the validated close. Any
    // errors will have been caught and displayed as exceptions along the
    // way.
    QDialog::accept();

    MASTER_CATCH("Person");
}   // end accept method
