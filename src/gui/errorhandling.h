/**
 * \file Declares some error-handling macros
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMessageBox>
#include <QException>

#include <utilities/Log.h>
#include <business-logic/Error.h>

#ifndef ERRORHANDLING_H
#define ERRORHANDLING_H

/**
 * \brief A 'try' for a 'master try/catch block' for displaying errors at
 * the top level of the GUI
 */
#define MASTER_TRY try {

/**
 * \brief A 'catch everything' construct for displaying error messages
 * at the top level of the GUI
 *
 * \param titlebar_text The text to put in the title bar of the message box
 */
#define MASTER_CATCH( titlebar_text ) \
} \
catch (const Error& e) \
{ \
    QMessageBox::critical( \
        this, \
        titlebar_text, \
        e.what()); \
    LOG_ERROR(e.what()); \
} \
catch (const QException& e) \
{ \
    QMessageBox::critical( \
        this, \
        titlebar_text, \
        e.what()); \
    LOG_ERROR(e.what()); \
} \
catch(const std::exception& e) \
{ \
    QMessageBox::critical( \
        this, \
        titlebar_text, \
        e.what()); \
    LOG_ERROR(e.what()); \
} \
catch(...) \
{ \
    QMessageBox::critical( \
        this, \
        titlebar_text, \
        "An unrecognised error occurred."); \
    LOG_ERROR("An unrecognised error occurred."); \
}

#endif // ERRORHANDLING_H
