/**
 * \file itemwidget.h Declares the ItemWidget class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ITEMWIDGET_H
#define ITEMWIDGET_H

#include <QWidget>
#include <QImage>
#include <QRunnable>
#include <QUrl>
#include <QMutex>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <business-logic/Item.h>
#include <utilities/MutexGuard.h>

class MainWindow;

namespace Ui {
class ItemWidget;
}

/**
 * \brief A widget for displaying the content of an Item
 *
 * We note that the term "item" is used in more than one way. The `Item`
 * class (in the SocOut business logic framework) is a piece of content that
 * is posted on a social media `Service`.
 */
class ItemWidget : public QWidget
{
    Q_OBJECT

public:

    /**
     * \brief Constructor - sets up everything
     *
     * \todo One thing this method does is calculate the 'preferred height'
     * of the widget, based on the item content. At this moment, this
     * algorithm is pretty arbitray, and definitely requires more study.
     *
     * \param item The item to be displayed
     *
     * \param mainwindow A pointer to the main window object; this is used
     * to connect the widget object's signals for its background tasks to
     * the slots that the main window uses to monitor their progress
     *
     * \param parent The parent widget
     */
    ItemWidget(const Item& item,
            MainWindow* mainwindow,
            QWidget *parent = 0);

    /**
     * \brief Destructor - shuts down the UI
     */
    ~ItemWidget(void);

    /**
     * \brief Retrieve the preferred height of the widget
     *
     * \return The preferred height
     */
    int get_preferred_height(void) const { return m_preferred_height; }

    /**
     * \brief Paint the widget
     *
     * This method is override so that we can do our graphical stuff.
     *
     * \param event The event object
     */
    virtual void paintEvent(QPaintEvent * event) override;

signals:

    /**
     * \brief Signal that the widget has commenced downloading its avatar (in
     * a background task)
     *
     * This signal can be connected up to the main window's
     * `background_task_commenced` slot.
     */
    void avatar_download_started(void);

    /**
     * \brief Signal that the avatar has finished downloading
     *
     * This can be connected up to the background_task_finished slot of the
     * main window, to signal that a background task has finished.
     *
     * \param stopped_early The task finished early, because of a `stop`
     * signal (this is always set to false)
     */
    void avatar_finished_downloading(bool stopped_early);

public slots:

    /**
     * \brief Process a download avatar image data and display it
     *
     * This method is called by the network access manager when it has
     * downloaded the image data for the avatar.
     */
    void process_downloaded_avatar_image_data(QNetworkReply* reply);

private:

    /**
     * The Qt-generated UI framework
     */
    Ui::ItemWidget *ui;

    /**
     * \brief The item being displayed
     */
    Item m_item;

    /**
     * \brief The preferred height of the widget, calculated from the text
     * content
     */
    int m_preferred_height;

    /**
     * \brief The profile image
     *
     * This is may or may not be present. It is set using the
     * `set_profile_image` slot method, and will be drawn if it is present.
     * If it is present on destruction, the image is deleted properly.
     */
    QImage* m_profile_image;

    /**
     * \brief The network access manager used to download additional
     * resources
     */
    QNetworkAccessManager m_network_access_manager;
};  // end ItemWidget class

#endif // ITEMWIDGET_H
