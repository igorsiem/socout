/**
 * \file main.cpp The main method of the application
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>

#include <QApplication>
#include <QException>

#include <utilities/Log.h>

#include "mainwindow.h"
#include <business-logic/Error.h>
#include <business-logic/Configuration.h>

// Stop the CLI from coming up in Windows
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

/**
 * \brief The entry point for the application
 *
 * \param argc The number of command-line arguments
 *
 * \param argv The vector of command-line arguments
 *
 * \return Non-zero on error
 */
int main(int argc, char *argv[])
{
    int result = 0;

    // Catch ALL errors, and send them to the console. All other reporting
    // is done via the logs.
    try
    {
        // Initialise the configuration.
        Configuration& config = Configuration::get_configuration();
        config.initialise(argc, argv);

        // Set up logging before we do anything else.
        if (config.get_console_log_level() != Log::level_none)
            Log::get_log().add_log_stream(
                        config.get_console_log_level(),
                        std::cout);

        if (config.get_file_log_level() != Log::level_none)
            Log::get_log().open_log_file(
                        config.get_file_log_level(),
                        config.get_file_log_filename());

        LOG_INFO("--- SocOut log begins ---");

        // Derive some config stuff.
        LOG_INFO("Config file location (if present): " <<
                 config.get_config_filename());
        LOG_INFO("Log file location (if active): " <<
                 config.get_file_log_filename());

        // Now we can start out Qt application and main window.
        QApplication a(argc, argv);
        MainWindow w;
        w.show();

        result = a.exec();
    }
    catch (const Error& e)
    {
        std::cerr << "[ERROR] " << e.what() << std::endl;
        result = -1;
    }
    catch (const QException& e)
    {
        std::cerr << "[ERROR] Qt exception: " << e.what() << std::endl;
        result = -2;
    }
    catch (const std::exception& e)
    {
        std::cerr << "[ERROR] Standard exception: " << e.what() << std::endl;
        result = -3;
    }
    catch (...)
    {
        std::cerr << "[ERROR] *** Unrecognised exception" << std::endl;
        result = -4;
    }

    LOG_INFO("--- SocOut log ends with result " << result << " ---");

    return result;
}   // end main function
