/**
 * \file twitternewaccountdialog.cpp Implements the TwitterNewAccountDialog
 * class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <business-logic/Error.h>

#include "twitternewaccountdialog.h"
#include "ui_twitternewaccountdialog.h"
#include "errorhandling.h"

TwitterNewAccountDialog::TwitterNewAccountDialog(
        QWidget *parent,
        const AccountIdentifiers &existing_account_ids) :
    QDialog(parent),
    ui(new Ui::TwitterNewAccountDialog),
    m_account(NULL),
    m_existing_account_ids(existing_account_ids)
{
    ui->setupUi(this);
}   // end constructor

TwitterNewAccountDialog::~TwitterNewAccountDialog(void)
{
    if (m_account)
    {
        delete m_account;
        m_account = NULL;
    }

    delete ui;
}   // end destructor

Twitter::Account* TwitterNewAccountDialog::get_new_twitter_account(
        QWidget *parent,
        const AccountIdentifiers& existing_account_ids)
{
    // Create a new dialog instance.
    TwitterNewAccountDialog dlg(parent, existing_account_ids);
    if (dlg.exec() == QDialog::Accepted)
    {
        // Get the new Account pointer out, and clear the internal one of
        // the dialog (so that the account object does not get deleted by
        // destructor of the dialog object.
        Twitter::Account* account = dlg.m_account;
        dlg.m_account = NULL;

        return account;
    }
    else return NULL;
}   // end get_new_twitter_account

void TwitterNewAccountDialog::accept(void)
{
    MASTER_TRY

    // Check that we have non-empty account credentials.
    if (ui->username_edit->text().isEmpty())
        RAISE_ERROR("Twitter username may not be empty.");

    if (ui->password_edit->text().isEmpty())
        RAISE_ERROR("Twitter password may not be empty.");

    // Check that we haven't got this account already.
    if (m_existing_account_ids.find(
                std::make_pair(
                    Twitter::get_twitter().get_name(),
                    ui->username_edit->text().toStdString())) !=
            m_existing_account_ids.end())
        RAISE_ERROR("The Twitter account \"" <<
                    ui->username_edit->text().toStdString() << "\" has "
                    "already been added to SocOut.");

    // Everything is validated. Now we can attempt to get full account
    // credentials. An exception will be thrown at this point if
    // authentication fails.
    //
    // TODO Account checking can take a while, and locks the UI - we probably
    // want to do this in a background thread.
    m_account = Twitter::get_twitter().authenticate_account(
                ui->username_edit->text().toStdString(),
                ui->password_edit->text().toStdString());

    QDialog::accept();

    MASTER_CATCH("Twitter Account Details");
}   // end accept
