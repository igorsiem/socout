/**
 * \file Declares the ItemDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ITEMDIALOG_H
#define ITEMDIALOG_H

#include <QDialog>

#include <business-logic/Item.h>
#include <business-logic/Account.h>

namespace Ui {
class ItemDialog;
}

/**
 * \brief A dialog for editing an item
 *
 * This dialog is for editing a content Item. Note that it is constructed
 * and used via the `edit_item` static method, and cannot be constructed
 * directly.
 *
 * \todo Make the `ItemDialog` resize and reconfigure itself to take into
 * account the various services that the Item may be going to.
 */
class ItemDialog : public QDialog
{
    Q_OBJECT

protected:

    /**
     * \brief Constructor - sets up the UI
     *
     * \param item The item being edited
     *
     * \param account_ids The list of account identifiers, so that the user
     * can select which accounts to post on
     *
     * \param selected_account_ids The list of accounts that the user has
     * selected to post the item to; this is not emptied prior to filling,
     * and if it contains items, these will be checked in the dialog list
     * box
     *
     * \param parent A pointer to the parent widget
     */
    explicit ItemDialog(
            Item& item,
            const AccountIdentifiers& account_ids,
            AccountIdentifiers& selected_account_ids,
            QWidget *parent = 0);

    /**
     * \brief Destructor - shuts down the UI
     */
    ~ItemDialog(void);

public:

    /**
     * \brief Edit an Item object
     *
     * This method constructs and executes the ItemDialog to edit a SocOut
     * Item object (which can be newly constructed and empty).
     *
     * \param item The Item to edit
     *
     * \param account_ids The list of account identifiers, so that the user
     * can select which accounts to post on
     *
     * \param selected_account_ids The list of accounts that the user has
     * selected to post the item to; this is not emptied prior to filling,
     * and if it contains items, these will be checked in the dialog list
     * box
     *
     * \param parent A pointer to the parent widget
     *
     * \return The return code for the Dialog (QDialog::Accepted if OK)
     */
    static int edit(
            Item& item,
            const AccountIdentifiers& account_ids,
            AccountIdentifiers& selected_account_ids,
            QWidget* parent);

protected slots:

    /**
     * \brief Perform validation before closing
     *
     * This method validates the fields, ensuring that at least one account
     * has been selected. Note that at least one of the headline, digest or
     * text fields need to be non-blank.
     *
     * If everything is OK, the base-class `accept` method is called.
     */
    virtual void accept(void) override;

private:

    /**
     * \brief The UI items generated by the Qt framework
     */
    Ui::ItemDialog *ui;

    /**
     * \brief The item being edited
     */
    Item& m_item;

    /**
     * \brief The set of account identifiers so that the user can select
     * which to post
     */
    const AccountIdentifiers& m_account_ids;

    /**
     * \brief The set of account identifiers that the user has selected
     * for the item to be posted to
     */
    AccountIdentifiers& m_selected_account_ids;
};

#endif // ITEMDIALOG_H
