/**
 * \file twitternewaccountdialog.h Declares the TwitterNewAccountDialog class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TWITTERNEWACCOUNTDIALOG_H
#define TWITTERNEWACCOUNTDIALOG_H

#include <QDialog>

#include <business-logic/Account.h>
#include <business-logic/twitter/Twitter.h>

namespace Ui {
class TwitterNewAccountDialog;
}

/**
 * \brief A dialog for entering the details of a twitter account
 *
 * This dialog allows the user to enter the details of a Twitter account, and
 * then does the work of authenticating the account and producing a new
 * Account object (using the Twitter service class). Note that this class is
 * intended only for entering new Twitter account details - not editing an
 * existing account.
 */
class TwitterNewAccountDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * \brief Constructor, initialising the dialog UI
     *
     * \param The parent widget object
     *
     * \param existing_account_ids The collection of account IDs that we already
     * have
     */
    explicit TwitterNewAccountDialog(
            QWidget *parent,
            const AccountIdentifiers& existing_account_ids);

    /**
     * \brief Destructor - shuts down the dialog UI
     */
    ~TwitterNewAccountDialog(void);

    /**
     * \brief Execute the dialog, authenticating a Twitter account for use
     * with SocOut
     *
     * \param parent The parent widget of the dialog instance
     *
     * \param existing_account_ids The list of account IDs that already
     * exist; we don't want to validate the same account twice
     *
     * \todo Account authentication can take a while, and locks up the UI in
     * the process. We probably want to put this in a background thread, and
     * display a nice spinny wheel or something.
     *
     * \return A pointer to a new Twitter Account object (allocated using
     * `new`) if an account was successfully authenticated, or `NULL` if the
     * user cancelled
     */
    static Twitter::Account* get_new_twitter_account(
            QWidget *parent,
            const AccountIdentifiers& existing_account_ids);

protected slots:

    /**
     * \brief Perform validation before closing
     *
     * This method ensures that the username and password are not blank, and
     * then attempts to create a new Account object, using the Twitter
     * service.
     *
     * If everything is OK, the base-class `accept` method is called.
     */
    virtual void accept(void) override;

private:

    /**
     * \brief The Qt user interface framework
     */
    Ui::TwitterNewAccountDialog *ui;

    /**
     * \brief The Twitter account being created
     *
     * This object is created using new, when everything has been validated.
     * It is passed back to the caller after execution by the
     * `get_new_twitter_account` static method, after which deleting the new
     * account becomes the caller's responsibility. Prior to that, it is THIS
     * object's responsibility to delete the account object if something has
     * gone wrong.
     */
    Twitter::Account* m_account;

    /**
     * \brief A list of existing account IDs that we check to avoid having
     * the same account twice
     */
    const AccountIdentifiers& m_existing_account_ids;
};  // end TwitterNewAccountDialog class

#endif // TWITTERNEWACCOUNTDIALOG_H
