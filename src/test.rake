# Rake file for running all tests
#
# Note that the test executables are not built by this script. This is
# simply for running the tests.

# Test fixtures need to be copied to the release binaries directory every
# time testing is run (because some fixtures get changed during testing).
# Create individual rules for these.
fixture_targets = []
fixture_filenames = [
    "test-config-file.yml",
    "test_config_file_parsing-config.yml",
    "test_config_file_write-empty-config.yml",
    "test_config_file_write-items-config.yml"
]
fixtures_target_dir = "#{$build_dir}/test-fixtures"
fixtures_source_dir = "src/test/fixtures"

directory fixtures_target_dir

# Prepare for tests
task :configure_tests => fixtures_target_dir do
    # Copy fixture files.
    fixture_filenames.each do |filename|
        source_filename = "#{fixtures_source_dir}/#{filename}"
        target_filename = "#{fixtures_target_dir}/#{filename}"
        
        puts "Copying #{filename}..."
        FileUtils.cp(source_filename, target_filename)
        puts "    ... done"
    end # fixtures filename loop
end

# Run all the tests
task :run_tests, [:online] => [:binaries, :configure_tests] do |t, args|
    args.with_defaults :online => ""
    
    # Check the online argument.
    online = args[:online]
    if online != "" and online != "--online"
        raise "Invalid test option \"#{online}\""
    end
    
    # Work out what the location of the tests is (this is the binaries target
    # directory).
    test_location = $build_dir + "/bin"
    test_location += "/Release" if $platform == :windows

    for test in $tests do
        sh "#{test_location}/#{test} #{online}"
    end
end

namespace :test do
    desc "run all tests (add \"--online\" for online tests)"
    task :all, [:online] => :run_tests
end

desc "run all tests (add \"--online\" for online tests)"
task :test, [:online] => "test:all"

# Debug version of test running
namespace :debug do
    task :run_tests, [:online] => ["debug:binaries", :configure_tests] do |t, args|
        args.with_defaults :online => ""
        
        # Check the online argument.
        online = args[:online]
        if online != "" and online != "--online"
            raise "Invalid test option \"#{online}\""
        end
        
        # Work out what the location of the tests is (this is the binaries target
        # directory).
        test_location = $build_dir + "/bin"
        test_location += "/Debug" if $platform == :windows
    
        for test in $tests do
            sh "#{test_location}/#{test} #{online}"
        end
    end
    
    namespace :test do
        desc "run all debug tests (add \"--online\" for online tests)"
        task :all, [:online] => :run_tests
    end # namespace test
    
    desc "run all debug tests (add \"--online\" for online tests)"
    task :test, [:online] => "test:all"    
end # namespace debug
