/**
 * \file Log.h Implements the Log class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdexcept>

// TODO remove debugging code
#include <iostream>

#include "Log.h"
#include "MutexGuard.h"

std::string Log::to_string(level ll)
{
    switch (ll)
    {
    case level_none: return "NONE";
    case level_error: return "ERROR";
    case level_warning: return "WARNING";
    case level_info: return "INFO";
    case level_debug: return "DEBUG";
    default:
        {
            std::stringstream msg;
            msg << "The logging level code " <<
                static_cast<int>(ll) << " was not recognised.";
            throw std::runtime_error(msg.str());
        }
    };  // end switch on level
}   // end to_string method
    
Log::level Log::to_level(const std::string& lls)
{
    if (lls == "NONE") return level_none;
    else if (lls == "ERROR") return level_error;
    else if (lls == "WARNING") return level_warning;
    else if (lls == "INFO") return level_info;
    else if (lls == "DEBUG") return level_debug;
    else throw std::runtime_error("The logging level \"" + lls + "\" was "
        "not recognised.");
}   // end to_level method

Log& Log::get_log(void)
{
    static Log l;
    return l;
}   // end get_log

void Log::add_log_stream(level logging_level, std::ostream& ostrm)
{
    GUARD(m_mutex);
    m_logs.insert(std::pair<level, std::ostream*>(logging_level, &ostrm));
}   // end add_log_stream

void Log::open_log_file(level logging_level, const std::string& filename)
{
    std::ofstream* file = new std::ofstream(filename.c_str());
    if (!file)
        throw std::runtime_error("Could not open file \"" + filename +
            "\" for writing log messages");
    
    // Note: `add_log_stream` locks the mutex.
    add_log_stream(logging_level, *file);
}   // end open_log_file

void Log::clear(void)
{
    GUARD(m_mutex);

    for (auto i : m_file_logs)
        delete i;

    m_file_logs.clear();
    m_logs.clear();
}   // end clear

void Log::log_message(level logging_level, const std::string& message)
{
    GUARD(m_mutex);

    for (auto& itr : m_logs)
        if ((int)itr.first >= (int)logging_level)
            *itr.second << message << std::endl;
}   // end log_message

Log::Log(void) : m_logs(), m_file_logs(), m_mutex()
{
}   // end constructor

Log::~Log(void)
{
    clear();
}   // end destructor
