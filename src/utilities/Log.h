/**
 * \file Log.h Declares the Log class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <map>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

#include <QMutex>

#ifndef Log_h_included
#define Log_h_included

/**
 * \brief A simple thread-safe logging class, with multiple levels of
 * logging and multiple output streams
 *
 * This class is implemented as a singleton, and does not rely on anything
 * except the simple MT synchronisation classes that are part of this
 * library.
 */
class Log
{
    public:

    /**
     * \brief Enumerate the different logging levels
     */
    enum level
    {
        level_none = 0,     ///< No logging
        level_error = 1,    ///< Errors
        level_warning = 2,  ///< Warnings
        level_info = 3,     ///< Information messages
        level_debug = 4     ///< Debugging messages
    };  // end level
    
    /**
     * \brief Convert the given log level to a human-readable string
     *
     * \param ll The logging level enumerator
     *
     * \return The string version of the logging level
     *
     * \throw std::runtime_error `ll` is an unrecognised enum
     */
    static std::string to_string(level ll);
    
    /**
     * \brief Convert the given string to a log level enum
     *
     * \param lls The logging level strimg (must be one of "NONE", "ERROR",
     * "WARNING", "INFO", "DEBUG")
     */
    static level to_level(const std::string& lls);
    
    /**
     * Retrieve an instance to the single logging object
     */
    static Log& get_log(void);

    /**
     * \brief Add an output stream for logging at the given logging level
     *
     * Messages at the given logging level *and below* will be sent to this
     * stream. Note that the Log object will not own this stream, and it will
     * not be deleted when the Log object is destroyed. To create an output
     * log file that will be owned by the Log object automatically, use the
     * open_log_file method.
     *
     * \param logging_level The logging level for this output stream
     *
     * \param ostrm The output stream
     */
    void add_log_stream(level logging_level, std::ostream& ostrm);

    /**
     * \brief Open a new file for logging at the given logging level
     *
     * Messages at the given logging level *and below* will be sent to this
     * file, which will be closed when the Log object is destroyed.
     *
     * \param logging_level The logging level for this output stream
     *
     * \param filename The name of the log file to open
     *
     * \throw std::runtime_error The file could not be opened for writing
     */
    void open_log_file(level logging_level, const std::string& filename);

    /**
     * \brief Clear all logging streams
     */
    void clear(void);

    /**
     * \brief Log a message to the streams at the given logging level
     *
     * \param logging_level The logging level for the message; streams set
     * to this logging level or greater will get the message
     *
     * \param message The message to log; this is suffixed with `std::endl`
     */
    void log_message(level logging_level, const std::string& message);

    private:

    /**
     * \brief Private constructor - sets up initial logging
     */
    Log(void);

    /**
     * \brief Private destructor - makes sure file-based logs are closed
     */
    ~Log(void);

    /**
     * \brief The collection of pointers to output log streams, with their
     * logging levels
     *
     * Note that this collection does not own the output stream objects. The
     * Log object does own the file stream objects, but it will delete them
     * via the pointers in the m_file_logs collection.
     */
    std::map<level, std::ostream*> m_logs;

    /**
     * \brief The collection of pointers to output file streams
     *
     * The Log object owns these file stream objects.
     */
    std::vector<std::ofstream*> m_file_logs;

    /**
     * \brief The mutex that protects logging operations
     */
    QMutex m_mutex;
}; // end class Log

/**
 * \brief Logs a message at a given logging level with a specific logging
 * level prefix (which should match the logging level)
 *
 * \param l The logging level enumerator
 *
 * \param prefix The logging level prefix string
 *
 * \param message The message to log (which can be multiple arguments with
 * stream operators
 */
#define LOG_MESSAGE( l, prefix, message) { \
    std::stringstream msg; \
    msg << "[" << prefix << "] " << message; \
    ::Log::get_log().log_message(l, msg.str()); \
}

/**
 * \brief Log an ERROR-level message
 *
 * \param message The message to log, which can be multiple arguments joined
 * by stream operators
 */
 #define LOG_ERROR( message ) \
    LOG_MESSAGE( ::Log::level_error, "ERR", message )

/**
 * \brief Log an WARNING-level message
 *
 * \param message The message to log, which can be multiple arguments joined
 * by stream operators
 */
 #define LOG_WARNING( message ) \
    LOG_MESSAGE( ::Log::level_warning, "WAR", message )

/**
 * \brief Log an INFO-level message
 *
 * \param message The message to log, which can be multiple arguments joined
 * by stream operators
 */
 #define LOG_INFO( message ) \
    LOG_MESSAGE( ::Log::level_info, "INF", message )

/**
 * \brief Log an DEBUG-level message
 *
 * \param message The message to log, which can be multiple arguments joined
 * by stream operators
 */
 #define LOG_DEBUG( message ) \
    LOG_MESSAGE( ::Log::level_debug, "DEB", message )

 #endif
