/**
 * \file JSONSerialiser.cpp Implements the JSONSerialiser class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "JSONSerialiser.h"

namespace SERIALISATION {
	
/**
 * \brief Convenience macro for raising exceptions with stream arguments
 */
#define RAISE_RUNTIME_ERROR( msg ) { \
    std::stringstream strm; \
    strm << msg; \
    throw std::runtime_error(strm.str()); \
}
	
void JSONSerialiser::write(const Value& v, std::ostream& out)
{
	Json::Value json_value;
	out << to_json_value(v, json_value);
}	// end write method
    
Value& JSONSerialiser::read(std::istream& in, Value& v)
{
	Json::Value json_value;
	in >> json_value;
	return to_value(json_value, v);
}	// end read method
    
Json::Value& JSONSerialiser::to_json_value(
        const Value& v,
        Json::Value& json_value)
{
	// What type of value is this?
	switch (v.get_type())
	{
		case Value::t_nil:
			return json_value = Json::Value();
			break;
			
		case Value::t_bool:
			return json_value = Json::Value(v.get_as_bool()); break;
			
		case Value::t_int:
			json_value = Json::Value(v.get_as_int());
			break;
			
		case Value::t_double:
			json_value = Json::Value(v.get_as_double());
			break;
			
		case Value::t_string:
			json_value = Json::Value(v.get_as_string());
			break;
		
		case Value::t_vector:
		{
			// Build an array node.
			json_value = Json::Value(Json::arrayValue);
			for (auto child : v.as_vector())
			{
				Json::Value json_child;
				json_value.append(to_json_value(child, json_child));
				json_value.append(json_child);
			}
			
			break;
		}
		
		case Value::t_map:
		{
			// Build an object (name/value pairs)
			json_value = Json::Value(Json::objectValue);
			for (auto itr : v.as_map())
			{
				Json::Value json_child;
				json_value[itr.first] =
					to_json_value(itr.second, json_child);
			}
			
			break;
		}
        
        default:
            RAISE_RUNTIME_ERROR("An unrecognised value type was encountered "
                "when atempting to serialise to the JSON format.");
	}	// end switch on value type
	
	return json_value;
}	// end to_json_value method
    
Value& JSONSerialiser::to_value(const Json::Value& json_value, Value& v)
{
	// What type of value do we have?
	switch (json_value.type())
	{
		default:
		case Json::nullValue: v = Value(); break;
		
		case Json::intValue: v = Value(static_cast<int64_t>(json_value.asInt64())); break;
		case Json::uintValue: v = Value(static_cast<int64_t>(json_value.asInt64())); break;
		case Json::realValue: v = Value(json_value.asDouble()); break;
		case Json::stringValue: v = Value(json_value.asString()); break;
		case Json::booleanValue: v = Value(json_value.asBool()); break;
		case Json::arrayValue:
		{
			v = Value(ValueVector());
			for (Json::ArrayIndex i = 0; i < json_value.size(); i++)
			{
				Value child;
				v.as_vector().push_back(to_value(json_value[i], child));
			}
			
			break;
		}
		
		case Json::objectValue:
		{
			v = Value(ValueMap());
			Json::Value::Members names = json_value.getMemberNames();
			for (std::size_t i = 0; i < names.size(); i++)
			{
				Value child;
				v.as_map().insert(
					std::make_pair(
						names[i],
						to_value(json_value[names[i]], child)));
			}
			
			break;
		}
	}
	
	return v;
}	// end to_value method
	
}	// end SERIALISATIONB namespace
