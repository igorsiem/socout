/**
 * \file JSONSerialiser.h Serialiser for JSON data
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <json/json.h>
#include "Serialiser.h"

#ifndef _JSONSerialiser_h_included
#define _JSONSerialiser_h_included

namespace SERIALISATION {

/**
 * \brief A serialiser object for parsing JSON data
 */
class JSONSerialiser final : public Serialiser
{
    public:
    
	/**
	 * \brief Trivial constructor
	 */
	JSONSerialiser(void) : Serialiser() {}
	
	/**
	 * \brief Trivial destructor
	 */
	virtual ~JSONSerialiser(void) {}
	
    /**
     * \brief Writes the given Value object to the given output stream
     *
     * \param v The Value object to write
     *
     * \param out The output stream to which the object is written
     *
     * \throw std::runtime_error A problem occurred in the conversion
     */
    virtual void write(const Value& v, std::ostream& out) override;
    
    /**
     * \brief Reads a Value object from the given input stream
     *
     * \param in The input stream from which to read
     *
     * \param v The Value object into which the data is constructor
     *
     * \return A reference to v (for function chaining)
     *
     * \throw std::runtime_error There was a problem reading or parsing the
     * input stream
     */
    virtual Value& read(std::istream& in, Value& v) override;
    
    // --- Internal Declarations ---
    
    private:
    
    /**
     * \brief Transform a SERIALISATION::Value object into a Json::Value
     * object
     *
     * This method works recursively in the case of Array and Map values.
     *
     * \param v The SERIALISATION::Value object to transform
     *
     * \param json_value A JSON value object to receive the data
     *
     * \return The Json::Value object (json_value)
     *
     * \throw std::runtime_error There was a problem creating the Json Value
     * object
     */
    static Json::Value& to_json_value(
        const Value& v,
        Json::Value& json_value);
    
    /**
     * \brief Transfer a Json::Value object into a SERIALISATION::Value
     *
     * In the case of Json arrays and objects, this method works recursively.
     *
     * \param json_value The Json::Value to transform
     *
     * \param v A reference to the Value object to receive the data
     *
     * \return The SERIALISATION::Value object (v)
     */
    static Value& to_value(const Json::Value& json_value, Value& v);
};	// end JSONSerialiser

}	// end SERIALISATIONB namespace

#endif
