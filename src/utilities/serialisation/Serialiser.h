/**
 * \file Declares the Serialiser class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "Value.h"

#ifndef _serialisation_Serialiser_h_included
#define _serialisation_Serialiser_h_included

namespace SERIALISATION {
	
/**
 * \brief Base-class for a serialisation interface for Value objects
 *
 * Classes derived from this class will read and write value objects from and
 * to std::istream and std::ostream object.
 */
class Serialiser
{
    public:
    
    /**
     * \brief Trivial destructor
     */
    virtual ~Serialiser(void) {}
    
    /**
     * \brief Writes the given Value object to the given output stream
     *
     * \param v The Value object to write
     *
     * \param out The output stream to which the object is written
     *
     * \throw std::runtime_error An error occurred while performing output
     */
    virtual void write(const Value& v, std::ostream& out) = 0;
    
    /**
     * \brief Reads a Value object from the given input stream
     *
     * \param in The input stream from which to read
     *
     * \param v The Value object that will be constructed from the input
     * stream
     *
     * \return A reference to v (for chaining purposes)
     *
     * \throw std::runtime_error There was a problem reading or parsing the
     * input stream
     */
    virtual Value& read(std::istream& in, Value& v)  = 0;
};  // end Serialiser

}	// end namespace SERIALISATION

#endif
