/**
 * \file Value.h Declares the Value class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <vector>
#include <map>
#include <utility>
#include <string>
#include <cstdint>

#ifndef _serialisation_Value_h_included
#define _serialisation_Value_h_included
 
/**
 * \brief A namespace for all Serialisation-specific declarations
 */
namespace SERIALISATION {
	
using namespace std::rel_ops;
	
/**
 * \brief A simple variant type
 *
 * We're deliberately not using a template here, mainly for simplicity, but
 * also because we want to constrain the types that can be used.
 *
 */
class Value final
{
	// --- Public Interface ---
	
	public:
	
	// -- Public subtypes --
	
	/**
	 * \brief Enumerates the different types of base-value that a value can
	 * have
	 */
	enum type
	{
		t_nil,		///< No type
		t_bool,		///< boolean
		t_int,		///< int
		t_double,		///< double
		t_string,		///< std::string
		t_vector,		///< std::vector
		t_map			///< std::map
	};	// end type
	
	/**
	 * \brief A vector of values
	 */
	typedef std::vector<Value> ValueVector;
	
	/**
	 * \brief A map of strings to values
	 */
	typedef std::map<std::string, Value> ValueMap;
	
	// -- Constructor / Destructor --
	
	/**
	 * \brief Default constructor - default to nil type
	 */
	Value(void);
	
	/**
	 * \brief Constructor with bool
	 *
	 * \param b The value to set
	 */
	explicit Value(bool b);

	/**
	 * \brief Constructor with int64
	 *
	 * \param i The value to set
	 */
	explicit Value(int64_t i);
	
	/**
	 * \brief Constructor with int
	 *
	 * \param i The value to set
	 */
	explicit Value(int i);
	
	/**
	 * \brief Constructor with double
	 *
	 * \param d The value to set
	 */
	explicit Value(double d);
	
	/**
	 * \brief Constructor with string
	 *
	 * \param s The string to set
	 */
	explicit Value(const std::string& s);

	/**
	* \brief Constructor with string
	*
	* \param s The string to set
	*/
	explicit Value(const char* s);

	/**
	 * \brief Constructor with vector
	 *
	 * \param v The vector to set
	 */
	explicit Value(const ValueVector& v);
	
	/**
	 * \brief Constructor with map
	 *
	 * \param m The map to set
	 */
	explicit Value(const ValueMap& m);
	
	/**
	 * \brief Destructor - ensures that complex types are cleaned up
	 */
	~Value(void);
	
	// -- Accessors --
	
	/**
	 * \brief Get the 'native' type of the value
	 */
	type get_type(void) const
		{ return m_type; }

	/**
	 * \brief Retrieve value to bool
	 *
	 * \return The converted value
	 *
	 * \throw std::runtime_error Invalid conversion
	 */
	bool get_as_bool(void) const;

	/**
	 * \brief Set value as bool
	 *
	 * \param b The value to set
	 */
	void set(bool b)
		{ make_nil(); m_type = t_bool; m_value.m_bool = b; }
	
	/**
	 * \brief Convert value to int
	 *
	 * \return The converted value
	 *
	 * \throw std::runtime_error Invalid conversion
	 */
	int64_t get_as_int(void) const;
	
	/**
	 * \brief Set value as int
	 *
	 * \param i The value to set
	 */
	void set(int64_t i)
		{ make_nil(); m_type = t_int; m_value.m_int = i; }
	
	/**
	 * \brief Convert value to double
	 *
	 * \return The converted value
	 *
	 * \throw std::runtime_error Invalid conversion
	 */
	double get_as_double(void) const;

	/**
	 * \brief Set value as double
	 *
	 * \param d The value to set
	 */
	void set(double d)
		{ make_nil(); m_type = t_double; m_value.m_double = d; }
	
	/**
	 * \brief Convert value to string
	 *
	 * \return The converted value
	 *
	 * \throw std::runtime_error Invalid conversion
	 */	
	std::string get_as_string(void) const;
	
	/**
	 * \brief Set value as string
	 *
	 * \param s The value to set
	 *
	 * \throw std::runtime_error Invalid conversion
	 */
	void set(const std::string& s)
	{
		make_nil();
		m_type = t_string;
		m_value.m_string = new std::string(s);
	}
	
	/**
	 * \brief Set value as string
	 *
	 * \param s The value to set
	 *
	 * \throw std::runtime_error Invalid conversion
	 */
	void set(const char* s)
	{
		make_nil();
		m_type = t_string;
		m_value.m_string = new std::string(s);
	}
	
	/**
	 * \brief Get value as vector
	 *
	 * If the value is an atomic structure or a string, the conversion
	 * returns a vector with the value as a single element. When converting
	 * from a map, the returned vector contains just the values.
	 *
	 * \return The converted value
	 *
	 * \throw std::runtime_error Invalid conversion
	 */
	ValueVector get_as_vector(void) const;
	
	/**
	 * \brief Set value as vector
	 *
	 * \param v The value to set
	 *
	 * \throw std::runtime_error Invalid conversion
	 */
	void set(const ValueVector& v)
	{
		make_nil();
		m_type = t_vector;
		m_value.m_vector = new ValueVector(v);
	}
	
	/**
	 * \brief Provides access to the internal vector as a reference, IF
	 * the value is a vector
	 *
	 * \return A reference to the ValueVector
	 *
	 * \throw std::runtime_error Value is not a vector
	 */
	ValueVector& as_vector(void);

	/**
	 * \brief Provides access to the internal vector as a reference, IF
	 * the value is a vector
	 *
	 * \return A reference to the ValueVector
	 *
	 * \throw std::runtime_error Value is not a vector
	 */
	const ValueVector& as_vector(void) const;
	
	/**
	 * \brief Get value as map
	 *
	 * Note that conversions from non-map types cause an exception, as there
	 * is no 'natural' way to convert to a map.
	 *
	 * \return The map
	 *
	 *
	 * \throw std::runtime_error Invalid conversion
	 */
	ValueMap get_as_map(void) const;
	
	/**
	 * \brief Set value as map
	 *
	 * \param m The value to set
	 */
	void set(const ValueMap& m)
	{
		make_nil();
		m_type = t_map;
		m_value.m_map = new ValueMap(m);
	}
	
	/**
	 * \brief Provides access to the internal map as a reference, IF
	 * the value is a map
	 *
	 * \return A reference to the ValueMap
	 *
	 * \throw std::runtime_error Value is not a map
	 */
	ValueMap& as_map(void);
		
	/**
	 * \brief Provides access to the internal map as a reference, IF
	 * the value is a map
	 *
	 * \return A reference to the ValueMap
	 *
	 * \throw std::runtime_error Value is not a map
	 */
	const ValueMap& as_map(void) const;
		
	// -- Copy Semantics --
	
	/**
	 * \brief Copy constructor
	 *
	 * \param rhs The object from which to copy
	 */
	Value(const Value& rhs);
	
	/**
	 * \brief Assignment operator
	 *
	 * \param rhs The object from which to copy
	 *
	 * \return A reference to copied self
	 */
	Value& operator=(const Value& rhs);
	
	// -- Comparison Semantics --
	
	/**
	 * \brief Equality comparison operator
	 *
	 * Equality is non-permissive. Values are only compared if they have the
	 * same type.
	 */
	bool operator==(const Value& rhs) const;
	
	// --- Internal Declarations
	
	private:
	
	// -- Internal Utilities --
	
	/**
	 * \brief Make the value nil, clearing any complex data types
	 */
	void make_nil(void);
	
	// -- Attributes --

	/**
	 * \brief The underlying type of the value
	 */	
	type m_type;
	
	/**
	 * \brief A union for the different value types that we have
	 *
	 * Note that complex types are pointer to instances that need to be
	 * deleted.
	 */
	union _Value
	{
		bool m_bool;
		int64_t m_int;
		double m_double;
		std::string* m_string;
		ValueVector* m_vector;
		ValueMap* m_map;
		
		explicit _Value(void) : m_double(0.0) {}
		explicit _Value(bool b) : m_bool(b) {}
		explicit _Value(int64_t i) : m_int(i) {}
		explicit _Value(int i) : m_int(i) {}
		explicit _Value(double d) : m_double(d) {}
		explicit _Value(std::string* s) : m_string(s) {}
		explicit _Value(ValueVector* v) : m_vector(v) {}
		explicit _Value(ValueMap* m) : m_map(m) {}
	};
	
	_Value m_value;
};	// end class Value

typedef Value::ValueVector ValueVector;
typedef Value::ValueMap ValueMap;

}	// end SERIALISATION

#endif
