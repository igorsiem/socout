/**
 * \file YAMLSerialiser.h Serialiser for YAML data
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <yaml-cpp/yaml.h>
#include "Serialiser.h"
 
#ifndef _serialisation_YAMLSerialiser_h_included
#define _serialisation_YAMLSerialiser_h_included

namespace SERIALISATION {

/**
 * \brief A serialiser object for parsing YAML data
 */
class YAMLSerialiser final : public Serialiser
{
    // --- External Interface ---
    
    public:
    
    /**
     * \brief Trivial constructor
     */
    YAMLSerialiser(void) : Serialiser() {}
    
    /**
     * \brief Trivial destructor
     */
    virtual ~YAMLSerialiser(void) {}
    
    /**
     * \brief Writes the given Value object to the given output stream
     *
     * \param v The Value object to write
     *
     * \param out The output stream to which the object is written
     *
     * \throw std::runtime_error A problem occurred in the conversion
     */
    virtual void write(const Value& v, std::ostream& out) override;
    
    /**
     * \brief Reads a Value object from the given input stream
     *
     * \param in The input stream from which to read
     *
     * \param v The Value object into which the data is constructor
     *
     * \return A reference to v (for function chaining)
     *
     * \throw std::runtime_error There was a problem reading or parsing the
     * input stream
     */
    virtual Value& read(std::istream& in, Value& v) override;
    
    
    // --- Internal Declarations ---
    
    private:
    
    /** 
     * \brief Transform a Value object into its equivalent YAML node object
     *
     * This method works recursively to create the YAML node tree
     *
     * \param v The Value object to transform
     *
     * \return The YAML node
     *
     * \throw std::runtime_error There was an error processing the YAML
     * object
     */
    static YAML::Node to_yaml_node(const Value& v);
    
    /**
     * \brief Transform a YAML node into a Value object
     *
     * This method works recursively to create the Value tree.
     *
     * \param node The node to transform
     *
     * \param v The Value object to be constructor
     *
     * \return A reference to v (for function chaining)
     *
     * \throw std::runtime_error An error occurred while processing the Node
     */
    static Value& to_value(
        const YAML::Node& node,
        Value& v); 
};  // end YAMLSerialiser

}	// end SERIALISATION

#endif
