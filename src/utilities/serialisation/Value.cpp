/**
 * \file Value.cpp Implements Value class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include "Value.h"

namespace SERIALISATION {
	
Value::Value(void) :
	m_type(t_nil),
	m_value()
{
}	// end constructor
	
Value::Value(bool b) :
	m_type(t_bool),
	m_value(b)
{
}	// end constructor

Value::Value(int64_t i) :
	m_type(t_int),
	m_value(i)
{
}	// end constructor
	
Value::Value(int i) :
	m_type(t_int),
	m_value(i)
{
}
	
Value::Value(double d) :
	m_type(t_double),
	m_value(d)
{
	
}	// end constructor
	
Value::Value(const std::string& s) :
	m_type(t_string),
	m_value(new std::string(s))
{
}	// end constructor
	
Value::Value(const char* s) :
	m_type(t_string),
	m_value(new std::string(s))
{

}	// end constructor

Value::Value(const ValueVector& v) :
	m_type(t_vector),
	m_value(new ValueVector(v))
{
}	// end constructor
	
Value::Value(const ValueMap& m) :
	m_type(t_map),
	m_value(new ValueMap(m))
{
}	// end constructor

Value::~Value(void)
{
	make_nil();
}	// end destructor

bool Value::get_as_bool(void) const
{
	switch (m_type)
	{
		case t_nil: return false;
		case t_bool: return m_value.m_bool;
		case t_int: return (m_value.m_int != 0);
		case t_double: return (m_value.m_double != 0.0);
		case t_string:
			if (*m_value.m_string == "true") return true;
			else if (*m_value.m_string == "false") return false;
			else return (get_as_int() != 0);
		
		case t_vector:
			throw std::runtime_error("Attempt to convert serialisation "
				"vector to bool.");		
		case t_map:
			throw std::runtime_error("Attempt to convert serialisation "
				"map to bool.");
		default:
			throw std::runtime_error("Unrecognised serialisation "
				"structure type.");						
	}	
}	// end get_as_bool

int64_t Value::get_as_int(void) const
{
	switch (m_type)
	{
		case t_nil: return 0;
		case t_bool: return m_value.m_bool;
		case t_int: return m_value.m_int;
		case t_double: return static_cast<int>(m_value.m_double);
		case t_string: return atoi(m_value.m_string->c_str());
		
		case t_vector:
			throw std::runtime_error("Attempt to convert serialisation "
				"vector to int.");		
		case t_map:
			throw std::runtime_error("Attempt to convert serialisation "
				"map to int.");
		default:
			throw std::runtime_error("Unrecognised serialisation "
				"structure type.");						
	}	// end switch on type
}	// end get_as_int

double Value::get_as_double(void) const
{
	switch (m_type)
	{
		case t_nil: return 0.0;
		case t_bool: return m_value.m_bool;
		case t_int: return static_cast<double>(m_value.m_int);
		case t_double: return m_value.m_double;
		case t_string: return atof(m_value.m_string->c_str());
		
		case t_vector:
			throw std::runtime_error("Attempt to convert serialisation "
				"vector to double.");		
		case t_map:
			throw std::runtime_error("Attempt to convert serialisation "
				"map to double.");
				
		default:
			throw std::runtime_error("VUnrecognised serialisation "
				"structure type.");						
	}	// end switch on type
}	// end get_as_double

std::string Value::get_as_string(void) const
{
	std::stringstream strm;
	
	switch (m_type)
	{
		case t_nil: return "";
		case t_bool: strm << m_value.m_bool; break;
		case t_int: strm << m_value.m_int; break;
		case t_double: strm << m_value.m_double; break;
		case t_string: return *m_value.m_string;
		
		case t_vector:
			throw std::runtime_error("Attempt to convert serialisation "
				"vector to string.");		
		case t_map:
			throw std::runtime_error("Attempt to convert serialisation "
				"map to string.");
		default:
			throw std::runtime_error("Unrecognised serialisation "
				"structure type.");						
	}	// end switch on type
	
	return strm.str();
}	// end get_as_string

Value::ValueVector Value::get_as_vector(void) const
{
	switch (m_type)
	{
		case t_nil: return ValueVector();
		case t_bool:
		case t_int:
		case t_double:
		case t_string:
			return ValueVector({ *this });
		
		case t_vector: return *m_value.m_vector;
				
		case t_map:
		{
			ValueVector v;
			for (auto itr : *m_value.m_map)
				v.push_back(itr.second);
		}
				
		default:
			throw std::runtime_error("Unrecognised serialisation "
				"structure type.");						
	}	// end switch on type
}

ValueVector& Value::as_vector(void)
{
	if (m_type != t_vector)
		throw std::runtime_error("Attempt to access non-vector value as a "
			"vector");
			
	return *m_value.m_vector;
}	// end as_vector

const ValueVector& Value::as_vector(void) const
{
	if (m_type != t_vector)
		throw std::runtime_error("Attempt to access non-vector value as a "
			"vector");
			
	return *m_value.m_vector;	
}	// end as_vector

ValueMap Value::get_as_map(void) const
{
	switch (m_type)
	{
		case t_nil: return ValueMap();
		case t_bool:
			throw std::runtime_error("Attempt to convert serialisation "
				"bool to map.");
		case t_int:
			throw std::runtime_error("Attempt to convert serialisation "
				"int to map.");
		case t_double:
			throw std::runtime_error("Attempt to convert serialisation "
				"double to map.");
		case t_string:
			throw std::runtime_error("Attempt to convert serialisation "
				"string to map.");
		
		case t_vector:
			throw std::runtime_error("Attempt to convert serialisation "
				"bool to map.");
				
		case t_map: return *m_value.m_map;
				
		default:
			throw std::runtime_error("Unrecognised serialisation "
				"structure type.");						
	}	// end switch on type
}	// end get_as_map

ValueMap& Value::as_map(void)
{
	if (m_type != t_map)
		throw std::runtime_error("Attempt to access non-map value as a "
			"map");
			
	return *m_value.m_map;	
}	// end as_map
		
const ValueMap& Value::as_map(void) const
{
	if (m_type != t_map)
		throw std::runtime_error("Attempt to access non-map value as a "
			"map");
			
	return *m_value.m_map;	
}	// end as_map 

Value::Value(const Value& rhs) :
	m_type(t_nil),
	m_value()
{
	switch (rhs.m_type)
	{
		case t_nil: break;
		case t_bool: set(rhs.get_as_bool()); break;
		case t_int: set(rhs.get_as_int()); break;
		case t_double: set(rhs.get_as_double()); break;

		case t_string:
			m_type = t_string;
			m_value.m_string = new std::string(*rhs.m_value.m_string);
			break;
		
		case t_vector:
			m_type = t_vector;
			m_value.m_vector = new ValueVector(*rhs.m_value.m_vector);
			break;
				
		case t_map:
			m_type = t_map;
			m_value.m_map = new ValueMap(*rhs.m_value.m_map);
			break;
				
		default:
			throw std::runtime_error("Unrecognised serialisation "
				"structure type.");						
	}	// end switch on type
}	// end copy constructor

Value& Value::operator=(const Value& rhs)
{
	make_nil();
	
	switch (rhs.m_type)
	{
		case t_nil: break;
		case t_bool: set(rhs.get_as_bool()); break;
		case t_int: set(rhs.get_as_int()); break;
		case t_double: set(rhs.get_as_double()); break;

		case t_string:
			m_type = t_string;
			m_value.m_string = new std::string(*rhs.m_value.m_string);
			break;
		
		case t_vector:
			m_type = t_vector;
			m_value.m_vector = new ValueVector(*rhs.m_value.m_vector);
			break;
				
		case t_map:
			m_type = t_map;
			m_value.m_map = new ValueMap(*rhs.m_value.m_map);
			break;
				
		default:
			throw std::runtime_error("Unrecognised serialisation "
				"structure type.");						
	}	// end switch on type
	
	return *this;
}	// end assignment operator

bool Value::operator==(const Value& rhs) const
{
	// Make sure the types are the same.
	if (m_type == rhs.m_type)
	{
		switch (rhs.m_type)
		{
			case t_bool:
				return (m_value.m_bool == rhs.m_value.m_bool);
			case t_int:
				return (m_value.m_int == rhs.m_value.m_int);
			case t_double:
				return (m_value.m_double == rhs.m_value.m_double);
			case t_string:
				return (*m_value.m_string == *rhs.m_value.m_string);
			case t_vector:
				return (*m_value.m_vector == *rhs.m_value.m_vector);
			case t_map:
				return (*m_value.m_map == *rhs.m_value.m_map);
				
			default:
				throw std::runtime_error("Unrecognised serialisation "
					"structure type.");						
		}	// end switch on type
	}
	else return false;
}	// end operator==

void Value::make_nil(void)
{
	// Delete any associated complex types.
	switch (m_type)
	{
		case t_string: delete m_value.m_string; break;
		case t_vector: delete m_value.m_vector; break;
		case t_map: delete m_value.m_map; break;
		
		default: break;
	}

	m_type = t_nil;	
}	// end make_nil

}	// end SERIALISATION
