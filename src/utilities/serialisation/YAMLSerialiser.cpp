/**
 * \file Implements the YAMLSerialiser class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include <sstream>
 
#include "YAMLSerialiser.h"

namespace SERIALISATION {
	
/**
 * \brief Convenience macro for raising exceptions with stream arguments
 */
#define RAISE_RUNTIME_ERROR( msg ) { \
    std::stringstream strm; \
    strm << msg; \
    throw std::runtime_error(strm.str()); \
}
    
void YAMLSerialiser::write(const Value& v, std::ostream& out)
{
    out << to_yaml_node(v);
}
    
Value& YAMLSerialiser::read(std::istream& in, Value& v)
{
    YAML::Node top = YAML::Load(in);
    return to_value(top, v);
}
    
YAML::Node YAMLSerialiser::to_yaml_node(const Value& v)
{
    // What type of value is it?
    switch (v.get_type())
    {
		case Value::t_nil:
            RAISE_RUNTIME_ERROR("Nil values cannot be serialised to the "
                "YAML format.");
            
		case Value::t_bool:
            return YAML::Node(v.get_as_bool());
            
		case Value::t_int:
            return YAML::Node(v.get_as_int());
            
		case Value::t_double:
            return YAML::Node(v.get_as_double());
        
		case Value::t_string:
            return YAML::Node(v.get_as_string());
        
		case Value::t_vector:
        {
            // Build an array node.
            YAML::Node node;
            for (auto itr : v.as_vector())
                node.push_back(to_yaml_node(itr));
            return node;
        }
        
		case Value::t_map:
        {
            // Build a  map
            YAML::Node node;
            for (auto itr : v.as_map())
                node[itr.first] = to_yaml_node(itr.second);
                
            return node;
        }
        
        default:
            RAISE_RUNTIME_ERROR("An unrecognised value type was encountered "
                "when atempting to serialise to the YAML format.");
    }   // end switch on value type
}   // to_yaml_node
    
Value& YAMLSerialiser::to_value(
        const YAML::Node& node,
        Value& v)   
{
    // Check what kind of node we have:
    switch (node.Type())
    {
        case YAML::NodeType::Scalar:
            // Convert straight to a string data item
            v.set(node.as<std::string>());
            break;
            
        case YAML::NodeType::Sequence:
        {
            // Create a vector, and put values in it, calling this method
            // recursively
            ValueVector value_vector;
            for (auto itr : node)
            {
                Value node_value;
                value_vector.push_back(to_value(itr, node_value));
            }   // end node array loop
            
            v.set(value_vector);
            break;
        }       
        
        case YAML::NodeType::Map:
        {
            // Create a map, and put values into it recursively.
            ValueMap value_map;
            for (auto itr : node)
            {
                Value node_value;
                to_value(itr.second, node_value);
                value_map.insert(
                    std::make_pair(
                        itr.first.as<std::string>(),
                        node_value));
            }   // end node loop
            
            v.set(value_map);
            break;
        }
        
        default:
            // Return an empty value.
            return v = Value();
    }   // end switch on YAML node type
    
    return v;
}   // end to_value

}	// end SERIALISATION
