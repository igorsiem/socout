/**
 * \file ThreadSafeStack.h Declares the ThreadSafeStack template
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stack>
#include <QMutex>

#include "MutexGuard.h"
 
#ifndef _ThreadSafeStack_h_included
#define _ThreadSafeStack_h_included

/**
 * \brief A thread-safe version of std::stack (with some important
 * differences)
 *
 * This implementation wraps a std::stack collection with a QMutex object.
 * This can be replaced with almost any other kind of mutex as long as it
 * works with the Guard class declared in the `MutexGuard.h` file.
 *
 * The main difference between this implementation and std::stack is that
 * element access is by value / copy, rather than by reference (to prevent
 * MT-related issues.
 *
 * Copy semantics are also non-trivial, as they need to lock the mutex of the
 * object being copied to ensure consistency in an MT context. In the case of
 * assignment, the mutex of the destination object (self) is locked as well,
 * for the same reason.
 *
 * \param T The element type
 */
template <class T>
class ThreadSafeStack
{
	// --- External Interface ---
	
	public:
	
	// -- Constructors and Destructors --
	
	/**
	 * \brief Trivial destructor
	 */
	ThreadSafeStack(void) : m_mutex(), m_elements() {}
	
	/**
	 * \brief Trivial destructor
	 *
	 * This is declared virtual in case we need to derive something from this
	 * class down the track.
	 */
	virtual ~ThreadSafeStack(void) {}
	
	// -- Copy Semantics --
	
	/**
	 * \brief Copy constructor
	 *
	 * \param rhs The object from which to copy; note that the mutex of this
	 * object is locked during the copy operation	 
	 */
	ThreadSafeStack(const ThreadSafeStack<T>& rhs) :
		m_mutex(),
		m_elements()
	{
		// Lock the mutex of the source object
		GUARD(rhs.m_mutex);
		
		m_elements = rhs.m_element;
	}	// end copy constructor
	
	/**
	 * \brief Assignment operator
	 *
	 * Note that our own mutex is also locked during this operation to ensure
	 * consistency.
	 *
	 * \param rhs The object from which to copy; note that the mutex of this
	 * object is locked during the copy operation	 
	 *
	 * \return A reference to copied self	 
	 */
	ThreadSafeStack<T>& operator=(const ThreadSafeStack<T>& rhs)
	{
		// Lock our own mutex.
		GUARD(m_mutex);
		
		// Lock the source object mutex as well. It needs to be in a separate
		// block, to avoid name conflict.
		{
			GUARD(rhs.m_mutex);
			
			// Now we can do our copy.
			m_elements = rhs.m_elements;
		}
	}	// end assignment operator
	
	// -- Element Management --
	
	/**
	 * \brief Check whether the container is empty
	 *
	 * \return True if self is empty
	 */
	bool empty(void) const { GUARD(m_mutex); return m_elements.empty(); }
	
	/**
	 * \brief Get the number of elements in the Stack
	 *
	 * \return The size of the Stack
	 */
	std::size_t size(void) const
		{ GUARD(m_mutex); return m_elements.size(); }
		
	/**
	 * \brief Access the top (next) element of the stack
	 *
	 * Note that the accessed element is not removed.
	 *
	 * \return A *copy* of the top element (*not* a reference)
	 */
	T top(void) const { GUARD(m_mutex); return m_elements.top(); }
	
	/**
	 * \brief Add an element to the stack (replacing the current top element)
	 *
	 * \param val The element to add (this is copied)
	 */
	void push(const T& val) { GUARD(m_mutex); m_elements.push(val); }
	
	/**
	 * \brief Destroy the top element of the stack
	 */
	void pop(void) { GUARD(m_mutex); m_elements.pop(); }
	
	// --- Internal Declarations ---
	
	protected:
	
	/**
	 * \param The mutex that protects the internal container
	 */
	mutable QMutex m_mutex;
	
	/**
	 * \brief The internal Stack of elements
	 */
	std::stack<T> m_elements;
};	// end ThreadSafeQueue

#endif
