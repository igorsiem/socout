/**
 * \file ThreadSafeDeque.h Declares the ThreadSafeDeque template
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <deque>
#include <QMutex>

#include "MutexGuard.h"

#ifndef _ThreadSafeDeque_h_included
#define _ThreadSafeDeque_h_included

/**
 * \brief A thread-safe version of std::deque
 *
 * This implementation wraps a std::deque collection with a QMutex object.
 * This can be replaced with almost any other kind of mutex as long as it
 * works with the Guard class declared in the `MutexGuard.h` file.
 *
 * The main difference between this implementation and std::deque is that
 * element access is by value / copy, rather than by reference (to prevent
 * MT-related issues.
 *
 * Copy semantics are also non-trivial, as they need to lock the mutex of the
 * object being copied to ensure consistency in an MT context. In the case of
 * assignment, the mutex of the destination object (self) is locked as well,
 * for the same reason.
 *
 * \param T The element type
 */
template <class T>
class ThreadSafeDeque
{
	// --- External Interface ---
	
	public:
	
	/**
	 * \brief Trivial constructor
	 */
	ThreadSafeDeque(void) : m_mutex(), m_elements() {}
	
	/**
	 * \brief Trivial destructor
	 */
	virtual ~ThreadSafeDeque(void) {}
	
	// -- Copy Semantics --
	
	/**
	 * \brief Copy constructor
	 *
	 * \param rhs The object from which to copy
	 */
	ThreadSafeDeque(const ThreadSafeDeque<T>& rhs) :
		m_mutex(),
		m_elements()
	{
		// Lock the mutex of the SOURCE object before copying.
		GUARD(rhs.m_mutex);
		m_elements = rhs.m_elements;
	}	// end copy constructor
	
	/**
	 * \brief Assignment operator
	 *
	 * Note that our own mutex is also locked during this operation to ensure
	 * consistency.
	 *
	 * \param rhs The object from which to copy; note that the mutex of this
	 * object is locked during the copy operation	 
	 *
	 * \return A reference to copied self	 
	 */
	ThreadSafeDeque<T>& operator=(const ThreadSafeDeque<T>& rhs)
	{
		// Lock our own mutex.
		GUARD(m_mutex);
		
		// Lock the source object mutex as well. It needs to be in a separate
		// block, to avoid a name conflict.
		{
			GUARD(rhs.m_mutex);
			
			// Now we can do our copy.
			m_elements = rhs.m_elements;
		}
	}	// end assignment operator

	// -- Element Management --
	
	/**
	 * \brief Check whether the container is empty
	 *
	 * \return True if self is empty
	 */
	bool empty(void) const { GUARD(m_mutex); return m_elements.empty(); }
	
	/**
	 * \brief Get the number of elements in the Stack
	 *
	 * \return The size of the Stack
	 */
	std::size_t size(void) const
		{ GUARD(m_mutex); return m_elements.size(); }
		
	/**
	 * \brief Add a copy of the given element to the front of the deque
	 *
	 * \param elem The element to add
	 */
	void push_front(const T& elem)
		{ GUARD(m_mutex); m_elements.push_front(elem); }
	
	/**
	 * \brief Add a copy of the given element to the back of the deque
	 *
	 * \param elem The element to add
	 */
	void push_back(const T& elem)
		{ GUARD(m_mutex); m_elements.push_back(elem); }
		
	/**
	 * \brief Return a copy of the front element
	 *
	 * \return The front element
	 */
	T front(void) const { GUARD(m_mutex); return m_elements.front(); }
	
	/**
	 * \brief Return a copy of the back element
	 *
	 * \return The back element
	 */
	T back(void) const { GUARD(m_mutex); return m_elements.back(); }
	
	/**
	 * \brief Destroy the front element
	 */
	void pop_front(void) { GUARD(m_mutex); m_elements.pop_front(); }
	
	/**
	 * \brief Destroy the back element
	 */
	void pop_back(void) { GUARD(m_mutex); m_elements.pop_back(); }
	
	/**
	 * \ brief Destroy all elements in the container
	 */
	void clear(void) { GUARD(m_mutex); m_elements.clear(); }
		
	// --- Internal Declarations ---
	
	protected:
	
	/**
	 * \param The mutex that protects the internal container
	 */
	mutable QMutex m_mutex;
	
	/**
	 * \brief The internal Stack of elements
	 */
	std::deque<T> m_elements;

};	// end ThreadSafeDeque template

#endif
