/**
 * file MutexGuard Declares the Guard class for mutexes
 *
 * In this case MT is based on Qt threads. The Guard class is similar to
 * the QMutexLocker class, but is simpler, and more generic.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMutex>

#ifndef MUTEXGUARD_H
#define MUTEXGUARD_H

/**
 * \brief A Guard object that locks a given Mutex in its constructor, and
 * unlocks it in the destructor
 */
class Guard
{
    public:

    /**
     * \brief Constructor - locks the given mutex, and retains a reference
     * to it so that it will be unlocked in the destructor
     *
     * \param m The mutex to lock
     */
    Guard(QMutex& m);

    /**
     * \brief Destructor - unlocks the Mutex that was locked in the
     * constructor
     */
    ~Guard(void);

    private:

    /**
     * \brief The mutex being unlocked and locked by this Guard
     */
    QMutex& m_mutex;
};  // end class Guard

/**
 * \brief Instantiate a Guard object for a block with a given Mutex
 *
 * \param m The mutex with which to guard the block
 */
#define GUARD( m ) ::Guard g(m);

#endif // MUTEXGUARD_H
