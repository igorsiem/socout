/**
 * \brief TestError.cpp Tests for the GUI Error class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <business-logic/Error.h>

/**
 * \brief Tests for the Error exception class
 */
class TestError : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestError);
        CPPUNIT_TEST( test_throw );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * \brief A very basic test that exercises the Log class with string
     * streams
     */
    void test_throw(void);
};  // end class TestLog

CPPUNIT_TEST_SUITE_REGISTRATION(TestError);

void TestError::test_throw(void)
{
	// Throw an Error 
	try
	{
		RAISE_ERROR("test");
		CPPUNIT_FAIL("Failed to throw Error exception");
	}
	catch (const Error& e)
	{
		CPPUNIT_ASSERT(std::string(e.what()) == "test");
	}
}   // end test_string_streams
