/**
 * \file TestSerialisation.cpp Tests for the Serialisation classes
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <utilities/Serialisation.h>

/**
 * \brief Tests for the functionality of the Serialisation classes
 */
class TestSerialisation : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestSerialisation);
        CPPUNIT_TEST( test_value );
        CPPUNIT_TEST( test_conversion_to_yaml );
        CPPUNIT_TEST( test_conversion_from_yaml );
		CPPUNIT_TEST( test_conversion_to_json );
		CPPUNIT_TEST( test_conversion_from_json );
    CPPUNIT_TEST_SUITE_END();
	
	/**
	 * \brief Test basic Value functionality
	 */
	void test_value(void);
	
	/**
	 * \brief Test conversion from Value to YAML
	 */
	void test_conversion_to_yaml(void);
	
	/**
	 * \brief Test conversion from YAML to Value
	 */
	void test_conversion_from_yaml(void);
	
	/**
	 * \brief Test conversion from Value to JSON
	 */
	void test_conversion_to_json(void);
	
	/**
	 * \brief Test conversion from JSON to Value
	 */
	void test_conversion_from_json(void);
};	// end TestSerialisation

CPPUNIT_TEST_SUITE_REGISTRATION(TestSerialisation);

void TestSerialisation::test_value(void)
{
	SERIALISATION::Value nil;
	CPPUNIT_ASSERT(nil.get_type() ==
		SERIALISATION::Value::t_nil);
		
	SERIALISATION::Value t(true);
	CPPUNIT_ASSERT(t.get_type() ==
		SERIALISATION::Value::t_bool);
	CPPUNIT_ASSERT(t.get_as_int() != 0);
	
	SERIALISATION::Value d((double)1.0);
	CPPUNIT_ASSERT(d.get_type() ==
		SERIALISATION::Value::t_double);
		
	CPPUNIT_ASSERT(d.get_as_string() == "1");
	CPPUNIT_ASSERT(d.get_as_vector().size() == 1);
	CPPUNIT_ASSERT(d.get_as_vector()[0].get_type() ==
		SERIALISATION::Value::t_double);
		
	SERIALISATION::ValueVector vv;
	vv.push_back(SERIALISATION::Value(1));
	vv.push_back(SERIALISATION::Value(2));
	vv.push_back(SERIALISATION::Value(3));
	SERIALISATION::Value v(vv);
	CPPUNIT_ASSERT(v.get_as_vector().size() == 3);
	CPPUNIT_ASSERT(v.get_as_vector()[1].get_as_string() == "2");
	
	SERIALISATION::ValueMap vm;
	vm.insert(std::make_pair("first", SERIALISATION::Value(1)));
	vm.insert(std::make_pair("second", SERIALISATION::Value(2)));
	vm.insert(std::make_pair("third", SERIALISATION::Value(3)));

	SERIALISATION::Value m(vm);
	CPPUNIT_ASSERT(m.get_type() ==
		SERIALISATION::Value::t_map);
		
	// For some reason, the 'find' method causes an 'incompatible iterator'
	// assertion in VisualStudio 12 / Debug. Extracting a const refernce
	// to the internal map works fine.
	const SERIALISATION::ValueMap& the_map = m.get_as_map();
	CPPUNIT_ASSERT(the_map.find("first") != the_map.end());
	
	CPPUNIT_ASSERT(m.get_as_map()["first"].get_as_int() == 1);
	
	nil = m;
	CPPUNIT_ASSERT(nil.get_type() ==
		SERIALISATION::Value::t_map);
}	// end test_value

void TestSerialisation::test_conversion_to_yaml(void)
{
    SERIALISATION::YAMLSerialiser serialiser;
	
	SERIALISATION::ValueMap vm1;
	vm1.insert(std::make_pair("first", SERIALISATION::Value(true)));
	vm1.insert(std::make_pair("second", SERIALISATION::Value(3)));
	
	SERIALISATION::ValueVector vv1;
	vv1.push_back(SERIALISATION::Value(false));
	vv1.push_back(SERIALISATION::Value(2.5));
	vv1.push_back(SERIALISATION::Value("My string"));
	
	vm1.insert(std::make_pair("my array", SERIALISATION::Value(vv1)));
	
	SERIALISATION::Value top(vm1);
    
    // Enable this code to actually look at the YAML string; this is not
    // asserted, mainly because it's hard to get an exact string match.
	/*
    std::cout << std::endl <<
        "[DEBUG] " << std::endl;
    serialiser.write(top, std::cout);
    std::cout << std::endl <<
        std::endl;
	*/

    // Write to a string stream.
    std::stringstream out;
    CPPUNIT_ASSERT_NO_THROW(serialiser.write(top, out));    
}	// end test_conversion_to_yaml
	
void TestSerialisation::test_conversion_from_yaml(void)
{
    SERIALISATION::YAMLSerialiser serialiser;
    
    std::stringstream yaml;
    yaml <<
		"top-level:" << std::endl <<
        "  element1: 1" << std::endl <<
        "  element2: 2" << std::endl <<
        "  element3:" << std::endl <<
        "    - 1.1" << std::endl <<
        "    - 2.2" << std::endl <<
        "    - 3.3";
		
	SERIALISATION::Value v;
	serialiser.read(yaml, v);
		
	// The top-level element is a map with a single element.
	CPPUNIT_ASSERT(v.get_type() ==
		SERIALISATION::Value::t_map);
	SERIALISATION::ValueMap v_vm = v.get_as_map();
	CPPUNIT_ASSERT(v_vm.size() == 1);
	
	// The single element should be a named "top-level", and be a map
	// with three elements.
	SERIALISATION::ValueMap::iterator top_level_itr =
		v_vm.find("top-level");
	CPPUNIT_ASSERT(top_level_itr != v_vm.end());
	CPPUNIT_ASSERT(top_level_itr->second.get_type() ==
		SERIALISATION::Value::t_map);
	SERIALISATION::ValueMap top_level_vm =
		top_level_itr->second.get_as_map();
	CPPUNIT_ASSERT(top_level_vm.size() == 3);
	
	// The first 2 elements are "1" and "2".
	CPPUNIT_ASSERT(top_level_vm["element1"].get_as_string() == "1");
	CPPUNIT_ASSERT(top_level_vm["element2"].get_as_string() == "2");
	
	// The third element is an array with three elements.
	CPPUNIT_ASSERT(top_level_vm["element3"].get_type() ==
		SERIALISATION::Value::t_vector);
	SERIALISATION::ValueVector e3_vv =
		top_level_vm["element3"].get_as_vector();
	CPPUNIT_ASSERT(e3_vv.size() == 3);

	CPPUNIT_ASSERT(e3_vv[0].get_as_double() == 1.1);	
	CPPUNIT_ASSERT(e3_vv[1].get_as_double() == 2.2);	
	CPPUNIT_ASSERT(e3_vv[2].get_as_double() == 3.3);	
}	// end test_conversion_from_yaml method

void TestSerialisation::test_conversion_to_json(void)
{
    SERIALISATION::JSONSerialiser serialiser;
	
	SERIALISATION::ValueMap vm1;
	vm1.insert(std::make_pair("first", SERIALISATION::Value(true)));
	vm1.insert(std::make_pair("second", SERIALISATION::Value(3)));
	
	SERIALISATION::ValueVector vv1;
	vv1.push_back(SERIALISATION::Value(false));
	vv1.push_back(SERIALISATION::Value(2.5));
	vv1.push_back(SERIALISATION::Value("My string"));
	
	vm1.insert(std::make_pair("my array", SERIALISATION::Value(vv1)));
	
	SERIALISATION::Value top(vm1);
    
    // Enable this code to actually look at the JSON string; this is not
    // asserted, mainly because it's hard to get an exact string match.
	/*
    std::cout << std::endl <<
        "[DEBUG] " << std::endl;
    serialiser.write(top, std::cout);
    std::cout << std::endl <<
        std::endl;
	*/

    // Write to a string stream.
    std::stringstream out;
    CPPUNIT_ASSERT_NO_THROW(serialiser.write(top, out));    
}	// end test_conversion_to_json method
	
void TestSerialisation::test_conversion_from_json(void)
{
    SERIALISATION::JSONSerialiser serialiser;
    
    std::stringstream json;
    json <<
		"{ \"top-level\" : {" << std::endl <<
        "  \"element1\" : 1," << std::endl <<
        "  \"element2\" : 2," << std::endl <<
        "  \"element3\" : [" << std::endl <<
        "    1.1," << std::endl <<
        "    2.2," << std::endl <<
        "    3.3 ] } }";
		
	SERIALISATION::Value v;
	serialiser.read(json, v);
		
	// The top-level element is a map with a single element.
	CPPUNIT_ASSERT(v.get_type() ==
		SERIALISATION::Value::t_map);
	SERIALISATION::ValueMap v_vm = v.get_as_map();
	CPPUNIT_ASSERT(v_vm.size() == 1);
	
	// The single element should be a named "top-level", and be a map
	// with three elements.
	SERIALISATION::ValueMap::iterator top_level_itr =
		v_vm.find("top-level");
	CPPUNIT_ASSERT(top_level_itr != v_vm.end());
	CPPUNIT_ASSERT(top_level_itr->second.get_type() ==
		SERIALISATION::Value::t_map);
	SERIALISATION::ValueMap top_level_vm =
		top_level_itr->second.get_as_map();
	CPPUNIT_ASSERT(top_level_vm.size() == 3);
	
	// The first 2 elements are "1" and "2".
	CPPUNIT_ASSERT(top_level_vm["element1"].get_as_string() == "1");
	CPPUNIT_ASSERT(top_level_vm["element2"].get_as_string() == "2");
	
	// The third element is an array with three elements.
	CPPUNIT_ASSERT(top_level_vm["element3"].get_type() ==
		SERIALISATION::Value::t_vector);
	SERIALISATION::ValueVector e3_vv =
		top_level_vm["element3"].get_as_vector();
	CPPUNIT_ASSERT(e3_vv.size() == 3);

	CPPUNIT_ASSERT(e3_vv[0].get_as_double() == 1.1);	
	CPPUNIT_ASSERT(e3_vv[1].get_as_double() == 2.2);	
	CPPUNIT_ASSERT(e3_vv[2].get_as_double() == 3.3);	
}	// end test_conversion_from_json method 
