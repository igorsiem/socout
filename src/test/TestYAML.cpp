/**
 * \file TestYAML.cpp Tests for the YAML library integration
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <sstream>
#include <cppunit/extensions/HelperMacros.h>
#include <yaml-cpp/yaml.h>

/**
 * \brief Tests for the functionality of the yaml-cpp library
 */
class TestYAML : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestYAML);
        CPPUNIT_TEST( test_basic_parsing );
        CPPUNIT_TEST( test_stream_parsing );
        CPPUNIT_TEST( test_structured_parsing );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * \brief Test really basic parsing functionality
     */
    void test_basic_parsing(void);
    
    /**
     * \brief Test parsing from a stream
     */
    void test_stream_parsing(void);
    
    /**
     * \brief Test parsing a more complex structure
     */
    void test_structured_parsing(void);
};  // end TestYAML

CPPUNIT_TEST_SUITE_REGISTRATION(TestYAML);

void TestYAML::test_basic_parsing(void)
{
    YAML::Node node = YAML::Load("[1, 2, 3]");
    CPPUNIT_ASSERT(node.Type() == YAML::NodeType::Sequence);
}   // end test

void TestYAML::test_stream_parsing(void)
{
    std::stringstream input;
    input << "[1, 2, 3]";
    YAML::Node node = YAML::Load(input);
    CPPUNIT_ASSERT(node.Type() == YAML::NodeType::Sequence);
}   // end test_stream_parsing

void TestYAML::test_structured_parsing(void)
{
    std::stringstream input;
    input <<
        "top-level:" << std::endl <<
        "    -   element 0" << std::endl <<
        "    -   element 1" << std::endl <<
        "    -   element 2";
        
    YAML::Node tl = YAML::Load(input);
    CPPUNIT_ASSERT(tl.Type() == YAML::NodeType::Map);
    CPPUNIT_ASSERT(tl.begin()->first.as<std::string>() == "top-level");
    CPPUNIT_ASSERT(tl["top-level"].Type() == YAML::NodeType::Sequence);
    CPPUNIT_ASSERT(tl["top-level"].size() == 3);
    CPPUNIT_ASSERT(tl["top-level"][0].Type() == YAML::NodeType::Scalar);
    CPPUNIT_ASSERT(tl["top-level"][1].as<std::string>() == "element 1");
}   // end test_structured_parsing
