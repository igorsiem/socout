/**
 * \brief TestThreadSafeStack.cpp Tests for the ThreadSafeStack class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <utilities/ThreadSafeStack.h>
#include <utilities/ThreadSafeDeque.h>

/**
 * \brief Tests for the functions of the ThreadSafeStack class
 */
class TestThreadSafeStack : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestThreadSafeStack);
        CPPUNIT_TEST( test_stack );
		CPPUNIT_TEST( test_deque );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * \brief A very basic test for stack operations
     */
    void test_stack(void);
	
	/**
	 * \brief Basic tests for deque operations
	 */
	void test_deque(void);
};  // end class TestThreadSafeStack

CPPUNIT_TEST_SUITE_REGISTRATION(TestThreadSafeStack);

void TestThreadSafeStack::test_stack(void)
{
	ThreadSafeStack<int> s;
	CPPUNIT_ASSERT(s.empty());
	CPPUNIT_ASSERT(s.size() == 0);
	
	s.push(1);
	CPPUNIT_ASSERT(s.empty() == false);
	CPPUNIT_ASSERT(s.size() == 1);
	CPPUNIT_ASSERT(s.top() == 1);
	
	s.push(2);
	s.push(3);
	
	CPPUNIT_ASSERT(s.empty() == false);
	CPPUNIT_ASSERT(s.size() == 3);
	CPPUNIT_ASSERT(s.top() == 3);
	
	s.pop();
	CPPUNIT_ASSERT(s.empty() == false);
	CPPUNIT_ASSERT(s.size() == 2);
	CPPUNIT_ASSERT(s.top() == 2);
	
	s.pop();
	CPPUNIT_ASSERT(s.empty() == false);
	CPPUNIT_ASSERT(s.size() == 1);
	CPPUNIT_ASSERT(s.top() == 1);

	s.pop();
	CPPUNIT_ASSERT(s.empty());
	CPPUNIT_ASSERT(s.size() == 0);
}   // end test_stack method

void TestThreadSafeStack::test_deque(void)
{
	ThreadSafeDeque<int> d;
	CPPUNIT_ASSERT(d.empty());
	CPPUNIT_ASSERT(d.size() == 0);
	
	d.push_front(2);
	CPPUNIT_ASSERT(!d.empty());
	CPPUNIT_ASSERT(d.size() == 1);
	
	d.push_back(3);
	CPPUNIT_ASSERT(!d.empty());
	CPPUNIT_ASSERT(d.size() == 2);
	
	CPPUNIT_ASSERT(d.front() == 2);
	CPPUNIT_ASSERT(d.back() == 3);
	
	d.push_front(1);
	CPPUNIT_ASSERT(d.size() == 3);
	
	d.pop_back();
	CPPUNIT_ASSERT(d.size() == 2);
	CPPUNIT_ASSERT(d.back() == 2);
	CPPUNIT_ASSERT(d.front() == 1);
	
	d.clear();
	CPPUNIT_ASSERT(d.empty());
	CPPUNIT_ASSERT(d.size() == 0);
}	// end test_deque method
