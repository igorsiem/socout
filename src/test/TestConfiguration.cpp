/**
 * \brief TestConfiguration.cpp Tests for the GUI Configuration class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>

#include <business-logic/Configuration.h>
#include <business-logic/Error.h>
#include <utilities/Serialisation.h>

/**
 * \brief Tests for the Error exception class
 */
class TestConfiguration : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestConfiguration);
        CPPUNIT_TEST( test_defaults );
		CPPUNIT_TEST( test_cl_config_filename_parsing );
		CPPUNIT_TEST( test_cl_console_log_level_parsing );
		CPPUNIT_TEST( test_cl_file_log_settings_parsing );
		CPPUNIT_TEST( test_config_file_parsing );
		CPPUNIT_TEST( test_config_file_write );
    CPPUNIT_TEST_SUITE_END();
    
	/**
	 * \brief Test the default values of all configuration options
	 */
    void test_defaults(void);
	
	/**
	 * \brief Test command-line parsing of the config filename
	 */
	void test_cl_config_filename_parsing(void);
	
	/**
	 * \brief Test parsing of the console log level setting
	 */
	void test_cl_console_log_level_parsing(void);
	
	/**
	 * \brief Test parsing of file log settings from the command-line
	 */
	void test_cl_file_log_settings_parsing(void);
	
	/**
	 * \brief Test parsing of the config file
	 */
	void test_config_file_parsing(void);
	
	/**
	 * \brief Test that writing the config file works correctly
	 */
	void test_config_file_write(void);
};  // end class TestConfiguration

CPPUNIT_TEST_SUITE_REGISTRATION(TestConfiguration);

void TestConfiguration::test_defaults(void)
{
	Configuration& config = Configuration::get_configuration();
	
	config.clear_to_defaults();
	
	// Note: this default will depend on the user application data path. We
	// won't bother testing it for now - instead we'll just make sure it's
	// not empty.
	// CPPUNIT_ASSERT(config.get_config_filename() == "SocOut-config.yml");
	CPPUNIT_ASSERT(!config.get_config_filename().empty());
	CPPUNIT_ASSERT(config.get_console_log_level() == Log::level_info);
	
	// Again, the log file location depends on the user account. Just check
	// that it is not empty.
	// CPPUNIT_ASSERT(config.get_file_log_filename() == "SocOut-log.txt");
	CPPUNIT_ASSERT(config.get_file_log_level() == Log::level_none);
	
	// TODO More configuration options go here
}   // end test method

void TestConfiguration::test_cl_config_filename_parsing(void)
{
	Configuration& config = Configuration::get_configuration();
	
	// Error where we have a recognised argument, but no following parameter.
	char* error_args_2[] = { "executable", "--config-filename" };
	CPPUNIT_ASSERT_THROW(config.initialise(2, error_args_2), Error);
	
	// We have a correctly specified config filename.
	char* config_filename_correct_args[] =
		{ "executable", "--config-filename",
			"build/test-fixtures/test-config-file.yml" };
	CPPUNIT_ASSERT_NO_THROW(
		config.initialise(3, config_filename_correct_args));
	CPPUNIT_ASSERT(config.get_config_filename() ==
		"build/test-fixtures/test-config-file.yml");
}	// end test_cl_parsing

void TestConfiguration::test_cl_console_log_level_parsing(void)
{
	Configuration& config = Configuration::get_configuration();
	
	// Try an incorrect console log level. It won't throw an
	// exception on parsing, but it will when we try to access the value.
	char* wrong_console_log_level_args[] =
		{ "executable", "--console-log-level", "WRONG" };
	config.initialise(3, wrong_console_log_level_args);
	CPPUNIT_ASSERT_THROW(config.get_console_log_level(), Error);
		
	// Now a correct console log level.
	char* console_log_level_args[] =
		{ "executable", "--console-log-level", "WARNING" };
	CPPUNIT_ASSERT_NO_THROW(config.initialise(3, console_log_level_args));
	CPPUNIT_ASSERT(config.get_console_log_level() == Log::level_warning);
}	// end test_cl_console_log_level_parsing

void TestConfiguration::test_cl_file_log_settings_parsing(void)
{
	Configuration& config = Configuration::get_configuration();

	// Set the log filename AND the file log level at the same time
	char* log_file_args[] =
		{ "executable", "--file-log-filename",
			"log.txt", "--file-log-level", "DEBUG" };
	CPPUNIT_ASSERT_NO_THROW(config.initialise(5, log_file_args));
	CPPUNIT_ASSERT(config.get_file_log_filename() == "log.txt");
	CPPUNIT_ASSERT(config.get_file_log_level() == Log::level_debug);
}	// end test_cl_file_log_settings_parsing

void TestConfiguration::test_config_file_parsing(void)
{
	Configuration& config = Configuration::get_configuration();
	char* args[] = {
		"executable", "--config-filename",
		"build/test-fixtures/test_config_file_parsing-config.yml"};
	config.initialise(3, args);
	
	CPPUNIT_ASSERT(config.get_console_log_level() == Log::level_warning);
	CPPUNIT_ASSERT(config.get_file_log_filename() == "log.txt");
	CPPUNIT_ASSERT(config.get_file_log_level() == Log::level_debug);
}	// end test_config_file_parsing

void TestConfiguration::test_config_file_write(void)
{
	Configuration& config = Configuration::get_configuration();
	
	// First, test a straight empty write.
	char* args1[] = {
		"executable",
		"--config-filename",
		"build/test-fixtures/test_config_file_write-empty-config.yml"
	};
		
	config.initialise(3, args1);
	config.write_config_file();
	
	// Read it back into a serialisation value. It will not come back as a
	// map, because it should be empty.
	std::ifstream empty_config_file(
		"build/test-fixtures/test_config_file_write-empty-config.yml");
	SERIALISATION::YAMLSerialiser ser;
	SERIALISATION::Value v;
	ser.read(empty_config_file, v);
	CPPUNIT_ASSERT(v.get_type() != SERIALISATION::Value::t_map);
	empty_config_file.close();
	
	// Now set up with another empty config file, but add some items.
	char* args2[] = {
		"executable",
		"--config-filename",
		"build/test-fixtures/test_config_file_write-items-config.yml"
	};
	config.initialise(3, args2);
	
	CPPUNIT_ASSERT(config.get_console_log_level() == Log::level_info);
	
	// We'll play with the log file items. Check default values.
	// CPPUNIT_ASSERT(config.get_file_log_filename() == "SocOut-log.txt");
	CPPUNIT_ASSERT(!config.get_file_log_filename().empty());
	
	CPPUNIT_ASSERT(config.get_file_log_level() == Log::level_none);
	
	// Change just the file log stuff.
	config.set_file_log_filename("log.txt");
	config.set_file_log_level(Log::level_debug);
	
	// Write the config file.
	config.write_config_file();
	
	// Open the config file independently, and check that it has the two
	// changed items and nothing else.
	std::ifstream items_file(
		"build/test-fixtures/test_config_file_write-items-config.yml");
	ser.read(items_file, v);
	items_file.close();
	
	CPPUNIT_ASSERT(v.as_map().size() == 2);
	CPPUNIT_ASSERT(v.as_map()["file-log-filename"].get_as_string() ==
		"log.txt");
	CPPUNIT_ASSERT(v.as_map()["file-log-level"].get_as_string() == "DEBUG");
	CPPUNIT_ASSERT(v.as_map().find("console-log-level") == v.as_map().end());
}	// end test_config_file_write 
