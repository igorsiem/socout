/**
 * \file TestJSON.cpp Tests for the JSON library integration
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <sstream>
#include <cppunit/extensions/HelperMacros.h>
#include <json/json.h>

/**
 * \brief Tests for the functionality of the jsoncpp library
 */
class TestJSON : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestJSON);
        CPPUNIT_TEST( test );
    CPPUNIT_TEST_SUITE_END();
    
    void test(void);
};  // end TestJSON

CPPUNIT_TEST_SUITE_REGISTRATION(TestJSON);

void TestJSON::test(void)
{
    std::stringstream input;
    input << 
    "{" << std::endl <<
    "    \"encoding\" : \"UTF-8\"," << std::endl <<
    "    \"plug-ins\" : [" << std::endl <<
    "        \"python\"," << std::endl <<
    "        \"c++\"," << std::endl <<
    "        \"ruby\"" << std::endl <<
    "        ]," << std::endl <<
    "    \"indent\" : { \"length\" : 3, \"use_space\": true }" <<
        std::endl <<
    "}";
    
    // Parse into a root node.
    Json::Value root;
    input >> root;
    
    // Check some values.
    CPPUNIT_ASSERT(root.get("encoding", "").asString() == "UTF-8");
    CPPUNIT_ASSERT(root["plug-ins"].size() == 3);
    CPPUNIT_ASSERT(root["plug-ins"][1].asString() == "c++");
    CPPUNIT_ASSERT(root["indent"]["length"].asInt() == 3);
    CPPUNIT_ASSERT(root["indent"]["use_space"].asBool() == true);
}   // end test
