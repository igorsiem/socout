/**
 * \brief TestPerson.cpp Tests for the Person and People classes
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <business-logic/Person.h>
#include <business-logic/Error.h>

/**
 * \brief Tests for the functions of the Person and Peoples classes
 */
class TestPerson : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestPerson);
		CPPUNIT_TEST( test_name );
        CPPUNIT_TEST( test_insert_and_retrieve );
		CPPUNIT_TEST( test_erase );
		CPPUNIT_TEST( test_serialisation );
		CPPUNIT_TEST( test_people_copy );
    CPPUNIT_TEST_SUITE_END();
	
	/**
	 * \brief Test basic Person::Name operations
	 */
	void test_name(void);
    
	/**
	 * \brief Test basic insertion and retrieval operations
	 */
    void test_insert_and_retrieve(void);
	
	/**
     * \brief Test the erase operations
	 */
	void test_erase(void);
	
	/**
	 * \brief Test serialisation operations
	 */
	void test_serialisation(void);
	
	/**
	 * \brief Test the copy semantics of the People collection class
	 */
	void test_people_copy(void);
};  // end class TestPerson

CPPUNIT_TEST_SUITE_REGISTRATION(TestPerson);

void TestPerson::test_name(void)
{
	// Test name aggregation.
	Person::Name joe_bloggs("Joe", "Bloggs");
	CPPUNIT_ASSERT(joe_bloggs.get_firstlast() == "Joe Bloggs");
	CPPUNIT_ASSERT(joe_bloggs.get_lastfirst() == "Bloggs, Joe");
	
	Person::Name john("John", "");
	CPPUNIT_ASSERT(john.get_firstlast() == "John");
	CPPUNIT_ASSERT(john.get_lastfirst() == "John");

	Person::Name smith("", "Smith");
	CPPUNIT_ASSERT(smith.get_firstlast() == "Smith");
	CPPUNIT_ASSERT(smith.get_lastfirst() == "Smith");
}	// end test_name method

void TestPerson::test_insert_and_retrieve(void)
{
	People people;
	
	// Put in a single Person, and retrieve it again.
	people.insert(Person("Joe", "Bloggs", "joe.bloggs@company.com"));
	CPPUNIT_ASSERT(people.size() == 1);
	CPPUNIT_ASSERT(people.get_person_with_email(
		"joe.bloggs@company.com").get_first_name() == "Joe");
	CPPUNIT_ASSERT(people.get_person_with_email(
		"joe.bloggs@company.com").get_last_name() == "Bloggs");
	
	// Put in another person.
	people.insert(Person("John", "Smith", "john@anothercompany.com"));
	CPPUNIT_ASSERT(people.size() == 2);
	
	// Attempt to add a person with the same e-mail address (but with a
	// different name). This will throw an exception.
	CPPUNIT_ASSERT_THROW(
		people.insert(Person("John", "", "john@anothercompany.com")),
		Error);
		
	// Add a Person with the same name, but a different e-mail address.
	// This is OK.
	CPPUNIT_ASSERT_NO_THROW(
		people.insert(
			Person("John", "Smith", "john.smith@company.com")));
			
	// Look for e-mails with the name "John Smith".
	std::set<std::string> addresses;
	people.get_emails_by_name(Person::Name("John", "Smith"), addresses);
	CPPUNIT_ASSERT(addresses.size() == 2);
	CPPUNIT_ASSERT(addresses.find("john@anothercompany.com") != addresses.end());
	CPPUNIT_ASSERT(addresses.find("john.smith@company.com") != addresses.end());
	
	// Look for e-mails with the name "A B".
	addresses.clear();
	people.get_emails_by_name(Person::Name("A", "B"), addresses);
	CPPUNIT_ASSERT(addresses.size() == 0);
	
	// There should only be two names in the container - "Joe Bloggs" and
	// "John Smith"
	std::set<Person::Name> names;
	people.get_names(names);
	CPPUNIT_ASSERT(names.size() == 2);
	CPPUNIT_ASSERT(names.find(Person::Name("John", "Smith")) != names.end());
	CPPUNIT_ASSERT(names.find(Person::Name("Joe", "Bloggs")) != names.end());
}	// end test_insert_and_retrieve method

void TestPerson::test_erase(void)
{
	// A collection of people with different names; some with common names.
	People people;
	people.insert(Person("John", "Smith", "john.smith@company.com"));
	people.insert(Person("John", "Smith", "johns@anothercompany.com"));
	people.insert(Person("John", "Smith", "john.smith2@company.com"));
	people.insert(Person("Joe", "Bloggs", "joe.bloggs@company.com"));
	people.insert(Person("Joe", "Bloggs", "joe.bloggs2@company.com"));
	people.insert(Person("Fred", "Nerk", "fredn@anothercompany.com"));
	
	std::set<std::string> emails;
	std::set<Person::Name> names;
	
	// Just check that we have 6 e-mail addresses and 3 names.
	CPPUNIT_ASSERT(people.get_emails(emails).size() == 6);
	CPPUNIT_ASSERT(people.get_names(names).size() == 3);
	
	// We should have 3 e-mail addresses for the name "John Smith".
	emails.clear();
	CPPUNIT_ASSERT(
		people.get_emails_by_name(
			Person::Name("John", "Smith"), emails).size() == 3);
			
	// Attempt to erase an non-existent person.
	CPPUNIT_ASSERT(people.erase("fred@company.com") == false);
			
	// Remove the John Smith at anothercompany.com.
	CPPUNIT_ASSERT(people.erase("johns@anothercompany.com") == true);
	
	// Check that we was removed correctly. It should no longer be available
	// when searching by e-mail address, and should not be returned in the
	// set when searching by name.
	CPPUNIT_ASSERT(
		people.has_person_with_email("johns@anothercompany.com") == false);
	CPPUNIT_ASSERT_THROW(
		people.get_person_with_email("johns@anothercompany.com"), Error);
	
	emails.clear();
	CPPUNIT_ASSERT(people.get_emails(emails).size() == 5);
	CPPUNIT_ASSERT(emails.find("johns@anothercompany.com") == emails.end());
	
	emails.clear();
	CPPUNIT_ASSERT(people.get_emails_by_name(Person::Name("John", "Smith"),
		emails).size() == 2);
	CPPUNIT_ASSERT(emails.find("john.smith@company.com") != emails.end());
	CPPUNIT_ASSERT(emails.find("johns@anothercompany.com") == emails.end());
	CPPUNIT_ASSERT(emails.find("john.smith2@company.com") != emails.end());
}	// end test_erase method


void TestPerson::test_serialisation(void)
{
	// Put together a collection of people and serialise it.
	People people1;
	people1.insert(Person("Joe", "Bloggs", "joe.bloggs@company.com"));
	people1.insert(Person("John", "Smith", "john.smith@company.com"));
	people1.insert(Person("Fred", "Nerk", "fred.nerk@company.com"));
	
	SERIALISATION::ValueVector vv;
	people1.copy_to(vv);
	CPPUNIT_ASSERT(vv.size() == 3);
	
	// Now read the collection back.
	People people2;
	people2.copy_from(vv);
	
	CPPUNIT_ASSERT(people2.size() == 3);
	CPPUNIT_ASSERT(people2.has_person_with_email("joe.bloggs@company.com"));
	CPPUNIT_ASSERT(people2.has_person_with_email("john.smith@company.com"));
	CPPUNIT_ASSERT(people2.has_person_with_email("fred.nerk@company.com"));

	// Check one Person object fully.
	Person john_smith = people2.get_person_with_email(
		"john.smith@company.com");
	CPPUNIT_ASSERT(john_smith.get_first_name() == "John");
	CPPUNIT_ASSERT(john_smith.get_last_name() == "Smith");
	CPPUNIT_ASSERT(john_smith.get_email() == "john.smith@company.com");	
}	// end test_serialisation method

void TestPerson::test_people_copy(void)
{
	// Create a People collection with some elements, and then copy it using
	// a copy constructor and an assignment operator.
	People people1;
	people1.insert(Person("John", "Smith", "john@company.com"));
	
	People people2(people1);
	CPPUNIT_ASSERT(people2.size() == 1);
	CPPUNIT_ASSERT(people2.has_person_with_email("john@company.com"));
	
	// Verify that assignment operation overwrites previous collection.
	People people3;
	people3.insert(Person("Joe", "Bloggs", "joe@com.com"));
	
	people3 = people1;
	CPPUNIT_ASSERT(people3.has_person_with_email("joe@com.com") == false);
	CPPUNIT_ASSERT(people3.has_person_with_email("john@company.com"));
}	// end test_people_copy
