/**
 * \brief TestCurl.cpp Tests for the cURL library
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <curl/curl.h>

/**
 * \brief Tests for the functions of the cURL library
 */
class TestCurl : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestCurl);
		CPPUNIT_TEST( test );
    CPPUNIT_TEST_SUITE_END();
	
	/**
	 * \brief Basic initialisation and shutdown test
	 */
	void test(void);
};  // end class TestDataFile

CPPUNIT_TEST_SUITE_REGISTRATION(TestCurl);

void TestCurl::test(void)
{
	CPPUNIT_ASSERT(curl_global_init(CURL_GLOBAL_ALL) == 0);
	CURL* curl = curl_easy_init();
	CPPUNIT_ASSERT(curl);
	
	// To display the libcurl version, uncomment the following lines.
	// std::cout << std::endl <<
	// 	"[DEBUG] cURL version " << curl_version() << std::endl;
	
	// curl_easy_cleanup(curl);
	curl_global_cleanup();
}	// end test
