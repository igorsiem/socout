/**
 * \brief TestLog.cpp Tests for the Log class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <utilities/Log.h>

/**
 * \brief Tests for the functions of the Log class
 */
class TestLog : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestLog);
        CPPUNIT_TEST( test_string_stream_logs );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * \brief A very basic test that exercises the Log class with string
     * streams
     */
    void test_string_stream_logs(void);
};  // end class TestLog

CPPUNIT_TEST_SUITE_REGISTRATION(TestLog);

void TestLog::test_string_stream_logs(void)
{
    // Clear the logging singleton, in case it has been used for something
    // else before.
    Log::get_log().clear();
    
    // Set up a logging string stream for errors, and one for info.
    std::stringstream errors, info;
    Log::get_log().add_log_stream(Log::level_error, errors);
    Log::get_log().add_log_stream(Log::level_info, info);
    
    // Add a message of every type.
    LOG_ERROR("error");
    LOG_WARNING("warning");
    LOG_INFO("info");
    LOG_DEBUG("debug");
    
    // Error stream should only receive errors. Info stream gets errors,
    // warnings and info, but not debug.
    CPPUNIT_ASSERT(errors.str() == "[ERR] error\n");
    CPPUNIT_ASSERT(info.str() == "[ERR] error\n"
        "[WAR] warning\n"
        "[INF] info\n");
}   // end test_string_streams
