/**
 * \file test/main.cpp Entry point for the test executable
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdexcept>
#include <iostream>

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

/**
 * \brief Global flag for running online tests
 *
 * If this is false, we don't run any test that requires access to online
 * systems.
 */
bool online = false;

/**
 * \brief Entry point for the test executable
 *
 * This function sets up the test framework, and runs the tests. Note that
 * command-line arguments are not used in this iteration, but may be in the
 * future.
 *
 * \param argc The number of command-line arguments
 *
 * \param argv The command-line arguments
 *
 * \return 0 if all tests pass, -1 if there was a test failure, -2 if there
 * was a test error with a known exception, and -3 if an unrecognised
 * exception is thrown
 */
int main(int argc, char* argv[])
{
    int retcode = 0;
    try
    {
        // Check for the "--online" cl argument.
        for (int i = 1; i < argc; i++)
        {
            std::string argument = argv[i];
            if (argument == "--online") online = true;
            else throw std::runtime_error("Unrecognised command-line "
                "argument \"" + argument + "\"");
        }   // end argument
        
        // Simple test run setup
		CppUnit::TextUi::TestRunner test_runner;
        CppUnit::TestFactoryRegistry& test_factory_registry =
			CppUnit::TestFactoryRegistry::getRegistry();
		test_runner.addTest(test_factory_registry.makeTest());
        
        // Run the test and check the result.
        int result = test_runner.run();
        if (result)
        {
            std::cout << "result: SUCCESS" << std::endl;
        }
        else
        {
            std::cout << "result: FAIL" << std::endl;
            return -1;
        }
    }
    catch (const std::exception& error)
    {
        std::cerr << "[ERROR] " << error.what() << std::endl;
        retcode = -2;
    }
    catch (...)
    {
        std::cerr << "[ERROR] *** unrecognised exception" << std::endl;
        retcode = -3;
    }
    
    return retcode;
}   // end main
