/**
 * \brief TestTwitcurl.cpp Tests for the Twitcurl library
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>

#include <QRegExp>
#include <QStringList>
#include <QDateTime>

#include <twitcurl/twitcurl.h>
#include <utilities/Serialisation.h>
#include <business-logic/Error.h>
#include <business-logic/tokens.h>

// This is defined in the `main` method.
extern bool online;

/**
 * \brief Tests for the twitcurl
 */
class TestTwitcurl : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestTwitcurl);
		CPPUNIT_TEST( test_access );
		CPPUNIT_TEST( test_timeline );
		CPPUNIT_TEST( test_datetime_processing );
    CPPUNIT_TEST_SUITE_END();
	
	/**
	 * \brief Basic test of authorisation and access
	 */
	void test_access(void);
	
	/**
	 * \brief Test for retrieving a timeline
	 */
	void test_timeline(void);
	
	/**
	 * \brief Test processing of date/time strings as sent by Twitter
	 */
	void test_datetime_processing(void);	 
	
	public:
	
	/**
	 * \brief Constructor - sets up the shared twitCurl object (if we
	 * are doing online testing)
	 */
	TestTwitcurl(void);
	
	// Default destructor is OK
	
	private:
	
	/**
	 * \brief A single Twitcurl object that is shared by all tests
	 *
	 * This is set up and authorised in the constructor IF we are doing
	 * online tests.
	 */
	twitCurl m_twitter;
	
	/**
	 * \brief The authorisation token key
	 */
	std::string m_token_key;
	
	/**
	 * \brief The authorisation token secret
	 */
	std::string m_token_secret;
};  // end class TestTwitcurl

CPPUNIT_TEST_SUITE_REGISTRATION(TestTwitcurl);

void TestTwitcurl::test_access(void)
{
	// Only do this test if we want to do online tests.
	if (online)
	{
    	// Verify our credentials.
		CPPUNIT_ASSERT(m_twitter.accountVerifyCredGet());
		
		// Parse the response.
		std::string response;
		m_twitter.getLastWebResponse(response);
		std::stringstream rsp;
		rsp << response;
		
		SERIALISATION::JSONSerialiser serialiser;
		SERIALISATION::Value r;
		serialiser.read(rsp, r);
		
		// Make sure that our respone has no error item.
		CPPUNIT_ASSERT(r.as_map().find("errors") == r.as_map().end());
		
		/*
		std::cout << std::endl <<
			"[DEBUG] verify credentials response: " << response << std::endl;
		*/
		
		// Now set an invalid key, and make sure we DO get an error.
    	m_twitter.getOAuth().setOAuthTokenSecret(m_token_secret+"x");
		m_twitter.accountVerifyCredGet();
		
		response.clear();
		rsp.str(std::string());
		
		m_twitter.getLastWebResponse(response);
		rsp << response;
		r = SERIALISATION::Value();
		serialiser.read(rsp, r);
		
		/*
		std::cout << std::endl <<
			"[DEBUG] verify invalid credentials response: " << response <<
				std::endl;
		*/
				
		CPPUNIT_ASSERT(r.as_map().find("errors") != r.as_map().end());
		
		// Finally, use a completely new and separate twitCurl object, set
		// our keys accordingly, and make sure that everything works.
		//
		// This is a minimal authentication sequence if we already have
		// access credentials.
		twitCurl new_twitter;
    	new_twitter.getOAuth().setConsumerKey(TWITTER_APP_KEY);
    	new_twitter.getOAuth().setConsumerSecret(TWITTER_APP_SECRET);
    	new_twitter.getOAuth().setOAuthTokenKey(m_token_key);
    	new_twitter.getOAuth().setOAuthTokenSecret(m_token_secret);
		
		new_twitter.accountVerifyCredGet();
				
		// Parse the response again - make sure we do NOT have an error
		response.clear();
		rsp.str(std::string());
		new_twitter.getLastWebResponse(response);
		rsp << response;
		r = SERIALISATION::Value();
		serialiser.read(rsp, r);
		
		/*
		std::cout << std::endl <<
			"[DEBUG] second verify credentials response: " << response <<
				std::endl;
		*/
		
		CPPUNIT_ASSERT(r.as_map().find("errors") == r.as_map().end());
	}	// end if we are doing online tests.
}	// end test_access

void TestTwitcurl::test_timeline(void)
{
	if (online)
	{
		// Get the home timeline.
		CPPUNIT_ASSERT(m_twitter.timelineHomeGet());
		
		// Get the response.
		std::string response;
		m_twitter.getLastWebResponse(response);

		// Uncomment this to see the raw JSON response.
		std::cout << std::endl <<
			"[DEBUG] " << response << std::endl;
		
		// Parse the response.
		std::stringstream rsp;
		rsp << response;
		
		SERIALISATION::JSONSerialiser serialiser;
		SERIALISATION::Value r;
		serialiser.read(rsp, r);

		// Make sure our response is a vector of entries with at least one
		// value.
		CPPUNIT_ASSERT(r.get_type() == SERIALISATION::Value::t_vector);
		CPPUNIT_ASSERT(r.as_vector().size() > 0);
		
		// Take the first entry.
		SERIALISATION::Value entry = r.as_vector()[0];
		
		// Uncomment this section to see some of the components of the entry.
		/*
		std::cout << std::endl <<
			"[DEBUG] created: " <<
				entry.as_map()["created_at"].get_as_string() <<
					std::endl <<
			"[DEBUG] text: " << entry.as_map()["text"].get_as_string() <<
				std::endl;
		*/		
	}
}	// end test_timeline

// Quick and dirty Month-string-to-number function
int to_month_number(const std::string& month)
{
	if (month == "Jan") return 1;
	else if (month == "Feb") return 2;
	else if (month == "Mar") return 3;
	else if (month == "Apr") return 4;
	else if (month == "May") return 5;
	else if (month == "Jun") return 6;
	else if (month == "Jul") return 7;
	else if (month == "Aug") return 8;
	else if (month == "Sep") return 9;
	else if (month == "Oct") return 10;
	else if (month == "Nov") return 11;
	else if (month == "Dec") return 12;
	else
		RAISE_ERROR("An illegal month name was encountered: \"" << month <<
			"\".");
}

void TestTwitcurl::test_datetime_processing(void)
{
	// RegExp for parsing Twitter date.
	QRegExp twitter_datetime_parser(
		"([A-Za-z]{3})\\s([A-Za-z]{3})\\s(\\d{1,2})\\s(\\d{2})\\:(\\d{2})\\:"
		"(\\d{2})\\s([\\+|\\-]\\d{4})\\s(\\d{4})");
		
	CPPUNIT_ASSERT(twitter_datetime_parser.indexIn(
		"Tue Jun 23 14:59:54 +0000 2015") == 0);
		
	QStringList elements = twitter_datetime_parser.capturedTexts();
	
	CPPUNIT_ASSERT(elements.size() == 9);
	CPPUNIT_ASSERT(elements[0] == "Tue Jun 23 14:59:54 +0000 2015");
	CPPUNIT_ASSERT(elements[1] == "Tue");		// Day of week
	CPPUNIT_ASSERT(elements[2] == "Jun");		// Month
	CPPUNIT_ASSERT(elements[3] == "23");		// Day of month
	CPPUNIT_ASSERT(elements[4] == "14");		// Hour of day
	CPPUNIT_ASSERT(elements[5] == "59");		// Minute of hour
	CPPUNIT_ASSERT(elements[6] == "54");		// Second of minute
	CPPUNIT_ASSERT(elements[7] == "+0000");		// Time difference
	CPPUNIT_ASSERT(elements[8] == "2015");		// Year
	
	// Create a date and time with offset from UTC
	QDateTime dtg(
		QDate(
			elements[8].toInt(),
			to_month_number(elements[2].toStdString()),
			elements[3].toInt()),
		QTime(
			elements[4].toInt(),
			elements[5].toInt(),
			elements[6].toInt()
		),
		Qt::OffsetFromUTC,
		elements[7].toInt() * 3600
	);
	
	CPPUNIT_ASSERT(dtg.isValid());
}	// end test_datetime_processing

TestTwitcurl::TestTwitcurl(void) :
	m_twitter(),
	m_token_key(),
	m_token_secret()
{
	if (online)
	{
    	// Set up the twitter object with our test account info, and app
		// keys.
    	m_twitter.setTwitterUsername("SocOutProject");
    	m_twitter.setTwitterPassword("ZMr6ZQBHSih4JPKA");
    
    	// Now we follow the twitcurl OpenAuth flow...
    	//
    	// TwitcurlOAuthFlow step 0: Set our consumer key and secret.
    	m_twitter.getOAuth().setConsumerKey(TWITTER_APP_KEY);
    	m_twitter.getOAuth().setConsumerSecret(TWITTER_APP_SECRET);
			
    	// TwitcurlOAuthFlow step 1: Get a URL to request an authorisation
		// PIN from Twitter
    	std::string auth_url;
    	m_twitter.oAuthRequestToken(auth_url);
    
    	// TwitcurlOAuthFlow step 2: Sort out PIN automatically (can ask a
		// user to go to the URL and grab the PIN manually.
    	m_twitter.oAuthHandlePIN(auth_url);
    	
    	// TwitcurlOAuthFlow step 3: Get our access tokens.
    	m_twitter.oAuthAccessToken();
    	
    	// TwitcurlOAuthFlow step 4: Get our keys out. These would normally
		// be stored somewhere for reuse.
    	m_twitter.getOAuth().getOAuthTokenKey(m_token_key);
    	m_twitter.getOAuth().getOAuthTokenSecret(m_token_secret);
		
		/*
		std::cout << std::endl <<
			"Access tokens:" << std::endl <<
			"    key: " << m_token_key << std::endl <<
			"    secret: " << m_token_secret << std::endl;
		*/ 			
	}
}	// end TestTwitcurl
