/**
 * \brief TestDataFile.cpp Tests for the DataFile class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <business-logic/DataFile.h>

/**
 * \brief Tests for the functions of the DataFile class
 */
class TestDataFile : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestDataFile);
		CPPUNIT_TEST( test_copy );
    CPPUNIT_TEST_SUITE_END();
	
	/**
	 * \brief Test the copy semantics of a DataFile
	 *
	 * DataFile objects use mutexes, so their copy operation is a little more
	 * convoluted.
	 *
	 * \todo Re-enable testing of People copying
	 *
	 * \todo Test copying of Accounts
	 *
	 * \todo Test copying of Timelines
	 */
	void test_copy(void);
};  // end class TestDataFile

CPPUNIT_TEST_SUITE_REGISTRATION(TestDataFile);

void TestDataFile::test_copy(void)
{
	// Create a data file containing a couple of Person objects, and set
	// the 'is dirty' flag.
	DataFile df1;
	
	// df1.get_people().insert(Person("Joe", "Bloggs", "joe@company.com"));
	// df1.get_people().insert(Person("John", "Smith", "john@co.com"));
	
	df1.set_is_dirty(true);
	
	// Use copy constructor.
	DataFile df2(df1);
	// CPPUNIT_ASSERT(df2.get_people().size() == 2);
	// CPPUNIT_ASSERT(df2.get_people().has_person_with_email(
	// 	"joe@company.com"));
	// CPPUNIT_ASSERT(df2.get_people().has_person_with_email(
	// 	"john@co.com"));
	CPPUNIT_ASSERT(df2.is_dirty());		

	// Now use assignment operator
	DataFile df3;
	df3 = df2;
	// CPPUNIT_ASSERT(df3.get_people().size() == 2);
	// CPPUNIT_ASSERT(df3.get_people().has_person_with_email(
	// 	"joe@company.com"));
	// CPPUNIT_ASSERT(df3.get_people().has_person_with_email(
	// 	"john@co.com"));
	CPPUNIT_ASSERT(df3.is_dirty());		
}	// end test
