/**
 * \file Error.cpp Implements the error class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Error.h"

Error::Error(const std::string& msg) :
    std::exception(),
    m_message(msg)
{

}   // end constructor

Error::Error(const Error& rhs) :
    std::exception(rhs),
    m_message(rhs.m_message)
{
}   // end copy constructor

Error& Error::operator=(const Error& rhs)
{
    std::exception::operator=(rhs);
    m_message = rhs.m_message;
    return *this;
}   // end assignment operator
