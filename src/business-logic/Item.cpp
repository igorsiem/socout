/**
 * \file Item.cpp Implements the Item class 
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Item.h"

Item::MetadataMap& Item::get_metadata_for(const std::string& service_name)
{
	// Check to see if we have a service by this name.
	if (m_service_metadata.find(service_name) == m_service_metadata.end())
		m_service_metadata[service_name] = MetadataMap();
		
	return m_service_metadata[service_name];
}	// end get_metadata_for method

const Item::MetadataMap& Item::get_metadata_for(
		const std::string& service_name) const
{
	// Check to see if we have a service by this name.
	if (m_service_metadata.find(service_name) == m_service_metadata.end())
		m_service_metadata[service_name] = MetadataMap();
		
	return m_service_metadata[service_name];			
}	// end get_metadata_for
