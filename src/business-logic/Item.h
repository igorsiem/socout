/**
 * \file Item.h Declares the Item class 
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <ctime>
#include <string>
#include <map>
 
#ifndef _Item_h_included
#define _Item_h_included

/**
 * \brief A piece of content posted to one or more Services
 *
 * Items are arrangements of text (markdown) and other data such as pictures
 * and video. No matter how they are encoded, Items are treated as a single
 * 'chunk' of data.
 *
 * Different services may treat Items in different ways, and subject them to
 * different constraints (e.g. Twitter with 140 chars), but there is only
 * ever a single class for Item. Service-specific information may be
 * attached to any item (e.g. for identification) in the form of name-value
 * pairs.
 *
 * Items may be created by the user, and/or be posted on, or originate with,
 * a particular Service, but has a single, canonical date/time associated with
 * it, which is part of its identity. Thus, if an Item is posted on a
 * different Services *at the samet time,* then it is still a single Item,
 * whereas if it were posted at *different* times (even repeatedly on the
 * same Service) then these would be considered separate items.
 *
 * \todo This class is likely to undergo extensive change
 *
 * \todo We've only got text supported for an Item at this time - add
 * multimedia
 */
class Item final
{
	// --- External Interface ---
	
	public:
	
	// -- Public Sub-types --
	
	/**
	 * \brief A simple name/value string pair used for service-specific
	 * metadata
	 */
	typedef std::pair<std::string, std::string> MetadataItem;
	
	/**
	 * \brief A map of name/value pairs, used for service-specific metadata
	 */
	typedef std::map<std::string, std::string> MetadataMap;
	
	// -- Constructor / Destructor --
	
	/**
	 * \brief Constructor
	 */
	Item(
			time_t datetime,
			const std::string& headline,
			const std::string& digest,
			const std::string& text,
			const std::string& profile_image_url = "",
			const std::string& author = "") :
		m_datetime(datetime),
		m_headline(headline),
		m_digest(digest),
		m_text(text),
		m_profile_image_url(profile_image_url),
		m_author(author) {}
	
	// Default destructor and copy semantics are fine
	
	// -- Accessors --
	
	/**
	 * \brief Retrieve the date/time at which the item was created / posted
	 *
	 * \return The date/time of the Item
	 */
	time_t get_datetime(void) const { return m_datetime; }
	
	/**
	 * \brief Retrieve the headline
	 *
	 * \return The headline
	 */
	const std::string& get_headline(void) const { return m_headline; }
	
	/**
	 * \brief Retrieve the digest / summary
	 *
	 * \return The digest
	 */
	const std::string& get_digest(void) const { return m_digest; }
	
	/**
	 * \brief Retrieve the text for this item
	 *
	 * \return The text
	 */
	const std::string get_text(void) const { return m_text; }
	
	/**
	 * \brief Retrieve the URL for a profile image for this item
	 *
	 * This item may be an empty string
	 *
	 * \return The profile image URL
	 */
	const std::string& get_profile_image_url(void) const
		{ return m_profile_image_url; }
		
	/**
	 * \brief Retrieve the author of a content item
	 */
	const std::string get_author(void) const { return m_author; }
	
	/**
	 * \brief Retrieve the metadata for this item for a given service
	 *
	 * Note that if the metadata for the service does not exist in the
	 * internal list, it is created on the fly (empty).
	 *
	 * \param service_name The name of the service of the metadata to
	 * retrieve
	 *
	 * \return A reference to the metadata for the named service
	 */
	MetadataMap& get_metadata_for(const std::string& service_name);

	/**
	 * \brief Retrieve the metadata for this item for a given service
	 *
	 * Note that if the metadata for the service does not exist in the
	 * internal list, it is created on the fly (empty). Note that this is
	 * done even though this is a const method.
	 *
	 * \param service_name The name of the service of the metadata to
	 * retrieve
	 *
	 * \return A reference to the metadata for the named service
	 */
	const MetadataMap& get_metadata_for(
		const std::string& service_name) const;
	
	// --- Internal Declarations ---
	
	private:
	
	/**
	 * \brief A name or account string denoting the author of an item; may
	 * be blank
	 */
	std::string m_author;
	
	/**
	 * \brief The date/time at which the item was posted or created
	 *
	 * This is the standard posix time specification (seconds since 00:00:00
	 * UTC, 1 Jan 1970).
	 */
	time_t m_datetime;
	
	/**
	 * \brief The 'headline' of the item
	 *
	 * A one-liner heading for the item (optional)
	 */
	std::string m_headline;
	 
	/**
	 * \brief A short summary of the item
	 *
	 * This is a short digest of a longer item that might be used in a
	 * variety of contexts. It is optional, and might be the same as the
	 * headline or the full text.
	 */
	std::string m_digest;	 
	
	/**
	 * \brief The text of the item (Markdown)
	 *
	 * \todo When multimedia is supported, the Markdown may include links to
	 * these items
	 */
	std::string m_text;
	
	/**
	 * \brief The URL of an avatar / author / profile image for the item
	 *
	 * This may be blank.
	 */
	std::string m_profile_image_url;
	
	/**
	 * \brief The metadata for this item, indexed by the name of the Service
	 * from which are obtained
	 */
	mutable std::map<std::string, MetadataMap> m_service_metadata;
};	// end Item class

/**
 * \brief A container of Items, indexed by datetime
 */
typedef std::multimap<time_t, Item> Items;

#endif
