/**
 * \file Person.h Declares the Person and People classes
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <map>
#include <set>
#include <utility>

#include <QMutex>

#include <utilities/MutexGuard.h>
#include <utilities/Serialisation.h>

#ifndef _Person_h_included
#define _Person_h_included

using namespace std::rel_ops;

/**
 * \brief A record about a person, organisation or group
 *
 * A Person object is used to represent the SocOut User, as well as any
 * contacts he or she may have on different social media platforms.
 *
 * This is pretty much an identity record, with the e-mail address comprising
 * a unique key.
 */
class Person final
{
    // --- External Interface ---

    public:

    // -- Sub-types --

    /**
     * \brief A person name, comprised of first name and last (family name)
     *
     * This class includes comparison semantics based on last-name/first-name
     * ordering
     */
    class Name final
    {
        // --- Public Interface ---

    public:

        /**
         * \brief Constructor with first name and last name
         *
         * \param fn The first name
         *
         * \param ln The last name
         */
        Name(const std::string& fn, const std::string& ln) :
            m_first_name(fn), m_last_name(ln) {}

        // Default destructor and copy semantics are fine

        // -- Accessors --

        /**
         * \brief Set the first and last names in one go
         *
         * \param fn The first name
         *
         * \param ln The last name
         */
        void set_name(
                const std::string& fn,
                const std::string& ln)
            { m_first_name = fn; m_last_name = ln; }

        /**
         * \brief Retrieve the first and last names as a pair
         *
         * This method is provided for convenience.
         *
         * \return The pair with first name and last name; note that this
         * is return-by-value, not reference
         */
        std::pair<std::string, std::string> get_name(void) const
            { return std::make_pair(m_first_name, m_last_name); }

        /**
         * \brief Get the first name
         *
         * \return The first name
         */
        const std::string& get_first_name(void) const
            { return m_first_name; }

        /**
         * \brief Set the first name
         *
         * \param fn The first name to set
         */
        void set_first_name(const std::string& fn) { m_first_name = fn; }

        /**
         * \brief Retrieve the last name
         *
         * \return The last name
         */
        const std::string& get_last_name(void) const { return m_last_name; }

        /**
         * \brief Set the last name
         *
         * \param ln The last name to set
         */
        void set_last_name(const std::string& ln) { m_last_name = ln; }

        /**
         * \brief Retrieve a single string in the form
         * "<first-name><space><last-name>"
         *
         * Note that if either first or last name is empty, then the other,
         * non-empty name is returned on its own, without a space. If both
         * are empty, then an empty string is returned.
         *
         * \return The constructed name
         */
        std::string get_firstlast(void) const;

        /**
         * \brief Retrieves a single string in the form
         * "<last-name>, <first-name>"
         *
         * This is the form of the name that is used for ordering. If either
         * the first or last name is blank, then the ", " sequence in the
         * middle is not appended. This means that if either non-blank, then
         * it is returned on its own, and if both are blank, then an empty
         * string is returned.
         *
         * \param The constructed name
         */
        std::string get_lastfirst(void) const;

        // --- Comparison Semantics ---

        /**
         * \brief Equality comparison operator
         *
         * \param rhs The object with which to compare
         *
         * \return True if first and last names are equal
         */
        bool operator==(const Name& rhs) const
            { return get_lastfirst() == rhs.get_lastfirst(); }

        /**
         * \brief 'Less than' comparison operator
         *
         * Comparison is by first name then last name.
         *
         * \param rhs The object with which to compare
         *
         * \return True if self is less than rhs
         */
        bool operator<(const Name& rhs) const
            { return get_lastfirst() < rhs.get_lastfirst(); }
                
        // -- Serialisation --
        
        /**
         * \brief Convert the Name object to a serialisation value map
         *
         * \param vm The value map object that will receive the Name object
         * data (two string values with keys "first-name" and "last-name");
         * note that this is cleared before the values are set
         *
         * \return A reference to `vm` after modification         
         */
        SERIALISATION::ValueMap& copy_to(SERIALISATION::ValueMap& vm) const;
        
        /**
         * \brief Set the Name object from the given value map
         *
         * The conversion expects two string values, with keys "first-name"
         * and "last-name".
         *
         * \param vm The value map from which the set the name values
         *
         * \throw Error the value map does not include the required values
         */
        void copy_from(const SERIALISATION::ValueMap& vm);

        // --- Internal Declarations ---

    private:

        /**
         * \brief The first or given name
         */
        std::string m_first_name;

        /**
         * \brief The last or family name
         */
        std::string m_last_name;
    };  // end Name class

    // -- Construction / Destruction ---

    /**
     * \brief Constructor, initialising the e-mail address, first name and
     * last name
     *
     * \param fn The first name of the person
     *
     * \param ln The last name of the person
     *
     * \param e The email address of the person
     */
    Person(
            const std::string& fn,
            const std::string& ln,
            const std::string& e);

    /**
     * \brief Constructor using a Name object and e-mail
     *
     * \param n The name
     *
     * \param e The e-mail address
     */
    Person(const Name& n, const std::string& e) : m_name(n), m_email(e) {}

    // Default destructor is fine

    // Default copy semantics are fine

    // -- Accessors --

    /**
     * \brief Get the first name of the person
     *
     * \return The first name
     */
    const std::string& get_first_name(void) const
        { return m_name.get_first_name(); }

    /**
     * \brief Set the first name of the person
     *
     * \param fn The first name to set
     */
    void set_first_name(const std::string& fn) { m_name.set_first_name(fn); }

    /**
     * \brief Get the last name of the person
     *
     * \return The last name
     */
    const std::string& get_last_name(void) const
        { return m_name.get_last_name(); }

    /**
     * \brief Set the last name of the person
     *
     * \param ln The last name to set
     */
    void set_last_name(const std::string& ln) { m_name.set_last_name(ln); }

    /**
     * \brief Get the full name of the person
     */
    const Name& get_name(void) const { return m_name; }

    /**
     * \brief Set the full name of the person
     */
    void set_name(const Name& name) { m_name = name; }

    /**
     * \brief Get the e-mail address
     *
     * \return The e-mail address
     */
    const std::string& get_email(void) const { return m_email; }

    /**
     * \brief Set the email address
     *
     * \param e The e-mail address
     */
    void set_email(const std::string& e) { m_email = e; }
    
    // -- Serialisation --
        
    /**
     * \brief Convert the Person object to a serialisation value map
     *
     * \param vm The value map object that will receive the Person object
     * data; note that this is cleared before the values are set
     *
     * \return A reference to `vm` after modification         
     */
    SERIALISATION::ValueMap& copy_to(SERIALISATION::ValueMap& vm) const;
    
    /**
     * \brief Set the Person object from the given value map
     *
     * \param vm The value map from which the set the person values
     *
     * \throw Error the value map does not include the required values
     */
    void copy_from(const SERIALISATION::ValueMap& vm);

    // --- Internal Attributes ---

private:

    /**
     * \brief The name of the person
     */
    Name m_name;

    /**
     * \brief The e-mail address of the person
     */
    std::string m_email;
};  // end Person class

/**
 * \brief A thread-safe collection of Person objects, with e-mail address as
 * the unique key, but also searchable by name
 *
 * Note that because this container needs to be thread-safe, normal iteration
 * is not supported. Code can access specific Person objects by e-mail
 * address, or retrieve vectors of e-mail addresses that can then be iterated
 * externally, and retrieving the Person objects in that way. It's obviously
 * less efficient, but maintains thread-safety.
 */
class People final
{
    // --- External Interface ---

public:

    /**
     * \brief Constructor - creates an empty container
     */
    People(void) : m_mutex(), m_people(), m_name_index() {}

    // Default destructor is fine

    // -- Person Management --

    /**
     * \brief Add a new Person object to the container
     *
     * \param p The person to add
     *
     * \throw Error A person with the e-mail address is already a member of
     * the container
     */
    void insert(const Person& p);

    /**
     * \brief The number of person objects in the container
     *
     * \return The size of the container
     */
    std::size_t size(void) const { return m_people.size(); }

    /**
     * \brief Retrieve all e-mail addresse
     *
     * \param addresses A reference to the container to receive the e-mail
     * addresses; this is not emptied prior to filling      
     *
     * \return A reference to `addresses`
     */
    std::set<std::string>& get_emails(
        std::set<std::string>& addresses) const;
        
    /**
     * \brief Retrieve all names
     *
     * Note that this will not be same number of items as retrieved by
     * `get_emails`. Multiple instances of the same name are only returned
     * once.
     *
     * \param names A reference to a container to receive the names; this is
     * not emptied prior to insertion
     *
     * \return A reference to `names`
     */
    std::set<Person::Name>& get_names(std::set<Person::Name>& names) const;

    /**
     * \brief Whether or not the container has a Person object with the given
     * e-mail address
     *
     * \param e The e-mail address to check
     *
     * \return True if there is a Person object with the given e-mail
     */
    bool has_person_with_email(const std::string& e) const;

    /**
     * \brief Retrieve the Person object with the given e-mail address
     *
     * Note that this is intentionally a return-by-value function. It's less
     * efficient, but allows us to maintain thread-safety. Use with care...
     *
     * \param e The e-mail address
     *
     * \return The Person object
     *
     * \throw Error There is no Person object with the given e-mail
     */
    Person get_person_with_email(const std::string& e) const;

    /**
     * \brief Updates the existing person with the given e-mail address
     *
     * The existing person record (indexed by e-mail address) is updated. If
     * there is no Person object with the given e-mail address, an exception
     * is thrown (to ensure that only updating of existing Person objects is
     * done, not insertion, which should be done using `insert` method).
     *
     * It is necessary to do updates like this (instead of directly editing
     * a reference to the Person object) for thread-safety.
     *
     * \param person The new person details
     */
    void update(const Person& person);
    
    /**
     * \brief Retrieve the e-mail addresses of everyone with the given name
     *
     * \param name The name for which to search
     *
     * \param addresses A reference to the container to receive the e-mail
     * addresses; this is not emptied prior to filling
     *
     * \return A reference to `addresses`          
     */
    std::set<std::string>& get_emails_by_name(
        const Person::Name& name,
        std::set<std::string>& addresses) const;

    // Note that we don't provide a '[]' operator for retrieving people
    // by their e-mail address. We don't want to imply that Person objects
    // can be retrieved by reference (and therefore used as l-values).

    /**
     * \brief Delete a person object from the container (by their e-mail
     * address)
     *
     * Note that if a person with the given e-mail address is NOT found for
     * deletion, and exception is NOT signalled, but `false` is returned.
     *
     * \param e The e-mail address of the Person record to delete
     *
     * \return True if a person record was found and deleted; false if a
     * Person with the given e-mail address was not found
     */
    bool erase(const std::string& e);
    
    /**
     * \brief Clear the container
     */
    void clear(void) { GUARD(m_mutex); m_people.clear(); m_name_index.clear(); }
    
    // -- Serialisation --
    
    /**
     * \brief Convert the Person object to a serialisation vector
     *
     * \param vv The value vector object that will receive the People 
     * collection data; note that this is cleared before the values are set
     *
     * \return A reference to `vv` after modification         
     */
    SERIALISATION::ValueVector& copy_to(
        SERIALISATION::ValueVector& vv) const;
    
    /**
     * \brief Set the People collection from the given value vector
     *
     * Note that the existing collection is cleared prior to filling.
     *
     * \param vv The value vector from which the people data comes
     *
     * \throw Error the value vector includes incorrect structures
     */
    void copy_from(const SERIALISATION::ValueVector& vv);
    
    // -- Copy Semantics --
    
    /**
     * \brief Copy constructor
     *
     * \param rhs The object from which to copy; note that the internal mutex
     * of this object is locked during the operation so that we get a
     * consistent copy in MT situations.
     */
    People(const People& rhs);
    
    /**
     * \brief Assignment operator
     *
     * \param rhs The object from which to copy; note that the internal mutex
     * of this object is locked during the operation so that we get a
     * consistent copy in MT situations.
     *
     * \return A reference to copied self
     */
    People& operator=(const People& rhs);

    // --- Internal Declarations ---

private:

    // -- Internal Types --

    /**
     * \brief A map of email addresses, multi-indexed by name
     */
    typedef std::multimap<Person::Name, std::string> EmailsByNameMultimap;

    /**
     * \brief A non-const iterator in an Emails-by-Name index
     */
    typedef EmailsByNameMultimap::iterator EmailsByNameIterator;

    /**
     * \brief A const iterator in an Emails-by-Name index
     */
    typedef EmailsByNameMultimap::const_iterator EmailsByNameConstIterator;
    
    /**
     * \brief A value range in an EmailsByName index collection
     */
    typedef std::pair<EmailsByNameIterator, EmailsByNameIterator>
        EmailsByNameRange;

    /**
     * \brief A const value range in an EmailsByName index collection
     */
    typedef std::pair<EmailsByNameConstIterator, EmailsByNameConstIterator>
        EmailsByNameConstRange;

    // -- Attributes --

    /**
     * \brief The mutex guarding the internals of this collection
     */
    mutable QMutex m_mutex;

    /**
     * \brief The collection of person objects, indexed by e-mail.
     */
    std::map<std::string, Person> m_people;

    /**
     * \brief An index of names to e-mail addresses.
     *
     * Note that names need not be unique, so there may be multiple people
     * with the same name.
     */
    EmailsByNameMultimap m_name_index;
};  // end People class

#endif // PERSON_H
