/**
 * \file Error.h Declares the Error exception class, and associated macros
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <sstream>
#include <stdexcept>

#ifndef _Error_h_included
#define _Error_h_included

/**
 * \brief A class for signalling application-level errors in SocOut
 */
class Error : public std::exception
{
    // --- External Inteface ---

public:

    /**
     * \brief Constructor, initialising the message
     *
     * \param msg A human-readable message, which can be a accessed using the
     * `what` method
     */
    Error(const std::string& msg);

    /**
     * \brief Trivial destructor
     */
    virtual ~Error(void) throw() {}

    /**
     * \brief Retrieve the error message
     *
     * \return The error message
     */
    const char* what(void) const throw() override
        { return m_message.c_str(); }

    // -- Copy Semantics --

    /**
     * \brief Copy constructor
     *
     * \param rhs The object from which to copy
     */
    Error(const Error& rhs);

    /**
     * \brief Assignment operator
     *
     * \param rhs The object from which to copy
     *
     * \return A reference to copied self
     */
    Error& operator=(const Error& rhs);

    // --- Internal Declarations ---

protected:

    /**
     * \brief A human-readable description of the error
     */
    std::string m_message;
};  // end Error class

/**
 * \brief Throw an Error object as an exception
 *
 * The message argument supports arguments streamed using the `<<` operator.
 *
 * \param message The error message
 */
#define RAISE_ERROR( message ) \
{ \
    std::stringstream msg; \
    msg << message; \
    throw ::Error(msg.str()); \
}

#endif
