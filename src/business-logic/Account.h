/**
 * \file Account.h Declares the Account class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <map>
#include <set>
#include <memory>

#include <utilities/Serialisation.h>

#ifndef _Account_h_included
#define _Account_h_included

/**
 * \brief A base-class encapsulating an account on a social media Service
 *
 * Each Service-derived class will have an associated account class that is
 * derived from Account, encapsulating the accounts for that Service. 
 */
class Account
{
	// --- External Interface ---
	
	public:
	
	/**
	 * \brief Constructor
	 *
	 * \param s The service object to which this account belongs
	 */
	Account(void) {}
	
	/**
	 * \brief Trivial destructor
	 */
	virtual ~Account(void) {}
	
	/**
	 * \brief Retrieve the name of the Service to which this Account belongs
	 *
	 * \return The Service name object
	 */
	virtual std::string get_service_name(void) const = 0;
	
	/**
	 * \brief Retrieve an ID for this account that is unique for the given
	 * Service
	 *
	 * This will usually be something like the account username or e-mail
	 * address. A combination of the Service name and this ID forms a unique
	 * key across all Account objects.
	 *
	 * \return The identifier as a string
	 */
	virtual std::string get_id(void) const = 0;
	
	/**
	 * \brief Clone method for polymorphic copying
	 *
	 * \return A reference to a copy of self; caller to delete
	 */
	virtual Account* clone(void) const = 0;
	
	/**
	 * \brief Put the data about the Account into the given serialisation
	 * ValueMap
	 *
	 * This method puts the name of the Service into the value map (key
	 * "service-name"). Implementations in derived classes should call this
	 * base-class version as well.
	 *
	 * Note that Accounts do not have a corresponding method to read values
	 * from serialisation ValueMap. This is because the Service object does
	 * the work of creating the Account object, after doing online
	 * validation.
	 *
	 * \param vm The value map into which the values are to be placed
	 *
	 * \return A reference to `vm`
	 */
	virtual SERIALISATION::ValueMap& copy_to(SERIALISATION::ValueMap& vm) const
	{
		vm["service-name"] = SERIALISATION::Value(get_service_name());
		return vm;
	}	// end copy_to method
};  // end Account class

/**
 * \brief A unique account identifier, consisting of the service name and
 * the ID
 *
 * Items of this type are used to identify Accounts in collections
 */
typedef std::pair<std::string, std::string> AccountIdentifier;

/**
 * \brief A set of Account IDs
 */
typedef std::set<AccountIdentifier> AccountIdentifiers;

/**
 * \brief A polymorphic collection of pointers to account objects, indexed by
 * account identifiers
 */
typedef std::map<AccountIdentifier, Account*> Accounts;
	
/**
 * \brief A shared pointer to an Account
 *
 * This is useful when retrieving Account objects from thread-safe
 * collections (like the DataFile), because the objects have to be cloned.
 */
typedef std::shared_ptr<Account> AccountSharedPtr;

/**
 * \brief A list of shared pointers to Accounts
 *
 * This is useful when retrieving Account objects from thread-safe
 * collections (like the DataFile), because the objects have to be cloned
 */
typedef std::list<AccountSharedPtr> AccountSharedPtrList;

#endif // ACCOUNT_H
