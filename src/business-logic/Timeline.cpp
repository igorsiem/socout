/**
 * \file Timeline.h Implements the Timeline class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "Timeline.h"
#include "Error.h"

SERIALISATION::ValueMap& Timeline::copy_to(
		SERIALISATION::ValueMap& vm) const
{
	vm.insert(std::make_pair(
		"name",
		SERIALISATION::Value(m_name)));
		
	SERIALISATION::ValueVector account_ids_vv;
	for (auto account_id : m_account_ids)
	{
		SERIALISATION::ValueMap account_id_vm;
		account_id_vm.insert(
			std::make_pair(
				"service-name",
				SERIALISATION::Value(account_id.first)));
		account_id_vm.insert(
			std::make_pair(
				"id",
				SERIALISATION::Value(account_id.second)));
				
		account_ids_vv.push_back(SERIALISATION::Value(account_id_vm));
	}	// end account_id loop
	
	vm.insert(std::make_pair(
		"account-ids",
		SERIALISATION::Value(account_ids_vv)));
	
	return vm;
}	// end copy_to method
	
void Timeline::copy_from(const SERIALISATION::ValueMap& vm)
{
	// Look for the top-level elements.
	SERIALISATION::ValueMap::const_iterator
		service_name_itr = vm.find("name"),
		account_ids_itr = vm.find("account-ids");
		
	if (service_name_itr == vm.end())
		RAISE_ERROR("Timeline data structure has no \"name\" element.");
		
	m_name = service_name_itr->second.get_as_string();
	
	if (account_ids_itr == vm.end())
		RAISE_ERROR("Timeline \"" << m_name << "\" has no \"account-ids\" "
			"element.");
			
	if (account_ids_itr->second.get_type() != SERIALISATION::Value::t_vector)
		RAISE_ERROR("Timeline \"" << m_name << "\" has an \"account-ids\" "
			"element that is not a proper list structure.");
	
	const SERIALISATION::ValueVector& account_ids_vv =
		account_ids_itr->second.as_vector();
	for (auto account_id_val : account_ids_vv)
	{
		// Check our account ID components.
		if (account_id_val.get_type() != SERIALISATION::Value::t_map)
			RAISE_ERROR("Timeline \"" << m_name << "\" has an account ID "
				"that is not a proper name/value pair structure.");
				
		SERIALISATION::ValueMap::const_iterator
			service_name_itr = account_id_val.as_map().find("service-name"),
			id_itr = account_id_val.as_map().find("id");
			
		if (service_name_itr == account_id_val.as_map().end())
			RAISE_ERROR("Timelime \"" << m_name << "\" has an account ID "
				"with no \"service-name\" attribute.");
				
		if (id_itr == account_id_val.as_map().end())
			RAISE_ERROR("Timeline \"" << m_name << "\" has an account ID "
				"with no \"id\" attribute.");
	
		// Insert the new account ID into our list.
		m_account_ids.insert(std::make_pair(
			service_name_itr->second.get_as_string(),
			id_itr->second.get_as_string()));
	}
}	// end copy_from
