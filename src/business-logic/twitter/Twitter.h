/**
 * \file Twitter.h Declares the Twitter service class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctime>
#include <string>

#include <QMutex>
 
#include <twitcurl/twitcurl.h>
#include <utilities/Serialisation.h>
 
#include "../Service.h"
#include "../Account.h"
#include "../Item.h"

#ifndef _twitter_Twitter_h_included
#define _twitter_Twitter_h_included

/**
 * \brief The Twitter service class
 *
 * This is a thread-safe singleton encapsulating all the app-wide
 * functionality associated with Twitter.
 */
class Twitter final : public Service
{
	// --- External Interface --- 
	
	public:
	
	/**
	 * \brief A Twitter account
	 *
	 * Note that we do not store a password for the account, just an app-
	 * specific token key and secret.
	 */
	class Account final : public ::Account
	{
		// Note that only the Twitter service can create Twitter account
		// objects
		friend class Twitter;
		
		public:
		
		// Declare the destructor public, because Account objects are owned
		// by Person objects.
		
		/**
		 * \brief Trivial destructor
		 */
		virtual ~Account(void) {}
		
		/**
		 * \brief Retrieve the name of the Service to which this Account belongs
	 	 *
		 * \return The Service name object
	 	 */
		virtual std::string get_service_name(void) const override
			{ return Twitter::get_twitter().get_name(); }
			
    	/**
    	 * \brief Retrieve an ID for this account that is unique for the given
    	 * Service
    	 *
    	 * This method returns the username for Twitter accounts
		 *
		 * \return The Twitter username
    	 */
    	virtual std::string get_id(void) const override { return m_username; }
		
		// -- Accessors --
		
		/**
		 * \brief Retrieve the username of the account
		 *
		 * Note: this is the same as the ID returned by the `get_id` method
		 *
		 * \return The username
		 */
		const std::string& get_username(void) const { return m_username; }
			
		/**
		 * Retrieve the token key for the validated account
		 *
		 * \return The token key
		 */
		const std::string& get_token_key(void) const { return m_token_key; }
		
		/**
		 * \brief Retrieve the token secret for the account
		 *
		 * \return The token secret
		 */
		const std::string& get_token_secret(void) const
			{ return m_token_secret; }
			
		// -- Copy Semantics --
		
		/**
		 * \brief Copy constructor
		 *
		 * \param rhs A reference to copied self
		 */
		Account(const Twitter::Account& rhs) :
			::Account(rhs),
			m_username(rhs.m_username),
			m_token_key(rhs.m_token_key),
			m_token_secret(rhs.m_token_secret)
		{}
		
		/**
		 * \brief Assignment operator
		 *
		 * \param rhs The object from which to copy
		 *
		 * \return A reference to copied self
		 */
		Twitter::Account& operator=(const Twitter::Account& rhs)
		{
			::Account::operator=(rhs);
			m_username = rhs.m_username;
			m_token_key = rhs.m_token_key;
			m_token_secret = rhs.m_token_secret;
		}	// end assignment operator
		
		/**
	 	 * \brief Clone method for polymorphic copying
	 	 *
	 	 * \return A reference to a copy of self; caller to delete
	 	 */
		virtual ::Account* clone(void) const override
			{ return new Twitter::Account(*this); }
		
		// -- Serialisation --
		
    	/**
    	 * \brief Put the data about the Account into the given serialisation
    	 * ValueMap
    	 *
    	 * Note that Accounts do not have a corresponding method to read
		 * values from serialisation ValueMap. This is because the Service
		 * object does the work of creating the Account object, after doing
		 * online validation.
    	 *
    	 * \param vm The value map into which the values are to be placed
    	 *
    	 * \return A reference to `vm`
    	 */
        virtual SERIALISATION::ValueMap& copy_to(
			SERIALISATION::ValueMap& vm) const override;
        
        /**
         * \brief Set the Name object from the given value map
         *
         * \param vm The value map from which the set the values
         *
         * \throw Error the value map does not include the required values
         */
        // void copy_from(const SERIALISATION::ValueMap& vm);
		
		 // --- Internal Declarations ---
		 
		 private:
		 
		/**
		 * \brief Constructor, initialising all parameters
		 *
		 * \param username The username of the account
		 *
		 * \param token_key The access token key assigned by Twitter
		 *
		 * \param token_secret The access token secret assigned by Twitter
		 */
		Account(
			const std::string& username,
			const std::string& token_key,
			const std::string& token_secret);
			
		/**
		 * \brief The username of the account
		 */
		std::string m_username;
		
		/**
		 * \brief The access token key for the account
		 *
		 * This is assigned by Twitter when the user grants access to the
		 * app. 
		 */
		std::string m_token_key;
		
		/**
		 * \brief The token secret
		 *
		 * This is assigned by Twitter when the user grants access to the
		 * app
		 */
		std::string m_token_secret;
	};	// end Account class
	
	/**
	 * \brief Retrieve the single instance of this class
	 *
	 * \return A reference to the single Twitter service object
	 */
	static Twitter& get_twitter(void);
	
	/**
	 * \brief Retrieve the unique name of the service as a string
	 *
	 * \return The name of the service
	 */
	virtual std::string get_name(void) const override { return "Twitter"; }
	
	// -- Account Creation --
	
	/**
	 * \brief Authenticate app for use with a Twitter account
	 *
	 * This method takes the username and password of a user's account, and
	 * uses them to authenticate SocOut to use the account. The password is
	 * *not* retained. Instead, the Account object retains the Twitter-
	 * assigned token key and secret.
	 *
	 * Note that this method can take an appreciable length of time to
	 * execute, and should possibly be done in a background thread.This is
	 * safe to do, as the Twitter object is thread-safe.
	 *
	 * \param username The username of the account
	 *
	 * \param password The true password of the account; note that this is
	 * NOT retained
	 *
	 * \return A pointer to a new Account object; it is the caller's
	 * responsibility to destroy this
	 *
	 * \throw Error Authenticatation failed
	 */
	Account* authenticate_account(
		const std::string& username,
		const std::string& password);
		
	/**
	 * \brief Recreate an Account object from a pre-authenticated token key
	 * and secret
	 *
	 * \param username The username of the account
	 *
	 * \param token_key The token key assigned by Twitter to SocOut for the
	 * account
	 *
	 * \param token_secret The token secret assigned by Twitter to SocOut
	 * for the account
	 *
	 * \return A pointer to a new Account object; it is the caller's
	 * responsibility to destroy this
	 *
	 * \throw Error Checking account credentials failed
	 */
	Account* recreate_account(
		const std::string& username,
		const std::string& token_key,
		const std::string& token_secret);
		
	/**
	 * \brief Recreate an Account object from a pre-authenticated token key
	 * and secret (expressed in a serialisation ValueMap)
	 *
	 * \param vm The ValueMap; this should contain items named "username",
	 * "token-key" and "token-secret"; any other items are ignored
	 *
	 * \return A pointer to a new Account object; it is the caller's
	 * responsibility to destroy this
	 *
	 * \throw Error The `vm` parameter did not include a required item, or
	 * checking account credentials failed
	 */
	Account* recreate_account(const SERIALISATION::ValueMap& vm);
	
	// -- Item Management --
	
	/**
	 * \brief Retrieve the most recent items for the given Account (which
	 * must be a Twitter account)
	 *
	 * \todo Implementations of this can be lengthy; it may pay to introduce
	 * an MT interface
	 *
	 * \param account The account object for which the items are to be
	 * returned; if this is not a Twitter account, an exception is thrown
	 *
	 * \param items The container to receive the items; this is not cleared
	 * prior to filling
	 *
	 * \param max The maximum number of items to retrieve; there are lots of
	 * reasons why the full number may not be returned
	 * 
	 * \return A reference to `items`
	 *
	 * \throw Error A problem occurred while retrieving the items
	 *
	 * \todo Item retrieval just retrieves the 'home' items of the account;
	 * `max` is ignored
	 */
	virtual Items& get_recent(
		::Account& account,
		Items& items,
		std::size_t max = 50) override;

	/**
	 * \brief Post an Item
	 *
	 * The Twitter implementation uses the digest of the Item, unless it is
	 * blank. If the digest is blank, then the headline is used, and if that
	 * is blank, then the text is used. The 140 character limit will
	 * obviously apply, and an exception is thrown if the Item's digest,
	 * headline and text are all blank.
	 *
	 * \param account The account to use to post the item on the service
	 * (an exception will be thrown if the account is not for this Service)
	 *
	 * \param item The item to post
	 *
	 * \throw Error There was a problem posting the item; either it was
	 * invalid for the service, there was a problem with the Account (e.g.
	 * invalid), or the Service itself has a problem
	 */
	virtual void post(::Account& account, const Item& item) override;

	// --- Internal Declarations ---
	
	private:
	
	// -- Internal Methods --
	
	// - Constructor / Destructor -
	
	/**
	 * \brief Trivial constructor
	 */
	Twitter(void) : Service(), m_mutex() {}
	
	/**
	 * \brief Trivial destructor
	 */
	virtual ~Twitter(void) {}
	
	// - Process Twitter Responses -
	
	/**
	 * \brief Process the response from the most recent twitCurl operation
	 *
	 * The response is parsed into a SERIALISATION::Value object. If it is a
	 * with an "errors" item, then an exception is thrown containing the
	 * errors. If not, then the value is returned.
	 *
	 * \param twitter The twitCurl object
	 *
	 * \param v The value into which the response is parsed
	 *
	 * \return A reference to v after being filled with data
	 *
	 * \throw Error Twitter returned an error response
	 */
	static SERIALISATION::Value& process_response(
		twitCurl& twitter,
		SERIALISATION::Value& v);
		
	/**
	 * \brief Process a cURL error and throw an exception
	 *
	 * This method should *only* if a twitCurl method returns 'false' to
	 * that an error has occurred. This method extracts the error, and throws
	 * it as an exception.
	 *
	 * If the twitCurl error string is empty, an exception is still thrown,
	 * because it is assumed that an error has occured.
	 *
	 * \param twitter The twitcurl object
	 *
	 * \thow Error The twitCurl error message
	 */
	static void raise_twitcurl_error(twitCurl& twitter);
	
	// - Other Utility Methods -
	
	/**
	 * \brief Parse the given Twitter date/time string into a standard
	 * time_t number
	 *
	 * Note that this method uses the QRegExp and QDateTime class from Qt for
	 * convenience, but need not do so, if someone wants to write up some
	 * generic way to do it.
	 *
	 * \param dts The Twitter date/time string 
	 *
	 * \throw Error The date/time string could not be parsed
	 */
	static time_t to_time(const std::string& dts);
	
	// -- Attributes --
	
	/**
	 * \brief The mutex that protects the internals of the Twitter service
	 * object
	 */
	mutable QMutex m_mutex;
	
	/**
	 * \brief The token key for the application
	 */
	static const std::string m_app_token_key;
	
	/**
	 * \brief The token secret for the application
	 */
	static const std::string m_app_token_secret;
};	// end class Twitter

#endif
