/**
 * \file Twitter.cpp Implements the Twitter service class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <sstream>

#include <QRegExp>
#include <QDateTime>
#include <QStringList>

#include <utilities/MutexGuard.h>
#include <utilities/Log.h>

#include "Twitter.h"
#include "../Error.h"
#include "../Configuration.h"
#include "../tokens.h"

const std::string Twitter::m_app_token_key = TWITTER_APP_KEY;
	
const std::string Twitter::m_app_token_secret = TWITTER_APP_SECRET;

Twitter::Account::Account(
		const std::string& username,
		const std::string& token_key,
		const std::string& token_secret) :
	m_username(username),
	m_token_key(token_key),
	m_token_secret(token_secret)			
{
}	// end constructor method

SERIALISATION::ValueMap& Twitter::Account::copy_to(
		SERIALISATION::ValueMap& vm) const
{
	::Account::copy_to(vm);
	
	vm["username"] = SERIALISATION::Value(m_username);
	vm["token-key"] = SERIALISATION::Value(m_token_key);
	vm["token-secret"] = SERIALISATION::Value(m_token_secret);
	
	return vm;
}	// end copy_to method
        
Twitter& Twitter::get_twitter(void)
{
	static Twitter t;
	return t;	
}	// end get_twitter method

Twitter::Account* Twitter::authenticate_account(
		const std::string& username,
		const std::string& password)
{
	GUARD(m_mutex);

	twitCurl twitter;
	
	// Set the username and password.
	twitter.setTwitterUsername(username);
	twitter.setTwitterPassword(password);
	
	// OpenAuth flow begins here.
	//
	// Step 0: Set our consumer key and secret.
	//
	// These are hardcoded, and assigned by Twtiter.
    twitter.getOAuth().setConsumerKey(m_app_token_key);
    twitter.getOAuth().setConsumerSecret(m_app_token_secret);
	
	// Step 1: Get a URL to request an authorisation PIN from Twitter
	std::string auth_url;
	twitter.oAuthRequestToken(auth_url);
	
	// Step 2: Sort out PIN automatically
	//
	// TODO It is more trustworthy to let the user do this step manually in a
	// browser.
	twitter.oAuthHandlePIN(auth_url);

	// Step 3: Swap out our authorisation tokens for our access tokens.
    twitter.oAuthAccessToken();
    	
	// Step 4: Get our keys out for storage.
	std::string token_key, token_secret;
    twitter.getOAuth().getOAuthTokenKey(token_key);
    twitter.getOAuth().getOAuthTokenSecret(token_secret);
	
	// Verify credentials and throw an exception if there is a problem.
	if (twitter.accountVerifyCredGet() == false)
		raise_twitcurl_error(twitter);
		
	SERIALISATION::Value credential_info;
	process_response(twitter, credential_info);
	
	LOG_INFO("Credentials for Twitter account \"" << username << "\" were "
		"obtained and verified.");
	
	return new Account(username, token_key, token_secret);
}	// end authenticate_account method
		
Twitter::Account* Twitter::recreate_account(
		const std::string& username,
		const std::string& token_key,
		const std::string& token_secret)
{
	GUARD(m_mutex);
	
	// Check the credentials, throwing an exception if there is a problem.
	twitCurl twitter;
    twitter.getOAuth().setConsumerKey(m_app_token_key);
    twitter.getOAuth().setConsumerSecret(m_app_token_secret);
	twitter.getOAuth().setOAuthTokenKey(token_key);
	twitter.getOAuth().setOAuthTokenSecret(token_secret);

	// Verify the credentials if we're configured to.
	if (Configuration::get_configuration().get_verify_accounts_on_load())
	{
		if (twitter.accountVerifyCredGet() == false)
			raise_twitcurl_error(twitter);
		
		SERIALISATION::Value credential_info;
		process_response(twitter, credential_info);
	
		LOG_INFO("Existing credentials for Twitter account \"" << username <<
			"\" were verified.");
	}	// end if we are verifying the credentials
	
	// Recreate account object.
	return new Account(username, token_key, token_secret);
}	// end recreate_account method

Twitter::Account* Twitter::recreate_account(
		const SERIALISATION::ValueMap& vm)
{
	// Retrieve the account items from the value map.
	SERIALISATION::ValueMap::const_iterator
		username_itr = vm.find("username"),
		token_key_itr = vm.find("token-key"),
		token_secret_itr = vm.find("token-secret");
		
	if ((username_itr == vm.end()) ||
			(token_key_itr == vm.end()) ||
			(token_secret_itr == vm.end()))
		RAISE_ERROR("Serialised Twitter account details do not include all "
			"required attributes.");
			
	// Call the other overload of this method - note that this locks the
	// mutex for us.
	return recreate_account(
		username_itr->second.get_as_string(),
		token_key_itr->second.get_as_string(),
		token_secret_itr->second.get_as_string()); 
}	// end recreate_account

Items& Twitter::get_recent(
		::Account& account,
		Items& items,
		std::size_t max)
{
	GUARD(m_mutex);
	
	// Make sure that we have a Twitter account.
	::Twitter::Account* twitter_account =
		dynamic_cast<::Twitter::Account*>(&account);
	if (twitter_account == NULL)
		RAISE_ERROR("Account \"" << account.get_id() << "\" is not a "
			"Twitter account.");
		
	// Set up the twitCurl object with our credentials.	
	twitCurl twitter;
    twitter.getOAuth().setConsumerKey(m_app_token_key);
    twitter.getOAuth().setConsumerSecret(m_app_token_secret);
	twitter.getOAuth().setOAuthTokenKey(twitter_account->get_token_key());
	twitter.getOAuth().setOAuthTokenSecret(
		twitter_account->get_token_secret());
		
	// Now get the account recent times, checking the result.
	//
	// TODO This does not take `max` into account when retrieving.
	if (twitter.timelineHomeGet() == false)	
		raise_twitcurl_error(twitter);
		
	SERIALISATION::Value items_v;
	process_response(twitter, items_v);
	
	// Retrieve the items.
	if (items_v.get_type() != SERIALISATION::Value::t_vector)
		RAISE_ERROR("The response from Twitter was not a correct list "
			"structure.");
	for (auto item_v : items_v.as_vector())
	{
		// Each item must be a map.
		if (item_v.get_type() != SERIALISATION::Value::t_map)
			RAISE_ERROR("The response from Twitter includes an entry that "
				"is not a correct name/value structure.");

		// Make sure we have all the elements we need.
		//
		// TODO We'll probably have more item stuff to put in here later on.
		SERIALISATION::ValueMap::const_iterator
			id_itr = item_v.as_map().find("id"),
			created_at_itr = item_v.as_map().find("created_at"),
			user_itr = item_v.as_map().find("user"),
			text_itr = item_v.as_map().find("text");
					
		if (id_itr == item_v.as_map().end())
			RAISE_ERROR("The response from Twitter included an entry that "
				"does not have an \"id\" attribute.");	
		if (created_at_itr == item_v.as_map().end())
			RAISE_ERROR("The response from Twitter included an entry that "
				"does not have a \"created_at\" attribute.");
		if (text_itr == item_v.as_map().end())
			RAISE_ERROR("The response from Twitter included an entry that "
				"does not have a \"text\" attribute.");
								
		// Get the details of the user that posted the item
		std::string profile_image_url;
		
		if (user_itr == item_v.as_map().end())
			RAISE_ERROR("The response from Twitter included an entry that "
				"does not have a \"user\" attribute.");

		if (user_itr->second.get_type() != SERIALISATION::Value::t_map)
			RAISE_ERROR("The response from Twitter included an entry with "
				"a user structure that does not have properly structured "
				"name/value pairs.");
					
		SERIALISATION::ValueMap::const_iterator profile_image_url_itr =
				user_itr->second.as_map().find("profile_image_url_https");
				
		if (profile_image_url_itr == user_itr->second.as_map().end())
		{
			LOG_DEBUG("Twitter item with id " <<
				id_itr->second.get_as_string() << " has a \"user\" "
				"attribute with no https profile image URL attribute.");
		}
		else profile_image_url =
			profile_image_url_itr->second.get_as_string();
				
		SERIALISATION::ValueMap::const_iterator screen_name_itr =
			user_itr->second.as_map().find("screen_name");
		if (screen_name_itr == user_itr->second.as_map().end())
			RAISE_ERROR("The response from Twitter included an entry with "
				"a user structure that has no \"screen_name\" attribute.");
				
		// TODO we may want to pull out the ID of the posting user as well.
				
		// Now we can create our item.
		Item item(
			to_time(created_at_itr->second.get_as_string()),	// date/time
			"", 										// No headline
			text_itr->second.get_as_string(),			// digest
			"",											// text is blank
			profile_image_url,							// URL of profile image
			screen_name_itr->second.get_as_string()		// author
		);
		
		// Add the item ID and the account ID to the Twitter metadata.
		//
		// TODO More twitter-specific stuff goes here.
		Item::MetadataMap& twitter_metadata =
			item.get_metadata_for(get_name());
		twitter_metadata["item-id"] = id_itr->second.get_as_string();
		twitter_metadata["querying-account-id"] = account.get_id();
		twitter_metadata["source-user-screen-name"] =
			screen_name_itr->second.get_as_string();
			
		// Add our item to the container.
		items.insert(std::make_pair(item.get_datetime(), item));
	}	// end item value loop
	
	return items;
}	// end get_recent

void Twitter::post(::Account& account, const Item& item)
{
	GUARD(m_mutex);

	// Make sure that we have a Twitter account.
	::Twitter::Account* twitter_account =
		dynamic_cast<::Twitter::Account*>(&account);
	if (twitter_account == NULL)
		RAISE_ERROR("Account \"" << account.get_id() << "\" is not a "
			"Twitter account.");
		
	// Set up the twitCurl object with our credentials.	
	twitCurl twitter;
    twitter.getOAuth().setConsumerKey(m_app_token_key);
    twitter.getOAuth().setConsumerSecret(m_app_token_secret);
	twitter.getOAuth().setOAuthTokenKey(twitter_account->get_token_key());
	twitter.getOAuth().setOAuthTokenSecret(
		twitter_account->get_token_secret());
		
	// Work out what our message is, and make sure that we have something to
	// post.
	std::string message = item.get_digest();
	if (message.empty()) message = item.get_headline();
	if (message.empty()) message = item.get_text();
	if (message.empty())
		RAISE_ERROR("An attempt was made to post a blank status to Twitter.");
		
	if (twitter.statusUpdate(message) == false)
		raise_twitcurl_error(twitter);
		
	SERIALISATION::Value credential_info;
	process_response(twitter, credential_info);
}	// end post

SERIALISATION::Value& Twitter::process_response(
		twitCurl& twitter,
		SERIALISATION::Value& v)
{
	// Retrieve the last web response and parse it.
	std::string response;
	twitter.getLastWebResponse(response);
	
	std::stringstream rsp;
	rsp << response;
	
	SERIALISATION::JSONSerialiser ser;
	v = SERIALISATION::Value();
	ser.read(rsp, v);
	
	// If the response is a map, has an "errors" item, then throw an
	// exception.
	if ((v.get_type() == SERIALISATION::Value::t_map) &&
			(v.as_map().find("errors") != v.as_map().end()))
	{
		// Retrieve the errors item, and check to see if it is a vector.
		// If it isn't, then signal an unspecified error message.
		const SERIALISATION::Value& errors = v.as_map()["errors"];
		if (errors.get_type() != SERIALISATION::Value::t_vector)
			RAISE_ERROR("Twitter has indicated that an error occurred, but "
				"no error message was supplied.");
				
		// How many errors have we got?
		if (errors.as_vector().size() == 0)
		{
			RAISE_ERROR("Twitter has indicated that an error has occurred, "
				"but no error message was supplied.");
		}
		else if (errors.as_vector().size() == 1)
		{
			// Extract a single error.
			const SERIALISATION::Value& error = errors.as_vector()[0];
			
			// Look for the error code and message.
			std::string code_str = "unknown", message_str = "not supplied";
			
			if (error.get_type() != SERIALISATION::Value::t_map)
				RAISE_ERROR("Twitter indicated that an error occurred, but "
					"did not supply a recognised error description.");
					
			SERIALISATION::ValueMap::const_iterator
				code_itr = error.as_map().find("code"),
				message_itr = error.as_map().find("message");
				
			if (code_itr != error.as_map().end())
				code_str = code_itr->second.get_as_string();
				
			if (message_itr != error.as_map().end())
				message_str = message_itr->second.get_as_string();
				
			RAISE_ERROR("Twitter error: " << message_str << " (code " <<
				code_str << ")");
		}	// end if we have single error
		else
		{
			// Extract multiple errors.
			std::stringstream error_message_strm;
			error_message_strm << "Twitter has signalled the following "
				"errors:";
			for (auto error : errors.as_vector())
			{
				// Look for the error code and message.
				std::string code_str = "unknown",
					message_str = "not supplied";
				if (error.get_type() == SERIALISATION::Value::t_map)
				{
					SERIALISATION::ValueMap::const_iterator
						code_itr = error.as_map().find("code"),
						message_itr = error.as_map().find("message");
				
					if (code_itr != error.as_map().end())
						code_str = code_itr->second.get_as_string();
				
					if (message_itr != error.as_map().end())
						message_str = message_itr->second.get_as_string();
						
					error_message_strm << std::endl << "- " << message_str <<
						"(code " << code_str << ")";
				}	// end if we have a proper error map
			}	// end errors loop
			
			RAISE_ERROR(error_message_strm.str());
		}	// end if we have multiple errors
	}	// end if the response is a map with an "errors" entry.
	
	return v;
}	// end process_response

void Twitter::raise_twitcurl_error(twitCurl& twitter)
{
	std::string error;
	twitter.getLastCurlError(error);
	if (error.empty()) error = "unknown error";
	
	RAISE_ERROR("The Twitter library signalled the follwing error\"" <<
		error << "\"");
}	// end process_twitcurl_error

time_t Twitter::to_time(const std::string& dts)
{
	// RegExp for parsing Twitter date.
	QRegExp twitter_datetime_parser(
		"([A-Za-z]{3})\\s([A-Za-z]{3})\\s(\\d{1,2})\\s(\\d{2})\\:(\\d{2})\\:"
		"(\\d{2})\\s([\\+|\\-]\\d{4})\\s(\\d{4})");
		
	// Attempt to parse the string.
	if (twitter_datetime_parser.indexIn(dts.c_str()) != 0)
		RAISE_ERROR("The string \"" << dts << "\" could not be parsed into "
			"a proper date and time.");
			
	QStringList elements = twitter_datetime_parser.capturedTexts();
			
	// Get out the month string, and turn it into a month number.
	int month_n = -1;
	if (elements[2] == "Jan") month_n = 1;
	else if (elements[2] == "Feb") month_n = 2; 
	else if (elements[2] == "Mar") month_n = 3; 
	else if (elements[2] == "Apr") month_n = 4; 
	else if (elements[2] == "May") month_n = 5; 
	else if (elements[2] == "Jun") month_n = 6; 
	else if (elements[2] == "Jul") month_n = 7; 
	else if (elements[2] == "Aug") month_n = 8; 
	else if (elements[2] == "Sep") month_n = 9; 
	else if (elements[2] == "Oct") month_n = 10; 
	else if (elements[2] == "Nov") month_n = 11; 
	else if (elements[2] == "Dec") month_n = 12;
	else RAISE_ERROR("The abbreviate month name \"" <<
		elements[2].toStdString() << "\" is not recognised."); 
		
	// Now we can construct our QDateTime.
	QDateTime dtg(
		QDate(
			elements[8].toInt(),		// The year
			month_n,					// The month of the year
			elements[3].toInt()),		// The day of the month
		QTime(
			elements[4].toInt(),		// The hour of the day
			elements[5].toInt(),		// The minute of the hour
			elements[6].toInt()			// The second of the minute
		),
		Qt::OffsetFromUTC,				// Converting to UTC
		elements[7].toInt() * 3600		// The offset from UTC
	);
	
	// Make sure everything is a valid date.
	if (dtg.isValid() == false)
		RAISE_ERROR("The string \"" << dts << "\" could not be transformed "
			"into a valid date and time.");
	
	// Return as time_t
	return dtg.toTime_t();
}	// end to_time
