/**
 * \file DataFile.h Implements the DataFile class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include <utilities/Log.h>
#include <utilities/Serialisation.h>

#include "DataFile.h"
#include "Error.h"
#include "twitter/Twitter.h"

DataFile::DataFile(void) :
    m_mutex(),
    m_filename(),
    m_is_dirty(false),
    // m_people(),
    m_accounts(),
    m_timelines()
{

}   // end constructor

DataFile::~DataFile(void)
{
    clear_accounts();
}   // end destructor

void DataFile::clear_accounts(void)
{
    GUARD(m_mutex);
    
    // Destroy our Account objects, and clear the container.
    for (auto account_itr : m_accounts) delete account_itr.second;
    m_accounts.clear();    
}   // end clear_accounts

AccountIdentifiers& DataFile::get_account_ids(
        AccountIdentifiers& ids) const
{
    GUARD(m_mutex);

    // Grab all the IDs from the accounts.
    for (auto account_itr : m_accounts) ids.insert(account_itr.first);
    return ids; 
}   // end get_account_ids

void DataFile::add(Account* account)
{
    GUARD(m_mutex);
    
    // Check that we don't already have an account with this ID.
    AccountIdentifier id(
        account->get_service_name(),
        account->get_id());
        
    if (m_accounts.find(id) != m_accounts.end())
        RAISE_ERROR("The data file already has a " << id.first << " account "
            "with id \"" << id.second << "\".");
            
    m_accounts.insert(std::make_pair(id, account));
}   // end add method

void DataFile::erase_accounts(const AccountIdentifiers& ids)
{
    GUARD(m_mutex);

    for (auto id : ids)
    {
        Accounts::iterator account_itr = m_accounts.find(id);
        if (account_itr == m_accounts.end())
        {
            LOG_WARNING("An attempt was made to erase the account with "
                "ID \"" << id.second << "\" (service \"" << id.first <<
                "\" but this account was not found in the accounts "
                "collection.");
        }
        else
        {
            // Delete the account and its entry in the collection.
            delete account_itr->second;
            m_accounts.erase(account_itr);
        }
    }    
}   // end erase_accounts

AccountSharedPtr DataFile::get_account_copy(
        const AccountIdentifier& account_id) const
{
    GUARD(m_mutex);
    
    // Do we have an account with this ID?
    Accounts::const_iterator account_itr =
        m_accounts.find(account_id);
    if (account_itr == m_accounts.end())
        RAISE_ERROR("An account with identifier \"" << account_id.second <<
            ":" << account_id.first << "\" does not exist.");
            
    // Retrieve a clone of the account.
    return AccountSharedPtr(account_itr->second->clone());
}   // end get_account_copy method

AccountSharedPtrList& DataFile::get_account_copies(
        const AccountIdentifiers& account_ids,
        AccountSharedPtrList& accounts) const
{
    GUARD(m_mutex);
    
    for (auto account_id : account_ids)
    {
        Accounts::const_iterator account_itr = m_accounts.find(account_id);
        if (account_itr == m_accounts.end())
            RAISE_ERROR("An account with identifier \"" <<
                account_id.second << ":" << account_id.first << "\" does "
                "not exist.");
                
        accounts.push_back(AccountSharedPtr(account_itr->second->clone()));
    }   // end account_id loop
    
    return accounts;
}   // end get_account_copies

Timelines DataFile::get_timelines(void) const
{
    GUARD(m_mutex);
    
    LOG_DEBUG(__FUNCTION__ << " - returning " << m_timelines.size() <<
        " timeline(s).")
    
    return m_timelines;
}   // end get_timelines method
    
void DataFile::set_timelines(const Timelines& timelines)
{
    GUARD(m_mutex);
    m_timelines = timelines;
}   // end set_timelines method

void DataFile::save_to(const std::string& filename) const
{
    // Open the file
    std::ofstream file(filename.c_str());
    if (!file)
        RAISE_ERROR("Could not open file \"" << filename <<
            "\" for writing.");
            
    // This is the main Value Map for the SocOut data file.
    SERIALISATION::ValueMap socout_vm;            
            
    // Lock the mutex from here on.
    GUARD(m_mutex);
           
    /* 
    // Serialise the people collection.
    SERIALISATION::ValueVector people_vv;
    m_people.copy_to(people_vv);
    
    // Put the people collection in the main socout data structure.
    //
    // TODO People are not supported in SocOut at the moment.
    socout_vm["people"] = SERIALISATION::Value(people_vv);
    */
    
    // Serialise the Accounts collection.
    SERIALISATION::ValueVector accounts_vv;
    for (auto account_itr : m_accounts)
    {
        // Create a ValueMap for the Account object.
        SERIALISATION::ValueMap account_vm;
        account_itr.second->copy_to(account_vm);
        accounts_vv.push_back(SERIALISATION::Value(account_vm));
    }
    
    // Put the accounts vv into the main SocOut value map.
    socout_vm["accounts"] = SERIALISATION::Value(accounts_vv);
    
    // Serialise the timelines collection, if we have timelines.
    if (m_timelines.empty() == false)
    {
        SERIALISATION::ValueVector timelines_vv;
        for (auto timeline_itr : m_timelines)
        {
            SERIALISATION::ValueMap timeline_vm;
            timeline_itr.second.copy_to(timeline_vm);
            timelines_vv.push_back(SERIALISATION::Value(timeline_vm));
        }
    
        socout_vm["timelines"] = SERIALISATION::Value(timelines_vv);
    }   // end if we have some timelines
    
    // Now we can write the socout value map to the file.
    SERIALISATION::YAMLSerialiser ser;
    ser.write(SERIALISATION::Value(socout_vm), file);
    
    m_filename = filename;
    m_is_dirty = false;
}   // end save_to method
    
void DataFile::read_from(
        const std::string& filename,
        ProgressMonitor& monitor)
{
    // Open the file for reading.
    std::ifstream file(filename.c_str());
    if (!file)
        RAISE_ERROR("Could not open file \"" << filename <<
            "\" for reading.");
            
    // Lock the mutex from here on.
    GUARD(m_mutex);

    // Serialise from the file.
    SERIALISATION::Value socout_v;
    SERIALISATION::YAMLSerialiser ser;
    ser.read(file, socout_v);
    
    // Make sure we have a value map.
    if (socout_v.get_type() != SERIALISATION::Value::t_map)
        RAISE_ERROR("The SocOut data read from file \"" << filename <<
            "\" is not a value map.");
            
    // Look for the people element. If it is not there, it simply means that
    // we have no people in our data file (it's not an error).
    /*
    SERIALISATION::ValueMap::const_iterator people_itr =
        socout_v.as_map().find("people");
    if (people_itr == socout_v.as_map().end())
    {
        LOG_INFO("SocOut file \"" << filename << "\" has no people "
            "records.");   
    }
    else
    {
        // Make sure the people item is a vector. If it isn't it probably
        // means that the entry is empty.
        if (people_itr->second.get_type() != SERIALISATION::Value::t_vector)
        {
            LOG_INFO("SocOut file \"" << filename << "\" has a \"people\" "
                "entry that is either an empty list, or not a list at all.");
        }
        else m_people.copy_from(people_itr->second.as_vector());
    }   // end if we have a people element
    */
    
    // Look for the accounts element. If it isn't present, we have no
    // accounts yet.
    SERIALISATION::ValueMap::const_iterator accounts_itr =
        socout_v.as_map().find("accounts");
    if (accounts_itr == socout_v.as_map().end())
    {
        LOG_INFO("SocOut file \"" << filename << "\" has no account "
            "records.");
    }
    else
    {
        // Make sure the accounts item is a vector. If it isn't, it's
        // probably an empty list.
        if (accounts_itr->second.get_type() !=
                SERIALISATION::Value::t_vector)
        {
            LOG_INFO("The SocOut data read from file \"" << filename <<
                "\" has an \"accounts\" element that is either an empty "
                "list, or not a list at all.");
        }
        else
        { 
            // Loop through the account info, and recreate it using the relevant
            // Services.
            //
            // TODO This can take a while, and locks up the UI while it is doing
            // so. We need to put it in a background thread.
            //
            // Variables for measuring progress through the process.
            int number_of_accounts = static_cast<int>(
                accounts_itr->second.as_vector().size());
            int account_index = 0;
            
            // Call the progress monitor with initial values, and check to
            // see if we need to stop (before we've actually started).
            if (monitor.report_progress(0) == false)
            {
    
                // Loop though the accounts.
                for (auto account_v : accounts_itr->second.as_vector())
                {
                    // Make sure that the account is a map.
                    if (account_v.get_type() != SERIALISATION::Value::t_map)
                        RAISE_ERROR("The SocOut Accounts data read from "
                            "file \"" << filename << "\" has an entry that "
                            "is not properly structured with 'name:value' "
                            "pairs.");
                            
                    // Look for the service item to find out what sort of
                    // Service the account is for.
                    SERIALISATION::ValueMap::const_iterator
                        service_name_itr = account_v.as_map().find(
                            "service-name");
                    if (service_name_itr == account_v.as_map().end())
                        RAISE_ERROR("The SocOut Accounts data read from "
                            "file \"" << filename << "\" has an entry that "
                            "does not include a \"service-name\" "
                            "attribute.");
                            
                    // Which service is it?
                    if (service_name_itr->second.get_as_string() ==
                            "Twitter")
                    {
                        // Recreate the account object from Twitter.
                        Account* account =
                            Twitter::get_twitter().recreate_account(
                                account_v.as_map());
                        
                        // Create the ID for the Account.
                        AccountIdentifier id(
                            account->get_service_name(),
                            account->get_id());
                            
                        m_accounts.insert(std::make_pair(id, account));
                    }   // end if this is a Twitter account    
                    // TODO: More services go here 
                    else RAISE_ERROR("The SocOut Accounts data read from "
                        "file \"" << filename << "\" has an Account entry "
                        "for an unrecognised Service called \"" <<
                        service_name_itr->second.get_as_string() << "\".");
                        
                    // Work out our progress, and report it, checking for the
                    // halt signal.
                    account_index++;
                    int progress_percentage = static_cast<int>(
                        static_cast<double>(account_index) * 100.0 /
                        static_cast<double>(number_of_accounts));
                        
                    // If the progress report returns `true`, then we need
                    // to abort the process.
                    if (monitor.report_progress(progress_percentage))
                    {
                        // If we haven't completed our task, report that to
                        // the monitor.
                        if (account_index < number_of_accounts)
                            monitor.signal_task_halted_early();
                        
                        break;
                    }   // end if we need to halt early
                }   // end account loop
                
            }   // end if the user has not selected 'stop' for the long
                // accounts validation process before it has begun.
            else monitor.signal_task_halted_early();
        }   // end if the accounts item is a list
    }   // end if the accounts item is a vector value
    
    // Timelines. They may or may not be present.
    SERIALISATION::ValueMap::const_iterator timelines_itr =
        socout_v.as_map().find("timelines");
    if (timelines_itr == socout_v.as_map().end())
    {
        LOG_INFO("SocOut file \"" << filename << "\" has no timeline "
            "records.");
    }
    else
    {
        LOG_DEBUG(__FUNCTION__ << " - SocOut file \"" << filename <<
            "\" has a \"timelines\" entry.");
        
        // Make sure that the timelines record is a vector.
        if (timelines_itr->second.get_type() ==
                SERIALISATION::Value::t_vector)
        {
            const SERIALISATION::ValueVector& timelines_vv =
                timelines_itr->second.as_vector();
                
            for (auto timeline_v : timelines_vv)
            {
                if (timeline_v.get_type() != SERIALISATION::Value::t_map)
                    RAISE_ERROR("SocOut data file \"" << filename <<
                        "\" includes a Timeline that is not a properly "
                        "formed name/value sequence.");
                        
                Timeline timeline;
                timeline.copy_from(timeline_v.as_map());
                
                m_timelines.insert(
                    std::make_pair(timeline.get_name(), timeline));
                    
                LOG_DEBUG(__FUNCTION__ << " - retrieved a Timeline object");
            }   // end timelines loop
        }   // end if the timelines entry is a proper list
        else LOG_INFO("The \"timelines\" entry of SocOut data file \"" <<
            filename << "\" is either an empty list, or not a list at all.");
    }   // end if the data file has a timelines entry
    
    LOG_DEBUG(__FUNCTION__ << " - data file now has " <<
        m_timelines.size() << " timeline(s).");
    
    // We're done reading the data. Remember the filename, and mark the
    // document as clean.
    m_filename = filename;
    m_is_dirty = false;
}   // end read_from method

void DataFile::clear_to_new(void)
{
    // We need to lock the mutex for these actions.
    {
        GUARD(m_mutex);
        m_filename.clear();
        m_is_dirty = false;
        m_timelines.clear();
    }
    
    // These methods implement their own thread-safety measures.
    // m_people.clear();
    clear_accounts();
}   // end clear_to_new

DataFile::DataFile(const DataFile& rhs) :
    m_mutex(),
    m_filename(),
    m_is_dirty(false),
    // m_people(),
    m_timelines()
{
    // Lock the mutex of the incoming object so that we can copy it
    // consistently. We don't need to lock our own mutex, because we are
    // just being constructed at the moment - no other threads will be
    // bugging us right now.
    GUARD(rhs.m_mutex);
    m_filename = rhs.m_filename;
    m_is_dirty = rhs.m_is_dirty;
    // m_people = rhs.m_people;
    m_timelines = rhs.m_timelines;
    
    // Clone the accounts.
    for (auto account_itr : rhs.m_accounts)
        m_accounts.insert(
            std::make_pair(
                account_itr.first,
                account_itr.second->clone()));
}   // end copy constructor
    
DataFile& DataFile::operator=(const DataFile& rhs)
{
    // Lock the mutexes for a consistent copy. Note thew use of an inner
    // block scope - this is because the GUARD macro creates a mutex Guard
    // object each time, and we need to avoid a name conflict.
    GUARD(m_mutex);
    
    {
        GUARD(rhs.m_mutex);
        
        m_filename = rhs.m_filename;
        m_is_dirty = rhs.m_is_dirty;
        // m_people = rhs.m_people;
        m_timelines = rhs.m_timelines;
    
        // Clone the accounts.
        for (auto account_itr : rhs.m_accounts)
            m_accounts.insert(
                std::make_pair(
                    account_itr.first,
                    account_itr.second->clone()));
    }   // end mutex guard block
        
    return *this;
}   // end assignment operator
