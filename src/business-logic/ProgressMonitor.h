/**
 * \file ProgressMonitor.h Declares the ProgressMonitor class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef _ProgressMonitor_h_included
#define _ProgressMonitor_h_included

/**
 * \brief An interface for monitoring - and possibly interrupting - long
 * tasks
 *
 * References to objects of classes derived from this class are passed to
 * various methods that may take a long time to complete. These objects
 * implement that methods of this interface so that they can receive (and,
 * for example, display) notifications about the progress of the task. The
 * progress notification method can also return a boolean flag indicating
 * that the process should stop, if possible.
 */
class ProgressMonitor
{
	public:
	
	/**
	 * \brief Trivial destructor
	 */
	virtual ~ProgressMonitor(void) {}
	
	/**
	 * \brief Report on the progress of a task
	 *
	 * This callback is triggered at various point throughout the lengthy
	 * process to indicate progress through the task. Note that this *may* be
	 * called from a different thread than that in which the 
	 *
	 * \param progress An integer between 0 and 100, indicating percentage
	 * progress through the task.
	 *
	 * \return True if the process should halt; false otherwise
	 */
	virtual bool report_progress(int progress) = 0;
	
	/**
	 * \brief Signal that the monitored task was not completed due to
	 * cancellation
	 *
	 * The monitored task will call this method if it is stopping prior to
	 * completion due to the `report_progress` method returning `true`. Note
	 * that if that method returned `true` to halt the process, but the task
	 * completed anyway, then this method will not be called. Likewise, if
	 * the task halted early, but not because the method returned `true`,
	 * then this method is not called either (but the task method would have
	 * thrown an exception in this case).
	 */
	virtual void signal_task_halted_early(void) = 0;
};	// end ProgressMonitor class

#endif
