/**
 * \file Configuration.cpp Implements the Configuration class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include <QStandardPaths>
#include <QDir>

#include "Configuration.h"
#include "Error.h"

Configuration& Configuration::get_configuration(void)
{
    static Configuration c;
    return c;
}   // end get_configuration method

Log::level Configuration::get_console_log_level(void) const
{
    GUARD(m_mutex);

    std::string level_str =
            get_option_value("console-log-level").get_as_string();
    if (level_str == "NONE") return Log::level_none;
    else if (level_str == "ERROR") return Log::level_error;
    else if (level_str == "WARNING") return Log::level_warning;
    else if (level_str == "INFO") return Log::level_info;
    else if (level_str == "DEBUG") return Log::level_debug;
    else RAISE_ERROR("Configuration includes unrecognised console log level "
                     "of \"" << level_str << "\"");
}   // end get_console_log_level method

void Configuration::set_console_log_level(Log::level l)
{
    GUARD(m_mutex);

    switch (l)
    {
    case Log::level_none:
    default:
        set_option_value(
                    "console-log-level",
                    SERIALISATION::Value("NONE"),
                    true);
        break;

    case Log::level_error:
        set_option_value(
                    "console-log-level",
                    SERIALISATION::Value("ERROR"),
                    true);
        break;

    case Log::level_warning:
        set_option_value(
                    "console-log-level",
                    SERIALISATION::Value("WARNING"),
                    true);
        break;

    case Log::level_info:
        set_option_value(
                    "console-log-level",
                    SERIALISATION::Value("INFO"),
                    true);
        break;

    case Log::level_debug:
        set_option_value(
                    "console-log-level",
                    SERIALISATION::Value("DEBUG"),
                    true);
    }
}   // end set_console_log_level method

void Configuration::set_file_log_filename(const std::string filename)
{
    GUARD(m_mutex);

    set_option_value(
                "file-log-filename",
                SERIALISATION::Value(filename),
                true);
}   // end set_file_log_filename method

Log::level Configuration::get_file_log_level(void) const
{
    GUARD(m_mutex);

    std::string level_str =
            get_option_value("file-log-level").get_as_string();
    return Log::to_level(level_str);
}   // end get_file_log_level method

void Configuration::set_file_log_level(Log::level l)
{
    GUARD(m_mutex);

    switch (l)
    {
    case Log::level_none:
    default:
        set_option_value(
                    "file-log-level",
                    SERIALISATION::Value("NONE"),
                    true);
        break;

    case Log::level_error:
        set_option_value(
                    "file-log-level",
                    SERIALISATION::Value("ERROR"),
                    true);
        break;

    case Log::level_warning:
        set_option_value(
                    "file-log-level",
                    SERIALISATION::Value("WARNING"),
                    true);
        break;

    case Log::level_info:
        set_option_value(
                    "file-log-level",
                    SERIALISATION::Value("INFO"),
                    true);
        break;

    case Log::level_debug:
        set_option_value(
                    "file-log-level",
                    SERIALISATION::Value("DEBUG"),
                    true);
    }
}   // end set_file_log_level method

bool Configuration::get_verify_accounts_on_load(void) const
{
    GUARD(m_mutex);
    
    return get_option_value("verify-accounts-on-load").get_as_bool();
}   // end get_verify_accounts_on_load method
    
void Configuration::set_verify_accounts_on_load(bool f)
{
    GUARD(m_mutex);

    set_option_value(
                "verify-accounts-on-load",
                SERIALISATION::Value(f),
                true);
}   // end set_verify_accounts_on_load method

Configuration::window_state Configuration::get_main_window_state(void) const
{
    GUARD(m_mutex);

    return to_window_state(
        get_option_value("main-window-state").get_as_string());
}   // end get_main_window_state method
        
void Configuration::set_main_window_state(window_state mwss)
{
    GUARD(m_mutex);
    set_option_value(
        "main-window-state",
        SERIALISATION::Value(to_string(mwss)),
        true);    
}   // end set_main_window_state method
    
std::string Configuration::to_string(window_state ws)
{
    switch (ws)
    {
    case ws_normal: return "norm";
    case ws_max: return "max";
    case ws_min: return "min";
    default: RAISE_ERROR("Unrecognised window state code " <<
        static_cast<int>(ws) << ".");
    }
}   // end to_string method
    
Configuration::window_state Configuration::to_window_state(
        const std::string& s)
{
    if (s == "norm") return ws_normal;
    else if (s == "max") return ws_max;
    else if (s == "min") return ws_min;
    else RAISE_ERROR("Unrecognised window state code \"" << s << "\".");
}   // end to_window_state method

void Configuration::clear_to_defaults(void)
{
    GUARD(m_mutex);

    m_config_file_items.clear();
    m_options.clear();

    // Set up our options with their defaults.
    std::string config_dir = QStandardPaths::writableLocation(
        QStandardPaths::ConfigLocation).toStdString();
    m_options.insert(
        std::make_pair(
            "config-filename",
            OptionInfo({
                SERIALISATION::Value(config_dir + "\\SocOut-config.yml"),
                SERIALISATION::Value(
                    config_dir + "\\SocOut-config.yml")})));

    m_options.insert(
        std::make_pair(
            "console-log-level",
                OptionInfo({
                    SERIALISATION::Value("INFO"),
                    SERIALISATION::Value("INFO")})));

    std::string log_dir = QStandardPaths::writableLocation(
        QStandardPaths::DataLocation).toStdString();
    m_options.insert(
        std::make_pair(
            "file-log-filename",
                OptionInfo({
                    SERIALISATION::Value(log_dir + "\\SocOut-log.txt"),
                    SERIALISATION::Value(log_dir + "\\SocOut-log.txt")})));

    m_options.insert(
        std::make_pair(
            "file-log-level",
                OptionInfo({
                    SERIALISATION::Value("NONE"),
                    SERIALISATION::Value("NONE")})));
                    
    m_options.insert(
        std::make_pair(
            "verify-accounts-on-load",
            OptionInfo({
                SERIALISATION::Value(false),
                SERIALISATION::Value(false)})));
                
    m_options.insert(
        std::make_pair(
            "main-window-state",
            OptionInfo({
                SERIALISATION::Value(to_string(ws_normal)),
                SERIALISATION::Value(to_string(ws_normal))})));
}   // end clear_to_defaults method

void Configuration::initialise(int argc, char* argv[])
{
    // Clear our existing data. Note that this operation locks the internal
    // mutex.
    clear_to_defaults();
    
    // Lock our internal mutex for the rest of the block.
    GUARD(m_mutex);
    
    bool config_file_explicitly_specified = false;

    // Do a first pass of the command-line, just looking for the
    // "config-filename" argument
    for (int i = 1; i < argc; i++)
    {
        std::string argument = argv[i];
        if (argument == "--config-filename")
        {
            // Make sure there is one more argument for the filename itself.
            if (i > argc-2)
                RAISE_ERROR("The \"--config-filename\" command-line "
                            "argument requires an additional filename "
                            "parameter.");

            set_option_value(
                        "config-filename",
                        SERIALISATION::Value(argv[++i]),
                        false);
                        
            // Signal that a config file was specified explicitly.
            config_file_explicitly_specified = true;                        
                        
            break;
        }   // end if this is the config filename
    }   // end argument loop

    // Now read and interpret the config file, if we can. Note that if we
    // fail to open the config file, that is not an error - we simply use
    // defaults or command-line options. However, if the config filename has
    // been explicitly set, AND it cannot be opened, then we throw an
    // exception.
    std::ifstream config_file(get_config_filename().c_str());
    if (config_file)
    {
        // Read the data from the file.
        SERIALISATION::YAMLSerialiser ser;
        SERIALISATION::Value config_data;
        ser.read(config_file, config_data);
        
        // We may not get a map back at all. This is OK - it just means that
        // we don't have any configuration items.
        if (config_data.get_type() == SERIALISATION::Value::t_map)
        {
            // Get our config items out by name. This means that items in the
            // config file that we don't use will be ignored, but they will
            // also be lost when the file is written again.
            if (config_data.as_map().find("console-log-level") !=
                    config_data.as_map().end())
                set_option_value(
                    "console-log-level",
                    config_data.as_map()["console-log-level"], true);
                
            if (config_data.as_map().find("file-log-filename") !=
                    config_data.as_map().end())
                set_option_value(
                    "file-log-filename",
                    config_data.as_map()["file-log-filename"], true);
                    
            if (config_data.as_map().find("file-log-level") !=
                    config_data.as_map().end())
                set_option_value(
                    "file-log-level",
                    config_data.as_map()["file-log-level"], true);
                    
            if (config_data.as_map().find("verify-accounts-on-load") !=
                    config_data.as_map().end())
                set_option_value(
                    "verify-accounts-on-load",
                    config_data.as_map()["verify-accounts-on-load"], true);
                    
            if (config_data.as_map().find("main-window-state") !=
                    config_data.as_map().end())
                set_option_value(
                    "main-window-state",
                    config_data.as_map()["main-window-state"], true);
        }   // end if this a map value
    }   // end if we got our config file.
    else if (config_file_explicitly_specified)
        RAISE_ERROR("Config file \"" << get_config_filename() << "\" was "
            "explicitly specified on the command-line, but could not be "
            "opened.");

    // Read the command-line again, skipping the "config-filename" parameter
    // that we read before.
    for (int i = 1; i < argc; i++)
    {
        std::string argument = argv[i];

        // Loop through our option list, to see if we have a match.
        bool arg_match = false;
        for (auto& option : m_options)
        {
            // Does the argument match this option (not forgetting to add
            // the leading hyphens)?
            if (argument == ("--" + option.first))
            {
                // Make sure that we have an additional argument.
                if (i > argc-2)
                    RAISE_ERROR("The \"" << argument << "\" option "
                                "requires an additional argument");

                // Advance to the next argument no matter what - it's the
                // parameter for whatever we're setting.
                i++;
                
                // Only grab the option if it is NOT the config filename,
                // because we already dealt with that one in the first pass.
                if (argument != "--config-filename")
                    option.second.value = SERIALISATION::Value(argv[i]);

                arg_match = true;

                // Stop trying to match this argument.
                break;
            }   // end if the argument matches this option (and is not
                // "--config-filename")

        }   // end option loop
        if (!arg_match)
        {
            // The argument was not matched - we assume it is the filename.
            m_data_file_filename = argument;
        }   // end if the argument is not matched
    }   // end second argument loop
}   // end initialise method

void Configuration::write_config_file(void) const
{
    // Open our config file for writing. Note that `get_config_filename` logs
    // the internal mutex.
    std::ofstream file(get_config_filename().c_str());
    if (!file)
        RAISE_ERROR("Could not open configuration file \"" <<
            get_config_filename() << "\" for writing.");
            
    // Lock the mutex and serialise our config file items.
    GUARD(m_mutex);
    SERIALISATION::YAMLSerialiser ser;
    
    SERIALISATION::Value v(m_config_file_items);
    
    ser.write(v, file);
}   // end write_config_file method

Configuration::Configuration(void) :
    m_mutex(),
    m_options(),
    m_config_file_items(),
    m_data_file_filename()
{
    clear_to_defaults();
}   // end constructor

Configuration::~Configuration(void)
{
    // TODO make sure relevant config is written to the file properly, and
    // that everything is shut down OK
}   // end destructor

const SERIALISATION::Value& Configuration::get_option_value(
        const std::string& name) const
{
    OptionMap::const_iterator option_itr = m_options.find(name);
    if (option_itr == m_options.end())
        RAISE_ERROR("An attempt was made to retrieve the value of non-"
                    "existent configuration option \"" << name << "\".");
                    
    return option_itr->second.value;
}   // end get_option_value method

void Configuration::set_option_value(
        const std::string& name,
        const SERIALISATION::Value& value,
        bool write_to_config)
{
    OptionMap::iterator option_itr = m_options.find(name);
    if (option_itr == m_options.end())
        RAISE_ERROR("An attempt was made to set the value of non-existent "
                    "configuration option \"" << name << "\".");
                    
    option_itr->second.value = value;

    if (write_to_config && (name != "config-filename"))
        m_config_file_items[name] = value;
}   // end set_option_value method
