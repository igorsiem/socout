/**
 * \file Declares the DataFile class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMutex>
#include <utilities/MutexGuard.h>

#include "Person.h"
#include "Account.h"
#include "ProgressMonitor.h"
#include "Timeline.h"

#ifndef _DataFile_h_included
#define _DataFile_h_included

/**
 * \brief A top-level class containing all the user's data in a single file
 *
 * This method is intented to be used over multiple threads, and includes a
 * mutex for its internals.
 *
 * \todo Support for `Person` and `People` objects has been suspended for the
 * moment. This will be re-enabled in future versions.
 */
class DataFile final
{
    // --- External Interface ---

public:

    /**
     * \brief Default constructor - just creates a new data file, with no
     * data
     */
    DataFile(void);

    /**
     * \brief Destructor - shuts everything down
     *
     * Note that if this object is destroyed without saving (i.e. m_is_dirty
     * is true), a warning will be logged, but no error is signalled.
     */
    ~DataFile(void);

    // -- Accessors --

    /**
     * \brief Retrieve the filename of the data file (if it has been saved or
     * opened)
     *
     * The string is blank if no filename is set. The `save_to` and
     * `read_from` methods set this attribute.
     *
     * \return The filename
     */
    const std::string& get_filename(void) const
        { GUARD(m_mutex); return m_filename; }

    /**
     * \brief Signal whether or not the file has been saved since the last
     * edit
     *
     * \return True if the file needs to be saved
     */
    bool is_dirty(void) { GUARD(m_mutex); return m_is_dirty; }

    /**
     * \brief Set the 'is dirty' flag, indicating whether the data has been
     * changed, and needs to be saved
     *
     * \param d The value of the dirty flag
     */
    void set_is_dirty(bool d = true) { GUARD(m_mutex); m_is_dirty = d; }

    /**
     * \brief Clear the 'is dirty' flag, meaning that the data file does
     * not need to be saved
     */
    void clear_is_dirty(void) { GUARD(m_mutex); m_is_dirty = false; }

    /**
     * \brief Retrieve a reference to the People collection
     *
     * Note that if the People collection is changed, the 'is dirty' flag
     * needs to be set manually (call `set_is_dirty`).
     *
     * \return The people collection; note that this object implements its
     * own thread-safety protocols
     */
    // People& get_people(void) { return m_people; }

    /**
     * \brief Retrieve a const reference to the People collection
     *
     * \return The people collection; note that this object implements its
     * own thread-safety protocols
     */
    // const People& get_people(void) const { return m_people; }
    
    // -- Accounts --
    
    /**
     * \brief Delete all the account objects held by the Data File
     */
    void clear_accounts(void);
    
    /**
     * \brief Retrieve the IDs of all the accounts in the system
     *
     * Account IDs are pairs comprised of the Service name and the ID of
     * the accound on the service.
     *
     * \param ids The container into which the IDs are placed; note that this
     * is not cleared prior to filling
     * 
     * \return A reference to `ids`
     */
    AccountIdentifiers& get_account_ids(AccountIdentifiers& ids) const;
    
    /**
     * \brief Add a new account to the accounts list
     *
     * \param account The account object; note that the DataFile
     * *takes ownership* of this object
     *
     * \throw An account with the same identifier already exists in the
     * collection; this should already have been checked; if this error is
     * thrown, the DataFile object *does not take ownership* of the account
     * object
     */
    void add(Account* account);
    
    /**
     * \brief Delete accounts
     *
     * Note that if an indicated account is NOT in the Accounts list, an
     * exception is *not* thrown, but a warning is logged.
     *
     * \param ids The identifiers of the accounts to delete
     */
    void erase_accounts(const AccountIdentifiers& ids);
    
    /**
     * \brief Retrieve a *clone* of an account with
     *
     * This method retrieves a *clone copy* of the identified account. The
     * copy is necessary to preserve thread-safety.
     *
     * \param account_id The ID of the account to copy
     *
     * \return A *cloned copy* of the account object (as a shared ptr)
     *
     * \throw Error An account with the given ID does not exist
     */
    AccountSharedPtr get_account_copy(const AccountIdentifier& account_id) const;
    
    /**
     * \brief Get copies of the indicated accounts
     *
     * This method retrieves *clone copies* of the identified account. The
     * copies are necessary to preserve thread-safety.
     *
     * \param account_ids The IDs of the accounts to retrieve
     *
     * \param accounts The container to receive the new account objects; this
     * is not emptied prior to filling
     *
     * \return A reference to `accounts`
     *
     * \throw Error One of the account IDs in the list does not refer to a
     * valid account object
     */
    AccountSharedPtrList& get_account_copies(
        const AccountIdentifiers& account_ids,
        AccountSharedPtrList& accounts) const;
        
    /**
     * \brief Retrieve the timelines collection
     *
     * Note that this is a copy of the collection, not a reference. This is
     * necessary for thread-safety, and shouldn't be a problem efficiency-
     * wise, because timelines because this is a relatively small data
     * structure.
     *
     * \return The collection of timelines
     */
    Timelines get_timelines(void) const;
    
    /**
     * \brief Set the timelines collection
     *
     * \param timelines The timelines collection to set
     */
    void set_timelines(const Timelines& timelines);
    
    // -- Serialisation --
    
    /**
     * \brief Save the data to a named file
     *
     * \param filename The filename to save to
     *
     * \throw Error An error occurred while writing to the file
     */
    void save_to(const std::string& filename) const;
    
    /**
     * \brief Read the data from the named file
     *
     * If the data file includes Account information, then this is validated
     *
     * \param filename The filename to read from
     *
     * \param monitor The ProgressMonitor object that is used to pass back
     * progress to the caller
     *
     * \throw Error An error occured while reading from the file
     */
    void read_from(const std::string& filename, ProgressMonitor& monitor);

    /**
     * \brief Clear the data for a new file
     */
    void clear_to_new(void);
    
    // -- Copy Semantics --
    
    /**
     * \brief Copy constructor
     *
     * Copy everything except the mutex.
     *
     * \param rhs The object from which to copy; note that the internal mutex
     * of this object is locked during the operation so that we get a
     * consistent copy in MT situations.
     */
    DataFile(const DataFile& rhs);
    
    /**
     * \brief Assignment operator
     *
     * Copy everything exception the mutex.
     *
     * \param rhs The object from which to copy; note that the internal mutex
     * of this object is locked during the operation so that we get a
     * consistent copy in MT situations.
     *
     * \return A reference to copied self.     
     */
    DataFile& operator=(const DataFile& rhs);

    // --- Internal Declarations ---

private:

    /**
     * \brief A mutex to protect the internals of the object
     *
     * Note that some members of this class (like `m_people` have their
     * own mutexes for MT access. This mutex is not used for those objects.
     */
    mutable QMutex m_mutex;

    /**
     * \brief The filename to which the data file has been saved
     *
     * This is blank if the file has not been saved or loaded from a file.
     * The `save_to` and `read_from` methods automatically record the
     * filename in this attribute.
     */
    mutable std::string m_filename;

    /**
     * \brief Whether or not the DataFile has been saved since it was last
     * changed
     */
    mutable bool m_is_dirty;

    /**
     * \brief Contacts with which the User communicates via social media
     *
     * Note that this object has its own mutex, so MT access is not
     * protected by the DataFile mutex.
     */
    // People m_people;
    
    /**
     * \brief Account objects, created by the various Services
     *
     * This is a polymorphic collection. These  objects are owned by the
     * DataFile object, and need to be deleted on destruction. Access is
     * protected by the QMutex 
     */
    Accounts m_accounts;
    
    /**
     * \brief The timelines collection
     */
    Timelines m_timelines;
};  // end DataFile class

#endif // DATAFILE_H
