/**
 * \file Configuration.h Declares the Configuration class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tuple>
#include <map>
#include <string>

#include <QMutex>

#include <utilities/Serialisation.h>
#include <utilities/Log.h>
#include <utilities/MutexGuard.h>

#ifndef _Configuration_h
#define _Configuration_h

/**
 * \brief A class for handling all the configuration items in SocOut
 *
 * Configuration information is obtained from both the command-line and an
 * optional config file. All configuration items are *optional*, and there is
 * *always* a 'sensible' default. Configuration items given on the command-
 * line override those given in a config file. This allows the 'main'
 * SocOut configuration to be set in a persistent file, but temporarily
 * overridden by a single command-line invocation.
 *
 * Configuration options are listed below. On the command line, they are
 * preceded by two hyphens (--). In the config file, they are key values in
 * a YAML map.
 *
 *      *   `config-filename` - The name of the configuration file to use;
 *          obviously, this may only be used from the command-line, and the
 *          default value is "SocOut-config.yml"
 *
 *      *   `console-log-level=<LEVEL>` - The level of log messages to send
 *          to the console; legitimate values are "NONE", "ERROR", "WARNING",
 *          "INFO" and "DEBUG"; the default is "INFO"
 *
 *      *   `file-log-filename=<FILENAME> - The name of the log file; this is
 *          only used if the `file-log-level` option is set to something
 *          other than "NONE", and the default is "SocOut-log.txt"
 *
 *      *   `file-log-level=<LEVEL>` - The level of log messages to send to
 *          a file log; the legitimate values are as with the
 *          `console-log-level` option, but the default is "NONE"
 *
 *      *   `verify-accounts-on-load=[true|false]` - Whether or not to
 *          verify that the accounts in a data file are valid when the file
 *          is loaded; the default is `false`
 *
 *      *   `main-window-state=[max|min|norm]` - The state of the main window
 *          window on the screen (maximised, minimised or normal)
 *
 *      *   The data file filename - this is a 'special' file that is not
 *          part of the standard config, and is simply read without any other
 *          argument from the command line 
 *
 * This configuration class is a singleton, and all options may be read and
 * written programmatically. Any option other than the `config-filename`
 * option may or may not be written to the config file, according to the
 * following rules:
 *
 *      1.  The command-line is read first to determine whether it includes
 *          a `config-filename` directive, but its options are applied
 *          *after* the config file is read (and so override the config
 *          file).
 *
 *      2.  Command-line options are never written to the config file.
 *
 *      3.  Options set programmatically (that is, during the running of
 *          the application) take precendence over everything, and are always
 *          written to the config file. This applies, for example, to a user
 *          editing SocOut configuration in a dialog.
 *
 * The state of all config options, is maintained in memory, along with a
 * version of the config file (as a serialisation object). Programmatic
 * config changes are written to the serialisation structure, which may then
 * be (re)written to the file at any time.
 *
 * All config class/object operations are fully thread-safe.
 *
 * \todo The config file path location makes use of Qt functionality to find
 * the standard config file location for the OS, and also to ensure that the
 * location path exists. This may need to be re-written to remove the Qt
 * dependency.
 */
class Configuration final
{
    // --- External Interface ---

public:

    /**
     * \ brief Retrieve the single instance of this class
     */
    static Configuration& get_configuration(void);

    // -- Config Options --
    
    /**
     * \brief Get the data file filename
     */
    std::string get_data_file_filename(void) const
        { GUARD(m_mutex); return m_data_file_filename; }

    /**
     * \brief Retrieve the value of the `config-filename` property
     *
     * Note that there is no corresponding 'set' method for this accessor,
     * because the config filename can *only* be set from the command-line
     *
     * \return The name of the config filename; note that the return-by-value
     * convention is intentional here
     */
    const std::string get_config_filename(void) const
        { return get_option_value("config-filename").get_as_string(); }

    /**
     * \brief Retrieve the value of the console logging level
     *
     * \return The console logging level
     *
     * \throw Error the configured console logging level is not one of the
     * legal values ("NONE", "ERROR", "WARNING", "INFO" or "DEBUG")
     */
    Log::level get_console_log_level(void) const;

    /**
     * \brief Set the value of the console logging level
     *
     * Note that calling this method will ensure that this option is
     * explicitly written to the config file at the next write.
     *
     * \param l The logging level for the console log
     */
    void set_console_log_level(Log::level l);

    /**
     * \brief Retrieve the filename of the SocOut log file
     *
     * Note that this is only used if the file log level is set to something
     * other than "NONE".
     *
     * \return The SocOut log filename (return-by-value is intentional)
     */
    std::string get_file_log_filename(void) const
        { return get_option_value("file-log-filename").get_as_string(); }
        
    /**
     * \brief Set the filename of the log file
     *
     * Note that this is only used if the file logging level is set to
     * something other than "NONE". Calling this method ensures that the
     * option is writte to the config file at the next write.
     *
     * \param filename The filename to set
     */
    void set_file_log_filename(const std::string filename);

    /**
     * \brief Retrieve the logging level for the file log
     *
     * \return The file logging level
     *
     * \throw Error The configured file logging level is not one of the
     * following: "NONE", "ERROR", "WARNING", "INFO" or "DEBUG"
     */
    Log::level get_file_log_level(void) const;

    /**
     * \brief Set the logging level for the file log
     *
     * Calling this method ensures that the value is written to the config
     * file at the next write.
     *
     * \param l The logging level enumerator
     */
    void set_file_log_level(Log::level l);
    
    /**
     * \brief Check whether account details should be verified when a data
     * file is loaded
     *
     * \return True if accounts should be checked
     */
    bool get_verify_accounts_on_load(void) const;
    
    /**
     * \brief Set a flag indicating whether account details should be
     * verified when a data file is loaded
     *
     * \param f Set to `true` if account details should be verified
     */
    void set_verify_accounts_on_load(bool f);
    
    /**
     * \brief Enumerates the display state of a window
     */
    enum window_state
    {
        ws_normal,          ///< Normal window
        ws_max,             ///< Maximised window
        ws_min              ///< Minimised window
    };  // end window_state enum
    
    /**
     * \brief Retrieve the state of the main window
     *
     * \return The state of the main window
     */
    window_state get_main_window_state(void) const;
        
    /**
     * \brief Set the state of the main window
     *
     * \param mws The state of the main window
     */
    void set_main_window_state(window_state mwss);
    
    /**
     * \brief Convert a window state enum to a string code (for use in a
     * config file)
     *
     * \param ws The window state code
     *
     * \return "norm", "min" or "max"
     *
     * \throw Error Unrecognised enum code
     */
    static std::string to_string(window_state ws);
    
    /**
     * \brief Convert a string to a window state code
     *
     * \param s The string to convert; this must be "norm", "min" or "max",
     * or an exception will be thrown
     *
     * \return A window state code
     *
     * \throw Error Unrecognised window state string
     */
    static window_state to_window_state(const std::string& s);
    
    // -- Initialisation and Shutdown --

    /**
     * \brief Clear all configuration options back to defaults
     */
    void clear_to_defaults(void);
    
    /**
     * \brief Initialise the configuration from the command-line and config
     * file
     *
     * This method parses the given command-line parameters, and also opens
     * and reads the config file (if it can). The parameters are set up as
     * outlined in class description.
     *
     * If no config file is specified on the command-line, then an attempt
     * is made to open the default config file. If this fails, no error is
     * signalled - we simply move on. However, if a config file is specified
     * on the command-line, and this fails to open, then an error is
     * signalled. This allows SocOut to be run with no intervention or
     * config input, purely on defaults, but still gives the capability to
     * debug config problems by explicitly specifying the filename.
     *
     * If a config file is opened successfully, however, and contains an
     * error, this will be signalled with an exception as well.     
     *
     * \param argc The number of command-line arguments (as passed to the
     * `main` function)
     *
     * \param argv The command-line arguments passed to the `main` function
     *
     * \throw Error There was a problem with the initialisation parameters
     */
    void initialise(int argc, char* argv[]);
    
    /**
     * \brief Write the config file
     *
     * This method writes the config file. All options that were previously
     * read from the config file, as well as options that have been set
     * explicitly by one of the `set_` accessors are written. Options that
     * are just the defaults, or that were taken from the command-line are
     * not written.
     *
     * The name of the config file is configured, either with the default, or
     * one that is set from the command-line.
     */
    void write_config_file(void) const;
    
    // --- Internal Declarations ---

private:

    // -- Internal Types --

    /**
     * \brief A struct containing information about a given option
     *
     * Objects of this class are held in a map, indexed by the option name
     */
    struct OptionInfo
    {
        /**
         * \brief The current value of the option
         */
        SERIALISATION::Value value;

        /**
         * \brief The default value of the option
         *
         * Note: Every option's value is set to the default explicitly in
         * the constructor, so there seems to be little reason to have a
         * 'default' attribute. However, this is used in the configuration
         * editing context, to allow the user to reset an item to its
         * default.
         */
        SERIALISATION::Value default_value;
    };  // end OptionInfo struct

    /**
     * \brief A map of option names to option info structs
     */
    typedef std::map<std::string, OptionInfo> OptionMap;

    // -- Internal Methods --

    /**
     * \brief Constructor - initialises everything to default values
     */
    Configuration(void);

    /**
     * \brief Destructor - writes the config file one last time, just to be
     * sure, and shuts everything down
     */
    ~Configuration(void);

    /**
     * \brief Retrieve the value of a named option
     *
     * \param name The name of the configuration option
     *
     * \throw Error There is no option with the given name
     */
    const SERIALISATION::Value& get_option_value(const std::string& name)
        const;

    /**
     * \brief Set the value of a named option
     *
     * \param name The name of the configuration option
     *
     * \param value The value of the configuration option
     *
     * \param write_to_config Whether or not to write the value of the
     * configuration option to the config file at the next write; note
     * that this setting is explicitly ignored for the "config-filename",
     * because that can only be set from the command-line
     *
     * \throw Error There is no option with the given name (this method
     * cannot create new option entries
     */
    void set_option_value(
            const std::string& name,
            const SERIALISATION::Value& value,
            bool write_to_config);

    // -- Attributes --

    /**
     * \brief The mutex that protects the internals of the configration object
     */
    mutable QMutex m_mutex;

    /**
     * \brief The set of options
     */
    OptionMap m_options;
    
    /**
     * \brief The items in the config file, as serialisation values
     *
     * Note that not all configuration items are found in this collection. In
     * particular the `config-filename` items is *never* part of the config
     * file collection.
     */
    SERIALISATION::ValueMap m_config_file_items;
    
    /**
     * \brief The name of the data file; this is a 'special' value that is
     * not read from or written to the config parameters
     */
    std::string m_data_file_filename;
};  // end Configuration class

#endif // CONFIGURATION_H
