/**
 * \file Service.h Declares the Service class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "Item.h"
#include "Account.h"
 
#ifndef _Service_h_included
#define _Service_h_included

/**
 * \brief Encapsulates a social media service that is supported by SocOut
 *
 * Service is a base class for 'plug-ins' that act as interfaces for social
 * media services (e.g. Twitter). Each class derived from this Service is a
 * singleton that will interact with the service on behalf of SocOut.
 *
 * Services are identified with a unique string name, and need to be
 * implemeted in a thread-safe manner.
 */
class Service
{
	// --- External Interface ---
	
	public:
	
	/**
	 * \brief Trivial constructor
	 */
	Service(void) {}
	
	/**
	 * \brief Trivial destructor
	 */
	virtual ~Service(void) {}
	
	// -- Accessors --
	
	/**
	 * \brief Retrieve the unique name of the service as a string
	 *
	 * \return The name of the service
	 */
	virtual std::string get_name(void) const = 0;
	
	/**
	 * \brief Retrieve the most recent items for a given Account
	 *
	 * \todo Implementations of this can be lengthy; it may pay to introduce
	 * an MT interface
	 *
	 * \param account The account object for which the items are to be
	 * returned; if the Service for this account is not the same as this
	 * Service
	 *
	 * \param items The container to receive the items; this is not cleared
	 * prior to filling
	 *
	 * \param max The maximum number of items to retrieve; there are lots of
	 * reasons why the full number may not be returned
	 * 
	 * \return A reference to `items`
	 *
	 * \throw Error A problem occurred while retrieving the items
	 */
	virtual Items& get_recent(
		::Account& account,
		Items& items,
		std::size_t max = 50) = 0;
		
	/**
	 * \brief Post an Item
	 *
	 * Note that each Service implementation may use or ignore any part of
	 * the Item object at its own discretion. In some circumstances, the Item
	 * object may be invalid for a given service.
	 *
	 * \param account The account to use to post the item on the service
	 * (an exception will be thrown if the account is not for this Service)
	 *
	 * \param item The item to post
	 *
	 * \throw Error There was a problem posting the item; either it was
	 * invalid for the service, there was a problem with the Account (e.g.
	 * invalid), or the Service itself has a problem
	 */
	virtual void post(::Account& account, const Item& item) = 0;
	
	// --- Internal Declarations ---
	
	protected:
	
};	// end Service clas

#endif
