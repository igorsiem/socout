/**
 * \file Timeline.h Declares the Timeline class
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <map>

#include "Account.h"

#ifndef _Timeline_h_included
#define _Timeline_h_included

/**
 * \brief A specification for selecting content items to display in a single
 * view
 *
 * Timelines are essentially sets of rules for including and excluding items.
 * These are displayed on a timeline view. Timeline 'recipes' are stored in a
 * DataFile.
 *
 * \todo Currently, the `Timeline` class only contains specifications for
 * including Accounts. We also want inclusions / exclusions for keywords /
 * hashtags, and - perhaps - time periods.
 */
class Timeline final
{
	// --- Public Interface ---
	
	public:
	
	/**
	 * \brief Trivial constructor
	 */
	Timeline(void) : m_name(), m_account_ids() {}
	
	// Default destructor and copy semantics are fine
	
	// -- Accessors --
	
	/**
	 * \brief Retrieve the timeline name
	 *
	 * \return The name of the timeline
	 */
	const std::string& get_name(void) const { return m_name; }
	
	/**
	 * \brief Set the timeline name
	 *
	 * \param name The name to set
	 */
	void set_name(const std::string& name) { m_name = name; }
	 	
	/**
	 * \brief Get the account IDs collection
	 *
	 * \return A reference to the account IDs collection
	 */
	const AccountIdentifiers& get_account_ids(void) const
		{ return m_account_ids; }
		
	/**
	 * \brief Get the account IDs collection
	 *
	 * \return A reference to the account IDs collection
	 */
	AccountIdentifiers& get_account_ids(void)
		{ return m_account_ids; }
		
	// -- Serialisation --
	
	/**
	 * \brief Encode the timeline parameters to a serialisation value
	 *
	 * \param vm The value map into which the timeline is copied; note that
	 * this is not emptied prior to filling
	 *
	 * return `vm` after filling
	 */
	SERIALISATION::ValueMap& copy_to(SERIALISATION::ValueMap& vm) const;
	
	/**
	 * \brief Decode the timeline parameters from a serialisation value
	 *
	 * Note that the account IDs that are retrieved are not validated against
	 * any list of accounts. This should be done externally, to ensure that we
	 * are not referencing a non-existent account.
	 *
	 * \param vm The serialisation value map containing the timeline
	 * parameters
	 */
	void copy_from(const SERIALISATION::ValueMap& vm);
		 
	// --- Internal Declarations ---
	
	private:
	
	/**
	 * \brief The name of the timeline
	 *
	 * This is a short, human-readable, descriptive name that is displayed
	 * to identify the timeline.
	 */
	std::string m_name;
	
	/**
	 * \brief The IDs of the accounts to be included in the timeline
	 */
	AccountIdentifiers m_account_ids;
};	// end Timeline class

/**
 * \brief A collection of timelines, indexed by (unique) name
 */
typedef std::map<std::string, Timeline> Timelines;

#endif
