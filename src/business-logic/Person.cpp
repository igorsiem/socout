/**
 * \file Person.cpp Implements the Person and People classes
 *
 * SocOut is copyright (C) 2015 Igor Siemienowicz
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Person.h"
#include "Error.h"

std::string Person::Name::get_firstlast(void) const
{
    std::string middle = " ";
    if (get_first_name().empty() || get_last_name().empty())
        middle.clear();

    return get_first_name() + middle + get_last_name();
}   // end get_firstlast

std::string Person::Name::get_lastfirst(void) const
{
    std::string middle = ", ";
    if (get_first_name().empty() || get_last_name().empty())
        middle.clear();

    return get_last_name() + middle + get_first_name();
}   // end get_lastfirst

SERIALISATION::ValueMap& Person::Name::copy_to(
        SERIALISATION::ValueMap& vm) const
{
    vm.clear();
    vm["first-name"] = SERIALISATION::Value(m_first_name);
    vm["last-name"] = SERIALISATION::Value(m_last_name);
    
    return vm;
}   // end copy_to method
        
void Person::Name::copy_from(
        const SERIALISATION::ValueMap& vm)
{
    SERIALISATION::ValueMap::const_iterator
        first_name_itr = vm.find("first-name"),
        last_name_itr = vm.find("last-name");
    
    if (first_name_itr == vm.end())
        RAISE_ERROR("Serialisation Value Map does not contain a "
            "\"first-name\" attribute when converting to a Name object.");
    if (last_name_itr == vm.end())
        RAISE_ERROR("Serialisation Value Map does not contain a "
            "\"last-name\" attribute when converting to a Name object.");
            
    m_first_name = first_name_itr->second.get_as_string();
    m_last_name = last_name_itr->second.get_as_string();
}   // end copy_from method 

Person::Person(
        const std::string& fn,
        const std::string& ln,
        const std::string& e) :
    m_name(fn, ln),
    m_email(e)
{

}   // end constructor

SERIALISATION::ValueMap& Person::copy_to(
        SERIALISATION::ValueMap& vm) const
{
    vm.clear();
    
    // Get our the name value map.
    SERIALISATION::ValueMap name_vm;
    m_name.copy_to(name_vm);
    
    vm["name"] = SERIALISATION::Value(name_vm);
    vm["e-mail"] = SERIALISATION::Value(m_email);
    
    return vm;
}   // end copy_to method
    
void Person::copy_from(
        const SERIALISATION::ValueMap& vm)
{
    SERIALISATION::ValueMap::const_iterator
        name_itr = vm.find("name"),
        email_itr = vm.find("e-mail");
        
    if (name_itr == vm.end())
        RAISE_ERROR("Serialisation Value Map does not contain a \"name\" "
            "attribute when converting to a Person object.");
    if (name_itr->second.get_type() != SERIALISATION::Value::t_map)
        RAISE_ERROR("Serialisation Value Map contains a \"name\" attribute "
            "that is not itself a value map, when converting to a "
            "Person object.");
    if (email_itr == vm.end())
        RAISE_ERROR("Serialisation Value Map does not contain an \"e-mail\" "
            "attribute when converting to a Person object.");
            
    m_name.copy_from(name_itr->second.as_map());
    m_email = email_itr->second.get_as_string();
}   // end copy_from method

void People::insert(const Person& p)
{
    GUARD(m_mutex);

    // Check to see if there is already a Person object with this e-mail
    // address in the container.
    if (m_people.find(p.get_email()) != m_people.end())
        RAISE_ERROR("The People collection already includes a Person with "
                    "the e-mail address \"" << p.get_email() << "\".");

    // Insert into the main collection and the index.
    m_people.insert(std::make_pair(p.get_email(), p));
    m_name_index.insert(std::make_pair(p.get_name(), p.get_email()));
}   // end insert method

std::set<std::string>& People::get_emails(
        std::set<std::string>& addresses) const
{
    GUARD(m_mutex);

    for (auto person_itr : m_people)
        addresses.insert(person_itr.first);

    return addresses;
}   // end get_emails

std::set<Person::Name>& People::get_names(
        std::set<Person::Name>& names) const
{
    GUARD(m_mutex);

    for (auto person_itr : m_people)
        names.insert(person_itr.second.get_name());
        
    return names;        
}

bool People::has_person_with_email(const std::string& e) const
{
    GUARD(m_mutex);
    return (m_people.find(e) != m_people.end());
}   // end has_person_with_email

Person People::get_person_with_email(const std::string& e) const
{
    GUARD(m_mutex);
    std::map<std::string, Person>::const_iterator i = m_people.find(e);
    if (i == m_people.end())
        RAISE_ERROR("The People collection has no Person with the "
                    "e-mail address \"" << e << "\".");

    return i->second;
}   // end get_person_with_email

void People::update(const Person& person)
{
    GUARD(m_mutex);

    // Make sure we have a Person object with this e-mail address.
    std::map<std::string, Person>::iterator i =
            m_people.find(person.get_email());
    if (i == m_people.end())
        RAISE_ERROR("A record with the e-mail address \"" <<
            person.get_email() << "\" does not exist.");

    // Locate the corresponding entry in the name index, delete it, and then
    // reinsert the new name. We do this by grabbing all the entries in the
    // index with the same name, and then looking for the e-mail address.
    //
    // Obviously, we are searching for the OLD name to erase.
    EmailsByNameRange r = m_name_index.equal_range(i->second.get_name());
    bool found_name_entry = false;
    for (EmailsByNameIterator j = r.first; j != r.second; j++)
    {
        // Check whether this is the same e-mail address.
        if (j->second == person.get_email())
        {
            m_name_index.erase(j);
            found_name_entry = true;
            break;
        }
    }   // end e-mails by name iterator loop.

    // Did we find our name index?
    if (!found_name_entry)
        RAISE_ERROR("The name index entry for \"" <<
                    i->second.get_name().get_firstlast() << "\" (e-mail "
                    "address \"" << i->second.get_email() << "\") could not "
                    "be found.");

    // Don't forget to re-add a new name index entry.
    m_name_index.insert(
                std::make_pair(person.get_name(), person.get_email()));

    // Now we can update the main Person entry.
    i->second = person;
}   // end update

std::set<std::string>& People::get_emails_by_name(
        const Person::Name& name,
        std::set<std::string>& addresses) const
{
    GUARD(m_mutex);
    
    // Get the range of people with this name in the name index.
    EmailsByNameConstRange r = m_name_index.equal_range(name);
    for (EmailsByNameConstIterator i = r.first; i != r.second; i++)
         addresses.insert(i->second);
         
    return addresses;
}   // end get_emails_by_name method

bool People::erase(const std::string& e)
{
    GUARD(m_mutex);

    std::map<std::string, Person>::iterator i = m_people.find(e);
    if (i == m_people.end()) return false;
    else
    {
        // We need to locate the corresponding entry from the name index,
        // which will have the matching e-mail address. We find all the
        // entries with the same name (which narrows down our search), and
        // then find the e-mail address itself.
        EmailsByNameRange r = m_name_index.equal_range(i->second.get_name());
        for (EmailsByNameIterator j = r.first; j != r.second; j++)
        {
            // If this index entry has the same e-mail address delete it, and
            // we're done.
            if (j->second == e)
            {
                m_name_index.erase(j);
                break;
            }
        }   // end EmailsByName iterator loop

        // Now we can delete our main entry.
        m_people.erase(i);

        return true;
    }   // end if we have a Person with this e-mail
}   // end erase method

SERIALISATION::ValueVector& People::copy_to(
        SERIALISATION::ValueVector& vv) const
{
    GUARD(m_mutex);
    
    vv.clear();
    for (auto person_itr : m_people)
    {
        SERIALISATION::ValueMap vm;
        person_itr.second.copy_to(vm);
        vv.push_back(SERIALISATION::Value(vm));
    }
    
    return vv;
}   // end copy_to method
    
void People::copy_from(const SERIALISATION::ValueVector& vv)
{
    clear();
    
    // Loop through the value vector. The elements must all be map values.
    //
    // Note that we don't need to explicitly lock the mutex, because the only
    // point at which it needs to be locked is when a Person object is
    // inserted, and the `insert` method already locks the mutex.
    for (auto v : vv)
    {
        if (v.get_type() != SERIALISATION::Value::t_map)
            RAISE_ERROR("A list of Serialisation Values for Person objects "
                "includes an element that is not properly structured with "
                "'name:value' pairs.");
                
        Person p("", "", "");
        p.copy_from(v.as_map());
        
        insert(p);
    }   // end vv element loop
}   // end copy_from method

People::People(const People& rhs) :
    m_mutex(),
    m_people(),
    m_name_index()
{
    // Lock the rhs mutex, so that we get a consistent copy. We don't need to
    // lock our own, because we're just being constructor right now.
    GUARD(rhs.m_mutex);
    m_people = rhs.m_people;
    m_name_index = rhs.m_name_index;
}   // end copy constructor
    
People& People::operator=(const People& rhs)
{
    // Lock the rhs mutex and our own so that we get a consistent copy in an
    // MT situation.
    GUARD(m_mutex);
    
    {
        GUARD(rhs.m_mutex);
    
        m_people = rhs.m_people;
        m_name_index = rhs.m_name_index;
    }
    
    return *this;
}   // end operator=
