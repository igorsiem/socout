# Script for running packaging tasks

directory $deliverables_dir

desc "Build the installation package"
task :package => [:binaries, $deliverables_dir] do
    old_pwd = Dir.pwd
    Dir.chdir $build_dir
	
	# Run make for packaging
    make_command = "make package"
    
	if $platform == :windows
		make_command = "msbuild PACKAGE.vcxproj " +
			"/p:Configuration=Release"
    end
			
    sh make_command
	
    Dir.chdir old_pwd
	
	# Move the compiled package to the deliverables area.
	package_name = "socout-#{$socout_version}-"
	package_path = "build/_CPack_Packages"
    
    if $platform == :windows
        package_name = package_name + "windows-x"
        package_path = package_path + "/windows-x"
    else
        package_name = package_name + "debian-x"
        package_path = package_path + "/debian-x"
    end
    
	if $wordsize == 32
		package_name = package_name + "32"
		package_path = package_path + "32"
	else
		package_name = package_name + "64"
		package_path = package_path + "64"
	end
    
    if $platform == :windows
        package_path = package_path + "/NSIS"
        package_name = package_name + ".exe"
    else
        package_path = package_path + "/DEB"
        package_name = package_name + ".deb"
    end
	
	FileUtils.mv "#{package_path}/#{package_name}", "#{$deliverables_dir}"
end
