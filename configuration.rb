# Configuration for the various rake tasks

# --- Version ---

$socout_version_major = 0
$socout_version_minor = 1
$socout_version_patch = 0
$socout_version_build = 0

$socout_version = "#{$socout_version_major}." +
    "#{$socout_version_minor}." +
    "#{$socout_version_patch}." +
    "#{$socout_version_build}"

# --- General ---

# The overall name of the project - change it here
$project_name = "socout"

# The top-level directory for source code files
$source_dir = "src"

# The directory in which build artefacts should be placed (relative to the
# top-level directory). Everything generated goes under here.
$build_dir = "build"

# The directory in which project deliverables are placed
$deliverables_dir = "#{$build_dir}/deliverables"

# --- Documentation-related ---

# Documentation source directory
$doc_source_dir = "doc"

# Documentation build directory
$doc_build_dir = "#{$build_dir}/doc"

# Low-level code documentation build directory
$code_doc_build_dir = "#{$doc_build_dir}/code"

# --- Test-related ---

# The names of the test executables. These are run in the sequence given, and
# the process stops if any of the executables has a return value other than
# zero.
#
# Note that these executables may be passed the command-line argument
# "--online". This only applies to tests that need to go online to perform
# their verification. If the "--online" argument is supplied, these tests
# are enabled, otherwise they are just empty (passing) tests.
$tests = [
    "test-#{$project_name}"
]

# --- Environment ---

# Most of this stuff is determined automatically. You should only change it
# if you want to explicitly override these settings.

# Determine what platform we're on
#
# This is easier in later versions of ruby / rake, but we want to support as
# wide a range of options as possible.
$platform = :linux
$platform = :windows if (Rake::Win32::windows?)

# Use the ruby 'pack' trick to determine platform bit-ness / word size
$wordsize = 32
$wordsize = 64 if ['foo'].pack("p").size == 8

# Work out where Qt is, and signal if we can't find it
if ENV['QT_DIR'] == nil
    ENV['QT_DIR'] = "/opt/Qt"
    ENV['QT_DIR'] = "C:/Qt/" if $platform == :windows
end

if File.directory?(ENV['QT_DIR']) == false
    raise "could not find Qt installation at the standard location " +
        "(\"#{ENV['QT_DIR']}\"); please set the 'QT_DIR' environment " +
        "variable to your QT install location"
end

# Work out exactly where our Qt environment is
if $platform == :windows
    if $wordsize == 32
        $qt_environment = "#{ENV['QT_DIR']}/5.3/msvc2013_opengl"
    else
        $qt_environment = "#{ENV['QT_DIR']}/5.3/msvc2013_64_opengl"
    end
else
    # Assume unix-like
    # TODO Support Apple
    # TODO Support Redhat
    
    if $wordsize == 32
       $qt_environment = "#{ENV['QT_DIR']}/5.3/gcc"
    else
       $qt_environment = "#{ENV['QT_DIR']}/5.3/gcc_64"
    end
end

# Make sure our Qt environment exists
if File.directory?($qt_environment) == false
    raise "could not find the platform-specific Qt environment at \"" +
        "#{$qt_environment}\"; you may need to edit the configuration.rb " +
        "file"
end

# --- Environment Variables ---

# Some settings are communicated to other elements of the build system using
# environment variables.
ENV['RAKE_PROJECT_NAME'] = $project_name
ENV['RAKE_CODE_DOC_BUILD_DIR'] = $code_doc_build_dir
ENV['RAKE_QT_ENVIRONMENT'] = $qt_environment

ENV['SOCOUT_VERSION_MAJOR'] = "#{$socout_version_major}"
ENV['SOCOUT_VERSION_MINOR'] = "#{$socout_version_minor}"
ENV['SOCOUT_VERSION_PATCH'] = "#{$socout_version_patch}"
ENV['SOCOUT_VERSION_BUILD'] = "#{$socout_version_build}"
ENV['SOCOUT_VERSION'] = $socout_version
ENV['WORDSIZE'] = "#{$wordsize}"
